=============
BSP Settings
=============

.. doxygengroup:: group_bsp_settings
   :project: TARGET_CY8CKIT-062S4 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: