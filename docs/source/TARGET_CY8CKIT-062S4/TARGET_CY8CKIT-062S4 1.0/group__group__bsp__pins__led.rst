=========
LED Pins
=========

.. doxygengroup:: group_bsp_pins_led
   :project: TARGET_CY8CKIT-062S4 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: