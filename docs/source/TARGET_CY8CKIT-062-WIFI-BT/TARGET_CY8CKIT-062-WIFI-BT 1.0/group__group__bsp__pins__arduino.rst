====================
Arduino Header Pins
====================

.. doxygengroup:: group_bsp_pins_arduino
   :project: TARGET_CY8CKIT-062-WIFI-BT 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: