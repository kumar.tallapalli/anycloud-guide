Create a Customer PSoC 4 Board Support Package
===============================================

.. raw:: html

   <script type="text/javascript">
   window.location.href = "TARGET_PSOC4-GENERIC 1.0/index.html"
   </script>
   
   
.. toctree::
   :hidden:

   TARGET_PSOC4-GENERIC 1.0/TARGET_PSOC4-GENERIC.rst
   TARGET_PSOC4-GENERIC 1.1/TARGET_PSOC4-GENERIC.rst