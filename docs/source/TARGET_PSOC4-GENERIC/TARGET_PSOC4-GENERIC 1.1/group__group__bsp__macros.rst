=======
Macros
=======

.. doxygengroup:: group_bsp_macros
   :project: TARGET_PSOC4-GENERIC 1.1
   :members:
   :protected-members:
   :private-members:
