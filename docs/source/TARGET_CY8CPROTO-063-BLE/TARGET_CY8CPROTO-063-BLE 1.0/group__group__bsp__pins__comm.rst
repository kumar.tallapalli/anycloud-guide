===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CY8CPROTO-063-BLE 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
