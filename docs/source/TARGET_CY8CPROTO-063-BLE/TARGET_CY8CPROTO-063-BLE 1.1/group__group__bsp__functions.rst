==========
Functions
==========

.. doxygengroup:: group_bsp_functions
   :project: TARGET_CY8CPROTO-063-BLE 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: