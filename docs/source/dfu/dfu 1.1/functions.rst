========================================================================
Data Fields
========================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      -  appId :
         `cy_stc_dfu_params_t <structcy__stc__dfu__params__t.html#ac1ea5832fe23467ea98d2760f0d87a6b>`__
      -  appVerified :
         `cy_stc_dfu_params_t <structcy__stc__dfu__params__t.html#aa316309fbc14b2984aea6f9545e2c995>`__
      -  dataBuffer :
         `cy_stc_dfu_params_t <structcy__stc__dfu__params__t.html#af8a1cf06e15fae5ed56840b92938e176>`__
      -  dataOffset :
         `cy_stc_dfu_params_t <structcy__stc__dfu__params__t.html#ae93116a573d35a278c7630f68c904c43>`__
      -  enterDFUVersion :
         `cy_stc_dfu_enter_t <structcy__stc__dfu__enter__t.html#ac05a26255f9e529566ba270c1652a68e>`__
      -  enterRevision :
         `cy_stc_dfu_enter_t <structcy__stc__dfu__enter__t.html#a3a914152df65a2f24c6921fb8fce3876>`__
      -  enterSiliconId :
         `cy_stc_dfu_enter_t <structcy__stc__dfu__enter__t.html#a2235d2fe4b40fb392f16a1bea94000ab>`__
      -  initCtl :
         `cy_stc_dfu_params_t <structcy__stc__dfu__params__t.html#af12b92bf3c15d572c0e1746c3742e27d>`__
      -  packetBuffer :
         `cy_stc_dfu_params_t <structcy__stc__dfu__params__t.html#a0c50ecc808f04106a5a7fbbc1e4022b6>`__
      -  timeout :
         `cy_stc_dfu_params_t <structcy__stc__dfu__params__t.html#a65c877d47227fa78b629359fab0b417e>`__


