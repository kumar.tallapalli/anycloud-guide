================
Data Structures
================
.. doxygengroup:: group_dfu_data_structs
   :project: dfu1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::

   structcy__stc__dfu__params__t.rst
   structcy__stc__dfu__enter__t.rst

