=================
Enumerated Types
=================

.. doxygengroup:: group_dfu_enums
   :project: dfu1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: