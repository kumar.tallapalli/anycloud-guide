=======
Macros
=======
.. doxygengroup:: group_dfu_macro
   :project: dfu1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::
   
   group__group__dfu__macro__config.rst
   group__group__dfu__macro__state.rst
   group__group__dfu__macro__commands.rst
   group__group__dfu__macro__ioctl.rst
   group__group__dfu__macro__response__size.rst
      

