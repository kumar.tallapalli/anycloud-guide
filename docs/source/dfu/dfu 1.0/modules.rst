==============
API Reference
==============

.. raw:: html

   <hr>

.. toctree::
   :hidden:

   group__group__dfu__macro.rst
   group__group__dfu__functions.rst
   group__group__dfu__globals.rst
   group__group__dfu__data__structs.rst
   group__group__dfu__enums.rst

Provides the list of API Reference

+----------------------------------+----------------------------------+
| \ `Macros <group                 |                                  |
| __group__dfu__macro.html>`__     |                                  |
+----------------------------------+----------------------------------+
| \ `User Config Macros            |                                  |
| <group__group__                  |                                  |
| dfu__macro__config.html>`__      |                                  |
+----------------------------------+----------------------------------+
| \ `DFU                           | The state of updating            |
| State <group__g                  |                                  |
| roup__dfu__macro__state.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `DFU                           |                                  |
| Commands <group__grou            |                                  |
| p__dfu__macro__commands.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Read/Write Data IO Control    | The values of the ctl parameter  |
| Values <group__g                 | to the                           |
| roup__dfu__macro__ioctl.html>`__ | `Cy_DFU_ReadData() <group__g     |
|                                  | roup__dfu__functions.html#ga9709 |
|                                  | 777e59f6cbdc9ca70c1ca041e414>`__ |
|                                  | and                              |
|                                  | `Cy_DFU_WriteData() <group__g    |
|                                  | roup__dfu__functions.html#ga9f58 |
|                                  | 9c0df780bd29477ab4d73cf4063b>`__ |
|                                  | functions                        |
+----------------------------------+----------------------------------+
| \ `Response                      |                                  |
| Size <group__group__dfu          |                                  |
| __macro__response__size.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Functions <group              |                                  |
| __group__dfu__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Global                        |                                  |
| Variables <gro                   |                                  |
| up__group__dfu__globals.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `External ELF file             | CyMCUElfTools adds these symbols |
| symb                             | to a generated ELF file          |
| ols <group__group__dfu__globals_ |                                  |
| _external__elf__symbols.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Data                          |                                  |
| Structures <group__gr            |                                  |
| oup__dfu__data__structs.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Enumerated                    |                                  |
| Types <g                         |                                  |
| roup__group__dfu__enums.html>`__ |                                  |
+----------------------------------+----------------------------------+


