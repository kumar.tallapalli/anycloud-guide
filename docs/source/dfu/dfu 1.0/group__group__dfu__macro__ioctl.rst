==================================
Read/Write Data IO Control Values
==================================



.. doxygengroup:: group_dfu_macro_ioctl
   :project: dfu1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: