========================
CapSense Data Structure
========================

.. doxygengroup:: group_capsense_data_structure
   :project: capsense1.1

.. toctree:: 
   group__group__capsense__structures.rst
   group__group__capsense__gesture__structures.rst