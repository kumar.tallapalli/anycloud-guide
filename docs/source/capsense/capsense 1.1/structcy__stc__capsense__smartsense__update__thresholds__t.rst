======================================================
cy_stc_capsense_smartsense_update_thresholds_t Struct
======================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares Update Thresholds structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`fingerTh <structcy__stc__capsense__smartsense__update__thresholds__t.html#a87e1518bbd9d2c9745c1e51aee9e1380>`__
 
Widget finger threshold.
 
uint8_t 
`noiseTh <structcy__stc__capsense__smartsense__update__thresholds__t.html#a6278afa4506e0f81410444bc8bd4ef6e>`__
 
Widget noise threshold.
 
uint8_t 
`nNoiseTh <structcy__stc__capsense__smartsense__update__thresholds__t.html#a0e03cc2b5dcfb072e46e2d4a6afb2ef6>`__
 
Widget negative noise threshold.
 
uint8_t 
`hysteresis <structcy__stc__capsense__smartsense__update__thresholds__t.html#a958e9740d154803f7fbf750862d8713c>`__
 
Widget hysteresis for the signal crossing finger threshold.



