=========================================
cy_stc_capsense_gesture_context_t Struct
=========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture global context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`position1 <structcy__stc__capsense__gesture__context__t.html#a35b0182d531b19cf23e454b49ce332bd>`__
 
Current position of the first touch.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`positionLast1 <structcy__stc__capsense__gesture__context__t.html#aa9ca0fa15854876acb6836093813d18f>`__
 
Previous position of the first touch.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`position2 <structcy__stc__capsense__gesture__context__t.html#a57ddd934f52502d73624fd05c643fe01>`__
 
Current position of the second touch.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`positionLast2 <structcy__stc__capsense__gesture__context__t.html#a41e2cc14c3e6e152afe249821d218619>`__
 
Previous position of the second touch.
 
uint32_t 
`timestamp <structcy__stc__capsense__gesture__context__t.html#a2a24ea7e25b9910d7f02e7e19e43195a>`__
 
Current timestamp.
 
uint16_t 
`detected <structcy__stc__capsense__gesture__context__t.html#a607d08f453a989754dd877c3118ec4c4>`__
 
Detected gesture mask.
 
uint16_t 
`direction <structcy__stc__capsense__gesture__context__t.html#a2951e786f1279c80f1210b5d69339681>`__
 
Mask of direction of detected gesture.
 
`cy_stc_capsense_ofrt_context_t <structcy__stc__capsense__ofrt__context__t.html>`__ 
`ofrtContext <structcy__stc__capsense__gesture__context__t.html#a48dfae93de6b33ba60cdd10f8292e590>`__
 
One-finger rotate gesture context.
 
`cy_stc_capsense_ofsl_context_t <structcy__stc__capsense__ofsl__context__t.html>`__ 
`ofslContext <structcy__stc__capsense__gesture__context__t.html#a7f7257503a55d3790ab222a6ac280a85>`__
 
One-finger scroll gesture context.
 
`cy_stc_capsense_tfzm_context_t <structcy__stc__capsense__tfzm__context__t.html>`__ 
`tfzmContext <structcy__stc__capsense__gesture__context__t.html#abdb25e19025b77d4115b551bf54454b5>`__
 
Two-finger zoom gesture context.
 
`cy_stc_capsense_tfsc_context_t <structcy__stc__capsense__tfsc__context__t.html>`__ 
`tfscContext <structcy__stc__capsense__gesture__context__t.html#a56adc5033fdd287a8fefb4a6c8b1a251>`__
 
Two-finger single click gesture context.
 
`cy_stc_capsense_ofes_context_t <structcy__stc__capsense__ofes__context__t.html>`__ 
`ofesContext <structcy__stc__capsense__gesture__context__t.html#a34feed1bf649ef82f09da51745d0514e>`__
 
One-finger edge swipe gesture context.
 
`cy_stc_capsense_offl_context_t <structcy__stc__capsense__offl__context__t.html>`__ 
`offlContext <structcy__stc__capsense__gesture__context__t.html#a7a6f0b99bddc8e18cfea0fedb39e00b8>`__
 
One-finger flick gesture context.
 
`cy_stc_capsense_ofsc_context_t <structcy__stc__capsense__ofsc__context__t.html>`__ 
`ofscContext <structcy__stc__capsense__gesture__context__t.html#a72755d5985247fd60078900d1482ddfc>`__
 
One-finger single click gesture context.
 
`cy_stc_capsense_ofdc_context_t <structcy__stc__capsense__ofdc__context__t.html>`__ 
`ofdcContext <structcy__stc__capsense__gesture__context__t.html#a41504e9edd68b6aab487492c95b6a059>`__
 
One-finger double click gesture context.
 
`cy_stc_capsense_ofcd_context_t <structcy__stc__capsense__ofcd__context__t.html>`__ 
`ofcdContext <structcy__stc__capsense__gesture__context__t.html#ab916aaccb1b24a8dc5eee9385d0d2041>`__
 
One-finger click and drag gesture context.
 
`cy_stc_capsense_tfsl_context_t <structcy__stc__capsense__tfsl__context__t.html>`__ 
`tfslContext <structcy__stc__capsense__gesture__context__t.html#a3e8c82cbc4289674f444114478fc1a7b>`__
 
Two-finger scroll gesture context.
 
uint8_t 
`numPosition <structcy__stc__capsense__gesture__context__t.html#a5f96cbd3fde2fc2007499bdfc5e8ad59>`__
 
Current number of touches.
 
uint8_t 
`numPositionLast <structcy__stc__capsense__gesture__context__t.html#adfc86db072daa3b309979efb5768456d>`__
 
Previous number of touches.

