======================================
cy_stc_capsense_ofdc_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture One Finger Double Click context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`touchStartTime1 <structcy__stc__capsense__ofdc__context__t.html#a4301e8f87ce8a40ecc73c415ebd56eab>`__
 
Touchdown time.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__ofdc__context__t.html#a22e58ee9031099dd6d48aee61a1aadc2>`__
 
Touchdown position.
 
uint8_t 
`state <structcy__stc__capsense__ofdc__context__t.html#a68bdeb81301daf15477f4e5130a60b6d>`__
 
Gesture state.

