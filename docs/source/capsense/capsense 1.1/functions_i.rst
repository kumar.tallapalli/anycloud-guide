=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - i -
         :name: i--

      -  id :
         `cy_stc_capsense_position_t <structcy__stc__capsense__position__t.html#ab5fb803657d04c35adc32a2685905e67>`__
      -  iDacComp :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#a7c937fca893ce53a81dd29d5541c2b75>`__
      -  idacComp :
         `cy_stc_capsense_sensor_context_t <structcy__stc__capsense__sensor__context__t.html#a242877120ccdac585a0f001736fe7be2>`__
      -  iDacGain :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#aaa6b7ff2794e44d8f333ecf6048dd6f6>`__
      -  idacGainIndex :
         `cy_stc_capsense_bist_custom_parameters_t <structcy__stc__capsense__bist__custom__parameters__t.html#a60c9a9e518194f83bd0bba955066474f>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#aca335177119844084a40aab5a8e4022e>`__
      -  idacGainIndexVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a16b1ee33055b4f7d5c1d9c6ff0cdade2>`__
      -  idacGainTable :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#aa420b265a50ab175b9a0902bcc3a0b08>`__
      -  iDacMod :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#adbec8688c917a7a25a7cc4b14b1e49ed>`__
      -  idacMod :
         `cy_stc_capsense_bist_custom_parameters_t <structcy__stc__capsense__bist__custom__parameters__t.html#ada4f2a1109cc40af7ad1455e67252153>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#acfd1cb8d218cf32a3d9466fa55a4065a>`__
      -  idacModVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a1d341f57cd4922055d9b81d2ad84836e>`__
      -  iirCoeff :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a3837d9686917c0fdfc96cadb261dbc03>`__
      -  initDone :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#a0181ebd06d8e03e3b18ac5c08ca14742>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
