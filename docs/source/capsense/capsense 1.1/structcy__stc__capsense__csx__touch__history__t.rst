===========================================
cy_stc_capsense_csx_touch_history_t Struct
===========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         CSX Touchpad touch tracking history.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 

`velocity <structcy__stc__capsense__csx__touch__history__t.html#a5a32e8b0644a7bc98f45a4b345188810>`__

 

| The square of the "speed" (maximum distance change per refresh
  interval) threshold distinguishing a fast finger movement from
  separate finger touches (in [pixels/refresh interval]).
  `More... <#a5a32e8b0644a7bc98f45a4b345188810>`__

 

`cy_stc_capsense_position_t <structcy__stc__capsense__position__t.html>`__ 

`oldPeak <structcy__stc__capsense__csx__touch__history__t.html#a9cd73499eb9138949da6bd8812a654d1>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__]

 

| Touch Positions.

 

uint8_t 

`oldPeakNumber <structcy__stc__capsense__csx__touch__history__t.html#aad62c7d9b921294d3caff02bcca10734>`__

 

| Number of detected peaks.

 

uint8_t 

`oldActiveIdsMask <structcy__stc__capsense__csx__touch__history__t.html#a673eaa945ec772627c954df63b42a89e>`__

 

| Mask of used IDs.

 

Field Documentation
---------------------

`◆  <#a5a32e8b0644a7bc98f45a4b345188810>`__\ velocity
=====================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------------+
      | uint32_t cy_stc_capsense_csx_touch_history_t::velocity |
      +--------------------------------------------------------+

   .. container:: memdoc

      The square of the "speed" (maximum distance change per refresh
      interval) threshold distinguishing a fast finger movement from
      separate finger touches (in [pixels/refresh interval]).

      Squared speeds exceeding this value indicate separate finger
      touches. Squared speeds below this value indicate fast single
      finger movement.
