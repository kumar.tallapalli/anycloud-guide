=================
Enumerated Types
=================


.. doxygengroup:: group_capsense_enums
   :project: capsense1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: