==============================================================
Cypress CapSense Middleware Library 2.10: Data Structure Index
==============================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: qindex

         `c <#letter_c>`__

      +-----------------------------------------------------------------------+
      | .. container:: ah                                                     |
      |                                                                       |
      |      c                                                                |
      +-----------------------------------------------------------------------+

`cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html>`__   
`cy_stc_capsense_csx_touch_history_t <structcy__stc__capsense__csx__touch__history__t.html>`__   
`cy_stc_capsense_ofdc_context_t <structcy__stc__capsense__ofdc__context__t.html>`__   
`cy_stc_capsense_smartsense_csd_noise_envelope_t <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html>`__   
`cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html>`__   
`cy_stc_capsense_electrode_config_t <structcy__stc__capsense__electrode__config__t.html>`__   
`cy_stc_capsense_ofes_context_t <structcy__stc__capsense__ofes__context__t.html>`__   
`cy_stc_capsense_smartsense_update_thresholds_t <structcy__stc__capsense__smartsense__update__thresholds__t.html>`__   
`cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html>`__   
`cy_stc_capsense_ballistic_delta_t <structcy__stc__capsense__ballistic__delta__t.html>`__   
`cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html>`__   
`cy_stc_capsense_offl_context_t <structcy__stc__capsense__offl__context__t.html>`__   
`cy_stc_capsense_tfsc_context_t <structcy__stc__capsense__tfsc__context__t.html>`__   
`cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html>`__   
`cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html>`__   
`cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html>`__   
`cy_stc_capsense_ofrt_context_t <structcy__stc__capsense__ofrt__context__t.html>`__   
`cy_stc_capsense_tfsl_context_t <structcy__stc__capsense__tfsl__context__t.html>`__   
`cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html>`__   
`cy_stc_capsense_bist_custom_parameters_t <structcy__stc__capsense__bist__custom__parameters__t.html>`__   
`cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html>`__   
`cy_stc_capsense_ofsc_context_t <structcy__stc__capsense__ofsc__context__t.html>`__   
`cy_stc_capsense_tfzm_context_t <structcy__stc__capsense__tfzm__context__t.html>`__   
`cy_stc_capsense_advanced_touchpad_config_t <structcy__stc__capsense__advanced__touchpad__config__t.html>`__   
`cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html>`__   
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__   
`cy_stc_capsense_ofsl_context_t <structcy__stc__capsense__ofsl__context__t.html>`__   
`cy_stc_capsense_touch_t <structcy__stc__capsense__touch__t.html>`__   
`cy_stc_capsense_alp_fltr_channel_t <structcy__stc__capsense__alp__fltr__channel__t.html>`__   
`cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html>`__   
`cy_stc_capsense_idac_gain_table_t <structcy__stc__capsense__idac__gain__table__t.html>`__   
`cy_stc_capsense_pin_config_t <structcy__stc__capsense__pin__config__t.html>`__   
`cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html>`__   
`cy_stc_capsense_alp_fltr_config_t <structcy__stc__capsense__alp__fltr__config__t.html>`__   
`cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html>`__   
`cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html>`__   
`cy_stc_capsense_position_t <structcy__stc__capsense__position__t.html>`__   
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__   
`cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html>`__   
`cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html>`__   
`cy_stc_capsense_ofcd_context_t <structcy__stc__capsense__ofcd__context__t.html>`__   
`cy_stc_capsense_sensor_context_t <structcy__stc__capsense__sensor__context__t.html>`__   
`cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html>`__   

.. container:: qindex

   `c <#letter_c>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
