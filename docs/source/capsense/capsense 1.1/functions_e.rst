=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - e -
         :name: e--

      -  edge :
         `cy_stc_capsense_ofes_context_t <structcy__stc__capsense__ofes__context__t.html#ac157aeb4721e704a38dff878b34f4bbf>`__
      -  edgeAngleMax :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a3e6b32e61c14bedfe0dae9fa6fa2ae76>`__
      -  edgeCorrectionEn :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#a5101330723664b0878203d70729b0f81>`__
      -  edgeDistanceMin :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a18f7465297e8f690506025215800585a>`__
      -  edgeEdgeSize :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#aa9a25c2b003808ad393ca2a00dc3f531>`__
      -  edgeTimeoutMax :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a2b78f409219d6f4de0e86b0554c2ec57>`__
      -  eltdCapCsdISC :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a22d5181fd636a7ff20a47559140e1e39>`__
      -  eltdCapCsxISC :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a6370446fb7f72be84768cb8fb68c7375>`__
      -  eltdCapModClk :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a331645cdb0494af471f9bdd735ede166>`__
      -  eltdCapResolution :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a234d9c4330bd5e4f6eaf0241d9b7da01>`__
      -  eltdCapSnsClk :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a6b8b7d2d90ea3d1dde78f8cea3f7f40c>`__
      -  eltdCapSnsClkFreqHz :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a3fecccd03e56148f208e64836aa070b4>`__
      -  eltdCapVrefGain :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a2acbf7fcbd54f316d5ed0cd6f0be6748>`__
      -  eltdCapVrefMv :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a00a9b297c1710c64237e41cf65b52dd4>`__
      -  extCapIdacPa :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a02ec16251b0f3ad575db0d179f879c87>`__
      -  extCapModClk :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a8ffe130298947c266c56ddfdd64f8420>`__
      -  extCapSnsClk :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a35acc7ff82625d0f4ddd9e3c020ee406>`__
      -  extCapVrefGain :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#af41b354a7c567232a7572a73ed0bc5b9>`__
      -  extCapVrefMv :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a64ff2a0e0441bb1d7c9c1062283e897b>`__
      -  extCapWDT :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a71c6e39161ae61c940eb57c948efbf3c>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
