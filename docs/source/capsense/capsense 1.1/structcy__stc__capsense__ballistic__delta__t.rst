=========================================
cy_stc_capsense_ballistic_delta_t Struct
=========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares Ballistic Displacement structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

int16_t 
`deltaX <structcy__stc__capsense__ballistic__delta__t.html#a454e97190a56ea9075d1764b214203d9>`__
 
X-axis displacement.
 
int16_t 
`deltaY <structcy__stc__capsense__ballistic__delta__t.html#a469d02345ffee521218243a7d2e61fb4>`__
 
Y-axis displacement.


