==================================
cy_stc_capsense_position_t Struct
==================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares position structure that keep information of a single
         touch.

         Depending on a widget type each structure field keeps the
         following information:

         +-------------+-------------+-------------+-------------+-------------+
         | Structure   | Slider      | Matrix      | CSD         | CSX         |
         | Field       |             | Buttons     | Touchpad    | Touchpad    |
         +=============+=============+=============+=============+=============+
         | x           | X-axis      | Active      | X-axis      | X-axis      |
         |             | position    | Column      | position    | position    |
         +-------------+-------------+-------------+-------------+-------------+
         | y           | Reserved    | Active Row  | Y-axis      | Y-axis      |
         |             |             |             | position    | position    |
         +-------------+-------------+-------------+-------------+-------------+
         | z           | Reserved    | Reserved    | Reserved    | MSB = Age   |
         |             |             |             |             | of touch;   |
         |             |             |             |             | LSB =       |
         |             |             |             |             | Z-value     |
         +-------------+-------------+-------------+-------------+-------------+
         | id          | Reserved    | Logical     | Reserved    | MSB =       |
         |             |             | number of   |             | Debounce;   |
         |             |             | button      |             | LSB = touch |
         |             |             |             |             | ID          |
         +-------------+-------------+-------------+-------------+-------------+

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`x <structcy__stc__capsense__position__t.html#aaa333fb1aa5285e28ffdf6cc47248741>`__
 
X position.
 
uint16_t 
`y <structcy__stc__capsense__position__t.html#a29b1ad8aeb8ab133b7696273fbdfa6cf>`__
 
Y position.
 
uint16_t 
`z <structcy__stc__capsense__position__t.html#a9a62c41da414fc72932fd1bcd8fbfc96>`__
 
Z value.
 
uint16_t 
`id <structcy__stc__capsense__position__t.html#ab5fb803657d04c35adc32a2685905e67>`__
 
ID of touch.
 

