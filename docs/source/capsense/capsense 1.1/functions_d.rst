=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - d -
         :name: d--

      -  dataParam0 :
         `cy_stc_capsense_alp_fltr_channel_t <structcy__stc__capsense__alp__fltr__channel__t.html#a4eeaf7e6e557a71e478f2ebdb50574de>`__
      -  dataParam1 :
         `cy_stc_capsense_alp_fltr_channel_t <structcy__stc__capsense__alp__fltr__channel__t.html#a6cbff9d935df1845ac6cdf4172c7209b>`__
      -  dataParam2 :
         `cy_stc_capsense_alp_fltr_channel_t <structcy__stc__capsense__alp__fltr__channel__t.html#ae21096248150ede5126f6372dc8bc6e9>`__
      -  dataParam3 :
         `cy_stc_capsense_alp_fltr_channel_t <structcy__stc__capsense__alp__fltr__channel__t.html#a1bd5de721828504f592e4ed5d55e0564>`__
      -  dataParam4 :
         `cy_stc_capsense_alp_fltr_channel_t <structcy__stc__capsense__alp__fltr__channel__t.html#ac62dc41e0fd4f3ccd948f866549ea11e>`__
      -  dataParam5 :
         `cy_stc_capsense_alp_fltr_channel_t <structcy__stc__capsense__alp__fltr__channel__t.html#a4372588f14b58af19f65cb07aa13e847>`__
      -  dataParam6 :
         `cy_stc_capsense_alp_fltr_channel_t <structcy__stc__capsense__alp__fltr__channel__t.html#a40660b6044aa1b08f4e54629dcf0ce22>`__
      -  dataParam7 :
         `cy_stc_capsense_alp_fltr_channel_t <structcy__stc__capsense__alp__fltr__channel__t.html#a885a5ec429033d11ae602ebaf7676413>`__
      -  debounce :
         `cy_stc_capsense_ofrt_context_t <structcy__stc__capsense__ofrt__context__t.html#a3d14b869436d9a7a23c57db47a665140>`__
         ,
         `cy_stc_capsense_ofsl_context_t <structcy__stc__capsense__ofsl__context__t.html#a8b38d4d85c4c429162a82f66494bab4c>`__
         ,
         `cy_stc_capsense_tfsl_context_t <structcy__stc__capsense__tfsl__context__t.html#a716e4c137b9094a50dd9c85cb46327ea>`__
         ,
         `cy_stc_capsense_tfzm_context_t <structcy__stc__capsense__tfzm__context__t.html#a3a6d2543ec80c8af2b26ecd1e9cb4544>`__
      -  deltaX :
         `cy_stc_capsense_ballistic_delta_t <structcy__stc__capsense__ballistic__delta__t.html#a454e97190a56ea9075d1764b214203d9>`__
      -  deltaXfrac :
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#a8fe77855d41716420ecb5b8a6a4726d5>`__
      -  deltaY :
         `cy_stc_capsense_ballistic_delta_t <structcy__stc__capsense__ballistic__delta__t.html#a469d02345ffee521218243a7d2e61fb4>`__
      -  deltaYfrac :
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#ada5af6548b412a22be7317001ceafa1d>`__
      -  detected :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a607d08f453a989754dd877c3118ec4c4>`__
      -  diff :
         `cy_stc_capsense_sensor_context_t <structcy__stc__capsense__sensor__context__t.html#ae3a77556515876cf00e178ccc7cbd606>`__
      -  direction :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a2951e786f1279c80f1210b5d69339681>`__
         ,
         `cy_stc_capsense_ofsl_context_t <structcy__stc__capsense__ofsl__context__t.html#a16e86b0e4a4e18f39d1d04af5f19f77b>`__
         ,
         `cy_stc_capsense_tfsl_context_t <structcy__stc__capsense__tfsl__context__t.html#af33814e8e0f9b68b402b92868d440997>`__
      -  distanceMap :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#aea8ff4b04a46ed06f2e05ebdb85d2e7a>`__
      -  distanceX :
         `cy_stc_capsense_tfzm_context_t <structcy__stc__capsense__tfzm__context__t.html#aeba8c48b23a049c2c427c01d79f8cf4a>`__
      -  distanceY :
         `cy_stc_capsense_tfzm_context_t <structcy__stc__capsense__tfzm__context__t.html#af80b64aea8441ddd468baa43048846a4>`__
      -  divisorValue :
         `cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html#a847c20119361422daf67ca877c6ef456>`__
      -  divVal :
         `cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html#a9a1c907add092d6754cb3d5669b26b19>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
