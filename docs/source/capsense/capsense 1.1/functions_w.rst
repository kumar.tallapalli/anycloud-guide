=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - w -
         :name: w--

      -  wdgtCrcCalc :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a21a8e2f2c74ad54372244ad94f9245b5>`__
      -  wdTouch :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a8278c09fe62e33e5e460f3064b90e614>`__
      -  wdType :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a78fb9e1d1b8c8cdad2d4257f5881ddc2>`__
      -  widgetIndex :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#a013e17535d7b3ed5bb4268a5baf5863b>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
