==================================================
cy_stc_capsense_advanced_centroid_config_t Struct
==================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares Advanced Centroid configuration parameters.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`fingerTh <structcy__stc__capsense__advanced__centroid__config__t.html#ab7f5ac133fa009e0999779d34ee0c59e>`__
 
Finger threshold of widget.
 
uint16_t 
`penultimateTh <structcy__stc__capsense__advanced__centroid__config__t.html#a72dadc2970371bea639b29a91a7f4ff5>`__
 
Penultimate threshold.
 
uint16_t 
`virtualSnsTh <structcy__stc__capsense__advanced__centroid__config__t.html#a99998bdcb8adfc186748f5e8ecff0724>`__
 
Virtual sensor threshold.
 
uint16_t 
`resolutionX <structcy__stc__capsense__advanced__centroid__config__t.html#af68f4b04172c275b6be6e12768dc7991>`__
 
X axis maximum position.
 
uint16_t 
`resolutionY <structcy__stc__capsense__advanced__centroid__config__t.html#a41e2b2272353ff198fca4fbe4397d9bd>`__
 
Y axis maximum position.
 
uint8_t 
`crossCouplingTh <structcy__stc__capsense__advanced__centroid__config__t.html#a09416d6547439c3fd7ec944f3aa7893f>`__
 
Cross-coupling threshold.
 
uint8_t 
`snsCountX <structcy__stc__capsense__advanced__centroid__config__t.html#a6516dd861d60a16f4542edd695e6411a>`__
 
Number of segments on X axis.
 
uint8_t 
`snsCountY <structcy__stc__capsense__advanced__centroid__config__t.html#a3ca2e3fdf2d68035f14bd1f67c50f9bd>`__
 
Number of segments on Y axis.
 
uint8_t 
`edgeCorrectionEn <structcy__stc__capsense__advanced__centroid__config__t.html#a5101330723664b0878203d70729b0f81>`__
 
Edge correction enabled.
 
uint8_t 
`twoFingersEn <structcy__stc__capsense__advanced__centroid__config__t.html#a541c1a949ee1bb818933bbb95f153ada>`__
 
Two-finger detection enabled.
 
