=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - s -
         :name: s--

      -  scanCounter :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#a6406229ffd99b912c09ca0c80ea3f4ee>`__
      -  scanScopeAll :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#abf320502877dad97fd6227d8ee3202ca>`__
      -  scanScopeSns :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#ac0ec073645f66018f477e711dad937a7>`__
      -  scrollDebounce :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a3048bc2987cdde967abbbd4a18051ae1>`__
      -  scrollDistanceMin :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a27e76edb6a8d860482a55b8f4071f337>`__
      -  secondClickDistanceMax :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a32dd076cfd7800d96f465c6574059443>`__
      -  secondClickIntervalMax :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a1504ee52ad24c04ac285a6fa3acb3bde>`__
      -  secondClickIntervalMin :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a138b9550ac79485932c329daeb3a552f>`__
      -  senseMethod :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a417a8786dfe62d3940fd7d5e265b9e2e>`__
      -  sensorCap :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#a1d0e22bde6cc10e84ce2e37524486fc1>`__
      -  sensorIndex :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#ad8da18f16f62c0855f70ec8ba044f7aa>`__
      -  shieldCap :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ad9d7572ae68f02fff90133e676cc1bdb>`__
      -  shieldCapISC :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a8e6f44610a21e97402349e14d6c090d1>`__
      -  shortedSnsId :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#aefc11745803885a1b87cc4017288d7fa>`__
      -  shortedWdId :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a4d106f872eeea02719e146b4427c1d18>`__
      -  sigPFC :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#a02620f92d5236d731cfd30c52b3881c5>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a818a66cb212ab10f9e4fb4f25a5b8a14>`__
      -  sigPFCVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a2742dd33a65b5791a4aca1dd86f60910>`__
      -  snsClk :
         `cy_stc_capsense_bist_custom_parameters_t <structcy__stc__capsense__bist__custom__parameters__t.html#aa9db7044638f1e6ee7a56a850f31d67d>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#ac2c184d4476a32763d29fd3c1813d533>`__
      -  snsClkConstantR :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#a6ec095fe5aeb9289f98829815219ea37>`__
      -  snsClkInputClock :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#a104c0df5a13ed859885dd8be3d274dc2>`__
      -  snsClkSource :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#ab9a639859b9bb45518c041e37bfca15a>`__
      -  snsClkSourceVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a56160b9d3867b8b59fda1ebface25359>`__
      -  snsClkVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a888a88ed2d857e30e18b5240731fafb7>`__
      -  snsCountX :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#a6516dd861d60a16f4542edd695e6411a>`__
      -  snsCountY :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#a3ca2e3fdf2d68035f14bd1f67c50f9bd>`__
      -  snsIntgShortSettlingTime :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a5b5122344f3fac96dd7ba538eff80298>`__
      -  speedCoeff :
         `cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html#ad5992e4bc35b511b1d49699ead2d5d18>`__
      -  speedThresholdX :
         `cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html#ab4deccb79d0803f081e1283ee7006acd>`__
      -  speedThresholdY :
         `cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html#adb09b97cfc40452a62b7cc4c92dda9a3>`__
      -  ssIrefSource :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a0c80e26b5fe28518248152c5c9c6681a>`__
      -  ssVrefSource :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a9d4938569f715d511d5a92c65337903a>`__
      -  state :
         `cy_stc_capsense_ofcd_context_t <structcy__stc__capsense__ofcd__context__t.html#affdde7ae14962e5fb4a245bcc6431f46>`__
         ,
         `cy_stc_capsense_ofdc_context_t <structcy__stc__capsense__ofdc__context__t.html#a68bdeb81301daf15477f4e5130a60b6d>`__
         ,
         `cy_stc_capsense_ofes_context_t <structcy__stc__capsense__ofes__context__t.html#aceb2451dce6a58a27a57aad7567d96de>`__
         ,
         `cy_stc_capsense_offl_context_t <structcy__stc__capsense__offl__context__t.html#ab75c1891770f28ddf2e351352ad44ec9>`__
         ,
         `cy_stc_capsense_ofrt_context_t <structcy__stc__capsense__ofrt__context__t.html#a15e10b8b2d960028f26f3781bd86d1b2>`__
         ,
         `cy_stc_capsense_ofsc_context_t <structcy__stc__capsense__ofsc__context__t.html#a8b9f3178a921e342a800d39f2c276911>`__
         ,
         `cy_stc_capsense_ofsl_context_t <structcy__stc__capsense__ofsl__context__t.html#a7df429d86ffa5f133a64f3fa4a9c63bc>`__
         ,
         `cy_stc_capsense_tfsc_context_t <structcy__stc__capsense__tfsc__context__t.html#a8e22783a5b0ccb54c439fceb105978e4>`__
         ,
         `cy_stc_capsense_tfsl_context_t <structcy__stc__capsense__tfsl__context__t.html#aff4bf63c794f8b5f1c07d5473dbfa670>`__
         ,
         `cy_stc_capsense_tfzm_context_t <structcy__stc__capsense__tfzm__context__t.html#a4d55acf75ebf067266e51707eace00c2>`__
      -  status :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#ae823664536f36878278584c2e8967035>`__
         ,
         `cy_stc_capsense_sensor_context_t <structcy__stc__capsense__sensor__context__t.html#ac0c3267b406b40d89fb8219b75536eee>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#ad7b0478d50436b8f1cdb982b5d731a68>`__
      -  swSensorAutoResetEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a16001bf367c6118143ff1ec3511646fb>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
