=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - y -
         :name: y--

      -  y :
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#abadcc7e1af463b1668eb93183cb87252>`__
         ,
         `cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html#a3a1c2f83221a848b380ec4c10362187a>`__
         ,
         `cy_stc_capsense_position_t <structcy__stc__capsense__position__t.html#a29b1ad8aeb8ab133b7696273fbdfa6cf>`__
      -  yDelta :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a869b23837236045aba13b4fbde082e95>`__
      -  yResolution :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#ae527a11d29c8deb400a6c2a58d677aee>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
