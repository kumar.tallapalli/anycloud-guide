=================================================================
Cypress CapSense Middleware Library 2.10: Data Fields - Variables
=================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - g -
         :name: g--

      -  gainReg :
         `cy_stc_capsense_idac_gain_table_t <structcy__stc__capsense__idac__gain__table__t.html#ad44c7d56219cb4db0e7aaaba7821fb40>`__
      -  gainValue :
         `cy_stc_capsense_idac_gain_table_t <structcy__stc__capsense__idac__gain__table__t.html#ae0e27b2d2c7a17bde0d7ed7c00bd0074>`__
      -  gestureDetected :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a1abcf6d1b22d6cc09006eb81576f81f6>`__
      -  gestureDirection :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a1bd21ada41343af3e4102b85ab62edab>`__
      -  gestureEnableMask :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a47ee76801d9ab0c1c11e057d12fb37cc>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
