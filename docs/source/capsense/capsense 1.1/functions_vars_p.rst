=================================================================
Cypress CapSense Middleware Library 2.10: Data Fields - Variables
=================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - p -
         :name: p--

      -  param0 :
         `cy_stc_capsense_smartsense_csd_noise_envelope_t <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#a05b74489afc68fe873e70519784f8763>`__
      -  param1 :
         `cy_stc_capsense_smartsense_csd_noise_envelope_t <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#aa946864ff892df81a594e217ceba1d55>`__
      -  param2 :
         `cy_stc_capsense_smartsense_csd_noise_envelope_t <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#abd13d4d25441135f993b97aa47e2c230>`__
      -  param3 :
         `cy_stc_capsense_smartsense_csd_noise_envelope_t <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#a366c60ce71fcb0fce32659a16fa09e45>`__
      -  param4 :
         `cy_stc_capsense_smartsense_csd_noise_envelope_t <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#a836e0975942aa929fabd0e74a8628e33>`__
      -  param5 :
         `cy_stc_capsense_smartsense_csd_noise_envelope_t <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#aa10ef391278327840a384056d062ae41>`__
      -  param6 :
         `cy_stc_capsense_smartsense_csd_noise_envelope_t <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#a45d5c40b50ac10537e35b7e55dcc88f4>`__
      -  pcPtr :
         `cy_stc_capsense_pin_config_t <structcy__stc__capsense__pin__config__t.html#a6e73d425b113f359b2d0911e44b36e62>`__
      -  penultimateTh :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#a72dadc2970371bea639b29a91a7f4ff5>`__
         ,
         `cy_stc_capsense_advanced_touchpad_config_t <structcy__stc__capsense__advanced__touchpad__config__t.html#a990aa2b11b9bf7779a3de1eff80ded85>`__
      -  periClkHz :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a885d6f703d5687e6585b929f61265f9c>`__
      -  periDividerIndex :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#af4c7933702ebe6de8d7a51ead347f8dc>`__
      -  periDividerType :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ab1171e77b08743ec9cb1a0751f3a8a24>`__
      -  pinCintA :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a511bdf3dac96f6d21a99570a0d631a18>`__
      -  pinCintB :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ae19eb70caeeb497808e72757c4dc4d97>`__
      -  pinCmod :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a552e76fb483b1aef4e079c6a08343622>`__
      -  pinCmodPad :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a5115d84073d7d01cb4885798dcd8e0eb>`__
      -  pinCsh :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a1894d1621d3d0b85dcecb541580086b2>`__
      -  pinCshPad :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#affacfb2ff9e57a616462f673603d2da9>`__
      -  pinNumber :
         `cy_stc_capsense_pin_config_t <structcy__stc__capsense__pin__config__t.html#a87a0da3c8c5c9f372c4bbfbdbf3498d7>`__
      -  pinShieldPad :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a408b1a89c4eb3f5aec4a24ba2d9942d2>`__
      -  pinVrefExtPad :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#aff2a6b86b567e1b1c7328dae74aa4c0d>`__
      -  portCintA :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a325bae184f92fdb4eface75f45491a0a>`__
      -  portCintB :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ae8e8c568ea38fc8fde93b2dadc5e83f9>`__
      -  portCmod :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ab76476aba113baea0f471f559a36a99c>`__
      -  portCmodNum :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ae11a796b0265b7de228505259d4b8a98>`__
      -  portCmodPadNum :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a4176f5f4c3a0674e8db380c1cd53b077>`__
      -  portCsh :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ab8b3fc9607ea9d6a5902e69f6fa555eb>`__
      -  portCshNum :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a72dcc792179198a2c8a7a92b38c5af15>`__
      -  portCshPadNum :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a6f12b37bfb79c92ac49be9d9a10e1c85>`__
      -  portShieldPadNum :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a0757058df63bf046d385f86f141235f2>`__
      -  portVrefExtPadNum :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#acd41888daad97f1aa24dfbca213431a6>`__
      -  posFilterConfig :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a477c8db37f2ff589fa1967272d1f5260>`__
      -  position1 :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a35b0182d531b19cf23e454b49ce332bd>`__
      -  position2 :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a57ddd934f52502d73624fd05c643fe01>`__
      -  positionFilterEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a0ff07b677d89662e56814ba7ef8d550f>`__
      -  positionLast1 :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#aa9ca0fa15854876acb6836093813d18f>`__
      -  positionLast2 :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a41e2cc14c3e6e152afe249821d218619>`__
      -  proxTh :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#ac28a8ad287f401bde36886e1995ebd5d>`__
      -  proxThVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#ae33b026cd3a82c07e0dcdc80723b0e77>`__
      -  proxTouchCoeff :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#af700535efc4ea5d745cad94153d1d9a6>`__
      -  ptrActiveScanSns :
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a2719dc3e059d012106c5f4476faec66d>`__
      -  ptrBallisticContext :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#ad510b796c693e08b4f4b8bf8f76c3fb5>`__
      -  ptrBistContext :
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a92125bdfe35272f254fcc58d857670ce>`__
      -  ptrBslnInv :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#aa246164662305a0152b2736f56b67e5a>`__
      -  ptrCommonConfig :
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a7e55b9983341848b270166d72d9b50ac>`__
      -  ptrCommonContext :
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a2f4f7d25d0816cf7fe6f04103c4d647e>`__
      -  ptrCsdBase :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a314b8ab78e6a189daeb4d150d3f51512>`__
      -  ptrCsdContext :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a633cc2ca6a5528f5a82074e889287b1c>`__
      -  ptrCsdTouchBuffer :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a8da5973b3062723df1b1ebf6b7f5bd86>`__
      -  ptrCsxTouchBuffer :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a35afcfaedb37e24127fa5642bab59b45>`__
      -  ptrCsxTouchHistory :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#ac662c2d9016717ff89a9fd8c26c97c02>`__
      -  ptrDebounceArr :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#aa353e792734db1ff3873175bb8e832b9>`__
      -  ptrDiplexTable :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a6224ab7605e28404d786633b2c1c1f6b>`__
      -  ptrEltdCapacitance :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#aeb69ac183634b05ecb92ab57e1b8feb9>`__
      -  ptrEltdConfig :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#abc6f5af6f05f765faaca0aa5f4d9da40>`__
         ,
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a88a5166ff86cf001344434b0bb84d799>`__
      -  ptrEOSCallback :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#ae5ddccd94ecc0ee71482f6c6cbba947f>`__
      -  ptrFptrConfig :
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a00787c47f768578185fc57e6035ff68a>`__
      -  ptrGestureConfig :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a0f4c0e442a86b761a8a00cd76ecc83b5>`__
      -  ptrGestureContext :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#ad60be1a14aded108976eb2d66daac7f1>`__
      -  ptrInternalContext :
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a9b84076b589b363d35097eb12420fd4b>`__
      -  ptrISRCallback :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#a7f72ba89653be42335b33201907e4c5b>`__
      -  ptrNoiseEnvelope :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a8cbf228dd4ae8a901c911c162c70b30a>`__
      -  ptrPin :
         `cy_stc_capsense_electrode_config_t <structcy__stc__capsense__electrode__config__t.html#a2cdfd659151f2fc6d9e0f49aaa2058f7>`__
      -  ptrPinConfig :
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a9f676c61d0088ba82b3d7adaea8459e0>`__
      -  ptrPosFilterHistory :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a6136eead3f476c9c0d1d3fa1dfd8c189>`__
      -  ptrPosition :
         `cy_stc_capsense_touch_t <structcy__stc__capsense__touch__t.html#a41f4181caf21c8cc60825a4c88a952c6>`__
      -  ptrRawFilterHistory :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a3b1c56df207169c04a05b7fa8833b0fc>`__
      -  ptrRawFilterHistoryLow :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a9c3303699258fdb9e0a1c51ac8542370>`__
      -  ptrRxConfig :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#ada5061a0a2b46e3bff3553d98644c041>`__
      -  ptrSenseClk :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#ad6aa32b14c98f74107b4230446e35fed>`__
      -  ptrShieldPinConfig :
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a2a2bab67c44107594f5c43c785b0cb2e>`__
      -  ptrSnsContext :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#aeb8898be429a7205311694ac340b0d2e>`__
         ,
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#aace32d41062b136d802d346231967ad5>`__
      -  ptrSSCallback :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#ac3a7fcf3be3a2fc7491cdb9c002eb55e>`__
      -  ptrTunerReceiveCallback :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#a18d43c62af42453bbd7a41778691a7e2>`__
      -  ptrTunerSendCallback :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#ae0c310162f4df691e7d5ac8ec7937491>`__
      -  ptrTxConfig :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#a9cceebad50f0ca6d92bbfc2408d2c21a>`__
      -  ptrWdConfig :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#ab57fc4e1bdf74350bf3c6a701ad3c56a>`__
         ,
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a9b12863ecde08d94f68285a852e6f69b>`__
      -  ptrWdContext :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#a81beffc93033c90901da1f8debfbd59c>`__
         ,
         `cy_stc_capsense_context_t <structcy__stc__capsense__context__t.html#a1686259414950156dca1818d0db45922>`__
         ,
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#adb558f384dcfa5fe3701f6c377b94da3>`__
      -  ptrWdgtCrc :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#af1dcb8fad4599fa893c49f69abac5724>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
