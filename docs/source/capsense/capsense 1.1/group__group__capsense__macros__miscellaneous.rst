=====================
Miscellaneous Macros
=====================


.. doxygengroup:: group_capsense_macros_miscellaneous
   :project: capsense1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: