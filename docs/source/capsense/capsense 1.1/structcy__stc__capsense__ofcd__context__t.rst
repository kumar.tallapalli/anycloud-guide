======================================
cy_stc_capsense_ofcd_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture One Finger Click and Drag context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`touchStartTime1 <structcy__stc__capsense__ofcd__context__t.html#adac662a3fdcbff352d05b47127722437>`__
 
Touchdown time.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__ofcd__context__t.html#aa31dd67b76a421f313721950384c14ab>`__
 
Touchdown position.
 
uint8_t 
`state <structcy__stc__capsense__ofcd__context__t.html#affdde7ae14962e5fb4a245bcc6431f46>`__
 
Gesture state.

