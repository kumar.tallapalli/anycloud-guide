=============================================================================================
Cypress CapSense Middleware Library 2.10: cy_stc_capsense_alp_fltr_channel_t Struct Reference
=============================================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares ALP filter data structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`dataParam0 <structcy__stc__capsense__alp__fltr__channel__t.html#a4eeaf7e6e557a71e478f2ebdb50574de>`__
 
Parameter 0 context.
 
uint16_t 
`dataParam1 <structcy__stc__capsense__alp__fltr__channel__t.html#a6cbff9d935df1845ac6cdf4172c7209b>`__
 
Parameter 1 context.
 
uint16_t 
`dataParam2 <structcy__stc__capsense__alp__fltr__channel__t.html#ae21096248150ede5126f6372dc8bc6e9>`__
 
Parameter 2 context.
 
uint16_t 
`dataParam3 <structcy__stc__capsense__alp__fltr__channel__t.html#a1bd5de721828504f592e4ed5d55e0564>`__
 
Parameter 3 context.
 
uint16_t 
`dataParam4 <structcy__stc__capsense__alp__fltr__channel__t.html#ac62dc41e0fd4f3ccd948f866549ea11e>`__
 
Parameter 4 context.
 
uint16_t 
`dataParam5 <structcy__stc__capsense__alp__fltr__channel__t.html#a4372588f14b58af19f65cb07aa13e847>`__
 
Parameter 5 context.
 
uint16_t 
`dataParam6 <structcy__stc__capsense__alp__fltr__channel__t.html#a40660b6044aa1b08f4e54629dcf0ce22>`__
 
Parameter 6 context.
 
uint8_t 
`dataParam7 <structcy__stc__capsense__alp__fltr__channel__t.html#a885a5ec429033d11ae602ebaf7676413>`__
 
Parameter 7 context.
 

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
