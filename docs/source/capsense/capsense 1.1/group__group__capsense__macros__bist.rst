==========================
Built-in Self-test Macros
==========================


.. doxygengroup:: group_capsense_macros_bist
   :project: capsense1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: