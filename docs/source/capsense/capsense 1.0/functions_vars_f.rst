=================================================================
Cypress CapSense Middleware Library 2.10: Data Fields - Variables
=================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - f -
         :name: f--

      -  fineInitTime :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a5fc71d7b28788af5ec71a5e45c8ccf8e>`__
         ,
         `cy_stc_capsense_bist_custom_parameters_t <structcy__stc__capsense__bist__custom__parameters__t.html#a73d52513a1573f6fe1ad64ed47956660>`__
      -  fingerCap :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#a1cfd6c229a3feee37da2016b69128be8>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#aaddfea3cc416d040629708d1723c12f6>`__
      -  fingerCapVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a2c02fa3c77712c71160208f61b1d7f66>`__
      -  fingerPosIndexMap :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#af4749658a6668c77f5ed0c326cf6308e>`__
      -  fingerTh :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#ab7f5ac133fa009e0999779d34ee0c59e>`__
         ,
         `cy_stc_capsense_smartsense_update_thresholds_t <structcy__stc__capsense__smartsense__update__thresholds__t.html#a87e1518bbd9d2c9745c1e51aee9e1380>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#ab1730b817a861922f4375527d7a1d3b4>`__
      -  fingerThVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a373d9ef4767624a402f7b5212474c2e7>`__
      -  flickDistanceMin :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a4368430ad62e713cfa9c91275ffcceda>`__
      -  flickTimeoutMax :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#ac6379bfa4aa94d4031692bc31eeee6e1>`__
      -  fptrAdaptiveFilterInitializeLib :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a41770300c55cd8bb831388529fdda828>`__
      -  fptrAdaptiveFilterRunLib :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a38bca06b3f27677471df626f15185085>`__
      -  fptrBallisticMultiplierLib :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a57be94f71434afa9dd47cdae2b2e052d>`__
      -  fptrBistDisableMode :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a3e5bdaa1c968b2a28921aa71b23554a7>`__
      -  fptrBistDsInitialize :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#abb32fecc47613c7104f1d437224c2222>`__
      -  fptrBistInitialize :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a6daf21c1e84deeae72bf8c3b6e6bcbcf>`__
      -  fptrCalibrateAllCsdWidgets :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a2a94c5e6a622b2983a34f90fb057b2ab>`__
      -  fptrCalibrateAllCsxWidgets :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#aa9b696dcb3ceee40a579668c384e04f3>`__
      -  fptrCSDCalibrateWidget :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#ad7a1e474c2dc9408e6610ef68313b049>`__
      -  fptrCSDDisableMode :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#adaedd2f3df11f6560c9a548327c4f083>`__
      -  fptrCSDInitialize :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#abeb84b1ed0e6c899e0ac8775aa53ee72>`__
      -  fptrCSDScan :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a64546c38346006ec7c1137dae13e210a>`__
      -  fptrCSDScanISR :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a746ac7fd5454903e32338d5ceb85a7e8>`__
      -  fptrCSDSetupWidget :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#aeb2b9a96c64e8ff976d066db67950f7f>`__
      -  fptrCSXDisableMode :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#aa7757a0d1424e30d581c3bd80a7afc0e>`__
      -  fptrCSXInitialize :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a03e9ccdda20440a5337a38bab8b478a6>`__
      -  fptrCSXScan :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a5a7709afc918b5a0ac19aececd2caf0e>`__
      -  fptrCSXScanISR :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#ad45248eed224a9a3ae3e04adbfec90c9>`__
      -  fptrCSXSetupWidget :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a8a7e1b460e9e6cc73724def7469c5962>`__
      -  fptrDpAdvancedCentroidTouchpad :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a48fb912241f04ed457189575eaabc160>`__
      -  fptrDpProcessButton :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a2fcc6b9890c2741ce4d033bb22ae594d>`__
      -  fptrDpProcessCsdMatrix :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a54c1a05c2bdda9bb936f50ab37e3f304>`__
      -  fptrDpProcessCsdTouchpad :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a757bd8341a43081a8d893084a2ad472e>`__
      -  fptrDpProcessCsdWidgetRawCounts :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#ab714659f9debd5ce3946592c5bc58e82>`__
      -  fptrDpProcessCsdWidgetStatus :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a04cd8fabd3aae362d8ed413e5b02d1e5>`__
      -  fptrDpProcessCsxTouchpad :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a7609a9959e55ddefc5af6769b4e4b355>`__
      -  fptrDpProcessCsxWidgetRawCounts :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a9e40c13bb8b7e169f3d8528c0229b554>`__
      -  fptrDpProcessCsxWidgetStatus :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a2ae6f6acf2aaa8a58171f43b52812ae3>`__
      -  fptrDpProcessProximity :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a66ee988edf687d26fe19cb4ed38f3f36>`__
      -  fptrDpProcessSlider :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a1a9b01b42256b85a945033b75a101733>`__
      -  fptrDpUpdateThresholds :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a27eba5a24e091a1410ac07b486c39def>`__
      -  fptrFtRunEnabledFiltersInternal :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a9077cb0eccfed8647c91080e501fb18e>`__
      -  fptrInitializeAllFilters :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#aaf36b219b6ed11505273e20b523ba7a0>`__
      -  fptrInitializeNoiseEnvelopeLib :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a9d86ff861bf65d65ed110221c8fb721d>`__
      -  fptrInitPositionFilters :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a6cb6d8d58d21dbd4920246b84b30175e>`__
      -  fptrProcessPositionFilters :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#af14258d82e10a39eec262afdbe657866>`__
      -  fptrRunNoiseEnvelopeLib :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a0f9cf34d40eb21dcc0d8a6068ba1edaf>`__
      -  fptrRunPositionFilters :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#af5bb2f2a934df34465fa7dd9fd2358a3>`__
      -  fptrSsAutoTune :
         `cy_stc_capsense_fptr_config_t <structcy__stc__capsense__fptr__config__t.html#a61792eab9d1c4f9b16ee5cbab70345c3>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
