===============================
cy_stc_capsense_touch_t Struct
===============================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares touch structure used to store positions of Touchpad,
         Matrix buttons and Slider widgets.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_stc_capsense_position_t <structcy__stc__capsense__position__t.html>`__
\* 

`ptrPosition <structcy__stc__capsense__touch__t.html#a41f4181caf21c8cc60825a4c88a952c6>`__

 

| Pointer to the array containing the position information.
  `More... <#a41f4181caf21c8cc60825a4c88a952c6>`__

 

uint8_t 

`numPosition <structcy__stc__capsense__touch__t.html#a06915bc608b7177c425659ab61d38513>`__

 

| Total number of detected touches on a widget:
  `More... <#a06915bc608b7177c425659ab61d38513>`__

 

Field Documentation
--------------------

`◆  <#a41f4181caf21c8cc60825a4c88a952c6>`__\ ptrPosition
========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_stc_ca                                                           |
      | psense_position_t <structcy__stc__capsense__position__t.html>`__\ \* |
      | cy_stc_capsense_touch_t::ptrPosition                                 |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Pointer to the array containing the position information.

      A number of elements is defined by numPosition.

`◆  <#a06915bc608b7177c425659ab61d38513>`__\ numPosition
========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------+
      | uint8_t cy_stc_capsense_touch_t::numPosition |
      +----------------------------------------------+

   .. container:: memdoc

      Total number of detected touches on a widget:

      -  0 - no touch is detected
      -  1 - a single touch is detected
      -  2 - two touches are detected
      -  3 - three touches are detected
      -  CY_CAPSENSE_POSITION_MULTIPLE - multiple touches are detected
         and information in position structure should be ignored.

