=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - r -
         :name: r--

      -  raw :
         `cy_stc_capsense_sensor_context_t <structcy__stc__capsense__sensor__context__t.html#a54bbaaded1ac1a6857ce84a996224ccc>`__
      -  rawFilterConfig :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a67d55b8afd94b5ff3de3587a4241c628>`__
      -  regAmbuf :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ae8f8770f51182beb960139917bceb6e3>`__
      -  regAmbufShield :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a2ab8cac868956282420db3b54a7e0978>`__
      -  regConfig :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ae5f47666b04bc546cc716cd77c00446b>`__
      -  regConfigShield :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a1ab075dfb6504435139ec6b7bf68a42b>`__
      -  regHscmpScan :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ac44249ae8e20f8dfb19e42566fad97d0>`__
      -  regHscmpScanShield :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ad6fcce2910023a51b7f479a62d0b12ce>`__
      -  regIoSel :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#aad9f6bc6d2d8f7e2e9843a0013c43f55>`__
      -  regIoSelShield :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a20a4beb0abe24168be77ad9072590b57>`__
      -  regSwAmuxbufSel :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a8e0f79069bf4362e97a1fb82d0166bb6>`__
      -  regSwAmuxbufSelShield :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a82f8b8b8d650377c292bd35b4188f070>`__
      -  regSwBypSel :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a3aeb8661cd85b1aa2f266cee60be0240>`__
      -  regSwBypSelShield :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a68ebea3b267bf015d1e9e9802693a1c7>`__
      -  regSwDsiSel :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ac8babb76107f68ffc3b92736fb5f3a72>`__
      -  regSwHsPSelCmodInit :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a8e341ec1a291ce5bc030fcf60610fca9>`__
      -  regSwHsPSelCtankInit :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a338ca675ef90b7fdb80ab9a95eba5f8d>`__
      -  regSwHsPSelScan :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ae9b8e85cb52436b653b5e7d1fb1e5133>`__
      -  regSwHsPSelScanShield :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a25926bffe9b5af767f6db69988b7a506>`__
      -  regSwRefgenSel :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a8d0e2a3ef5a233e8da6471eedd227f08>`__
      -  regSwResInit :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a20d613bddf0e9ea1dcb6196a579ce414>`__
      -  regSwResScan :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ade86e6ef5b7e98386370aa2c567ee60e>`__
      -  regSwShieldSelScan :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#af8eeec145da8af1555e3bea62c8a2b6a>`__
      -  regSwShieldSelScanShield :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ac1503ac45a53e4be199d38ed77fee26f>`__
      -  reserved0 :
         `cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html#aeac367c4efeec355b75e991a523dfba1>`__
         ,
         `cy_stc_capsense_advanced_touchpad_config_t <structcy__stc__capsense__advanced__touchpad__config__t.html#a76405d828e3531855ab1ae70668d76a6>`__
         ,
         `cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html#af99a6dbf493de76f2c44a7e001075cd8>`__
         ,
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#aaf3e7d3b18b2ecf51031b644db4292a3>`__
      -  reserved1 :
         `cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html#a7b4b8bb5ec1ed763d04cea68627b33f8>`__
         ,
         `cy_stc_capsense_advanced_touchpad_config_t <structcy__stc__capsense__advanced__touchpad__config__t.html#a64845aacc58c2a1007eace4832b5a362>`__
         ,
         `cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html#a69d7a152771429f807f0fb2e3e34998b>`__
         ,
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#accd9ff41ad610ac269d04e3065af52c2>`__
      -  reserved2 :
         `cy_stc_capsense_advanced_touchpad_config_t <structcy__stc__capsense__advanced__touchpad__config__t.html#ab3ff0d6932c836ccb0b0e005578ef00c>`__
         ,
         `cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html#a51724cd248eff5076374e145950157d7>`__
      -  resolution :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a68e0c56527b8e16513c149bfc5adb623>`__
      -  resolutionVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a74ffdaa191f9c9bd3d17ff3170200bac>`__
      -  resolutionX :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#af68f4b04172c275b6be6e12768dc7991>`__
         ,
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#abcdafc8f4ecbb01e2202dee3ed252364>`__
      -  resolutionY :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#a41e2b2272353ff198fca4fbe4397d9bd>`__
         ,
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#ac418cd1cf76d0dcfe90232450954eb82>`__
      -  rotateDebounce :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#aeebc2692f6432fa15394678fb0278a9b>`__
      -  rotateDistanceMin :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#adf604320c16786aa9e89b5f02f02a643>`__
      -  rowIdacMod :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a685987ab8fcf308ba48669866de44274>`__
      -  rowIdacModVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a0c46fa98ffa822712ffe6adcbf05ae03>`__
      -  rowMap :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#a2235820c150af3e6594730e9031066de>`__
      -  rowSnsClk :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a2dd4e90d01407e1c2a0ed33f637e3b4f>`__
      -  rowSnsClkVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#af9f145585e1986461ae4ba26927bbe3c>`__
      -  rxIndex :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#aa4ba26b22efa56788c20fb2c2ce1fb81>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
