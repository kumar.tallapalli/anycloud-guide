=================================================================
Cypress CapSense Middleware Library 2.10: Data Fields - Variables
=================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - o -
         :name: o--

      -  ofcdContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#ab916aaccb1b24a8dc5eee9385d0d2041>`__
      -  ofdcContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a41504e9edd68b6aab487492c95b6a059>`__
      -  ofesContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a34feed1bf649ef82f09da51745d0514e>`__
      -  offlContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a7a6f0b99bddc8e18cfea0fedb39e00b8>`__
      -  ofrtContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a48dfae93de6b33ba60cdd10f8292e590>`__
      -  ofscContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a72755d5985247fd60078900d1482ddfc>`__
      -  ofslContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a7f7257503a55d3790ab222a6ac280a85>`__
      -  oldActiveIdsMask :
         `cy_stc_capsense_csx_touch_history_t <structcy__stc__capsense__csx__touch__history__t.html#a673eaa945ec772627c954df63b42a89e>`__
      -  oldPeak :
         `cy_stc_capsense_csx_touch_history_t <structcy__stc__capsense__csx__touch__history__t.html#a9cd73499eb9138949da6bd8812a654d1>`__
      -  oldPeakNumber :
         `cy_stc_capsense_csx_touch_history_t <structcy__stc__capsense__csx__touch__history__t.html#aad62c7d9b921294d3caff02bcca10734>`__
      -  oldTimestamp :
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#a38d8589f93a3bc5fa8fb53e5076d6da6>`__
      -  oldTouchNumber :
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#a74a898f524e561622e4fb8d5b302fa77>`__
      -  onDebounce :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a594bf2fb432dbeaa9b23545a9ff3d55a>`__
      -  onDebounceVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a656e5777ec25c6e3a3b5b64ba54fc4dc>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
