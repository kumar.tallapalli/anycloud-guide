==========================================
cy_stc_capsense_internal_context_t Struct
==========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares internal Context Data Structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`csdInactiveSnsDm <structcy__stc__capsense__internal__context__t.html#a2f898bf4518847f17b47956a335f6353>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegConfig <structcy__stc__capsense__internal__context__t.html#a904304439ab60e08be051b7f3d41e44d>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwHsPSelScan <structcy__stc__capsense__internal__context__t.html#a589c74e9be04b3416fadad444150da66>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwHsPSelCmodInit <structcy__stc__capsense__internal__context__t.html#af19d498cb29cbaa134653395eafd7ec6>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwHsPSelCtankInit <structcy__stc__capsense__internal__context__t.html#ad2aeacf04ec2d90fa491b0eef451010a>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwBypSel <structcy__stc__capsense__internal__context__t.html#a41e9700e4d9869a223efa4fb8c208011>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwResScan <structcy__stc__capsense__internal__context__t.html#a85cb2b96016d55f3df77b52e9f8a326f>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwResInit <structcy__stc__capsense__internal__context__t.html#aa6a2613fbe056600e359d10d3865c723>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwDsiSel <structcy__stc__capsense__internal__context__t.html#a988ff22fa06616b19a39c39d169a02f0>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegAmuxbufInit <structcy__stc__capsense__internal__context__t.html#a6c28b93814ffaa55e6fb872700b4ca39>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwAmuxbufSel <structcy__stc__capsense__internal__context__t.html#a19d99d39617a9c0e0282028ddd1ba2fe>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwShieldSelScan <structcy__stc__capsense__internal__context__t.html#a0319bc9875f2c2f7cd7eea16cd17b8c2>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegHscmpInit <structcy__stc__capsense__internal__context__t.html#a449bf02b29d79cf2a38e8681041d5552>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegHscmpScan <structcy__stc__capsense__internal__context__t.html#a9b8255706a4d3e0206a7afdbd4373c94>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdIdacAConfig <structcy__stc__capsense__internal__context__t.html#aa322ece65334e5f6bdc305e196c174e4>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdIdacBConfig <structcy__stc__capsense__internal__context__t.html#a60ad882d18e3b4c5ce1137b158ad3676>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwCmpPSel <structcy__stc__capsense__internal__context__t.html#a67205084e46e007d0a6d4c8811047845>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwCmpNSel <structcy__stc__capsense__internal__context__t.html#a14ee87b414f48d9a8c70c11032c823f0>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegIoSel <structcy__stc__capsense__internal__context__t.html#a69a75b1f9b0b9ee96d91fec2150e0b2e>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegRefgen <structcy__stc__capsense__internal__context__t.html#aea1919ff206aa3b96e18e0f584b41e23>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csdRegSwRefGenSel <structcy__stc__capsense__internal__context__t.html#afc2655e619b06ba8b961a434b14d30d1>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegConfigInit <structcy__stc__capsense__internal__context__t.html#a4769ef311a77513a99e8aa77ae0be92e>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegConfigScan <structcy__stc__capsense__internal__context__t.html#a6af1dd5754837d8be8da11a72241cc15>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegSwResInit <structcy__stc__capsense__internal__context__t.html#a1f11ddec8ecd0a2aed8347d237e5495f>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegSwResPrech <structcy__stc__capsense__internal__context__t.html#a0c1d55db8fd5b06a7f94d7ce8fa74966>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegSwResScan <structcy__stc__capsense__internal__context__t.html#a4a22eb64852220cd5ca1197e384dd737>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegAMuxBuf <structcy__stc__capsense__internal__context__t.html#a930ea5e7b3a0ab55e3d245a4c7fcaa44>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegRefgen <structcy__stc__capsense__internal__context__t.html#a43ebfc2e417b5727b9968f3a9f9d5446>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegRefgenSel <structcy__stc__capsense__internal__context__t.html#a1aae8b3da9ccfdc7af195dc760390588>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegSwCmpNSel <structcy__stc__capsense__internal__context__t.html#afdca09ebf3e847d35b4175a20761b09b>`__
 
Internal pre-calculated data for faster operation.
 
uint32_t 
`csxRegSwRefGenSel <structcy__stc__capsense__internal__context__t.html#af4fab337c4d3a776ad5aacf47da8c31f>`__
 
Internal pre-calculated data for faster operation.
 
uint8_t 
`csdCmodConnection <structcy__stc__capsense__internal__context__t.html#a52536fed664d3fc02213044a637d9d80>`__
 
Internal pre-calculated data for faster operation.
 
uint8_t 
`csdCshConnection <structcy__stc__capsense__internal__context__t.html#a3a30b3c133e73edbb7247dde94b8cfd9>`__
 
Internal pre-calculated data for faster operation.
 
en_hsiom_sel_t 
`csdInactiveSnsHsiom <structcy__stc__capsense__internal__context__t.html#a911c7ad42212f5ebc0d82849c27b6e04>`__
 
Internal pre-calculated data for faster operation.
 
uint8_t 
`csdVrefGain <structcy__stc__capsense__internal__context__t.html#a4973e3bb2c5cf514f7950d7bf44268cc>`__
 
Internal pre-calculated data for faster operation.
 
uint16_t 
`csdVrefVoltageMv <structcy__stc__capsense__internal__context__t.html#a30c3792b5d6a3df45fe801712fae87d4>`__
 
Internal pre-calculated data for faster operation.

