==========================================
cy_stc_capsense_gesture_position_t Struct
==========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture position structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`x <structcy__stc__capsense__gesture__position__t.html#aa7c7a9666ddec68e4ace38a030ecfdab>`__
 
X axis position.
 
uint16_t 
`y <structcy__stc__capsense__gesture__position__t.html#a3a1c2f83221a848b380ec4c10362187a>`__
 
Y axis position.

