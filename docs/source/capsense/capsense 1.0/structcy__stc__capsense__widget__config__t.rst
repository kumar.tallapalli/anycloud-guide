=======================================
cy_stc_capsense_widget_config_t Struct
=======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Widget configuration structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
\* 

`ptrWdContext <structcy__stc__capsense__widget__config__t.html#adb558f384dcfa5fe3701f6c377b94da3>`__

 

| Pointer to context structure of this widget.

 

`cy_stc_capsense_sensor_context_t <structcy__stc__capsense__sensor__context__t.html>`__
\* 

`ptrSnsContext <structcy__stc__capsense__widget__config__t.html#aace32d41062b136d802d346231967ad5>`__

 

| Pointer to the first object of sensor context structure that belongs
  to this widget.

 

const
`cy_stc_capsense_electrode_config_t <structcy__stc__capsense__electrode__config__t.html>`__
\* 

`ptrEltdConfig <structcy__stc__capsense__widget__config__t.html#a88a5166ff86cf001344434b0bb84d799>`__

 

| Pointer to the first object of electrode configuration structure that
  belongs to this widget.

 

uint32_t \* 

`ptrEltdCapacitance <structcy__stc__capsense__widget__config__t.html#aeb69ac183634b05ecb92ab57e1b8feb9>`__

 

| Pointer to the first object in the electrode capacitance array that
  belongs to this widget.

 

uint16_t \* 

`ptrBslnInv <structcy__stc__capsense__widget__config__t.html#aa246164662305a0152b2736f56b67e5a>`__

 

| Pointer to the first object in the sensor baseline inversion array
  that belongs to this widget.

 

`cy_stc_capsense_smartsense_csd_noise_envelope_t <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html>`__
\* 

`ptrNoiseEnvelope <structcy__stc__capsense__widget__config__t.html#a8cbf228dd4ae8a901c911c162c70b30a>`__

 

| Pointer to the noise envelope filter used by SmartSense.

 

uint16_t \* 

`ptrRawFilterHistory <structcy__stc__capsense__widget__config__t.html#a3b1c56df207169c04a05b7fa8833b0fc>`__

 

| Pointer to the raw count filter history of the widget.

 

uint8_t \* 

`ptrRawFilterHistoryLow <structcy__stc__capsense__widget__config__t.html#a9c3303699258fdb9e0a1c51ac8542370>`__

 

| Pointer to the raw count filter history extended of the widget.

 

uint32_t 

`iirCoeff <structcy__stc__capsense__widget__config__t.html#a3837d9686917c0fdfc96cadb261dbc03>`__

 

| Raw count IIR filter coefficient.
  `More... <#a3837d9686917c0fdfc96cadb261dbc03>`__

 

uint8_t \* 

`ptrDebounceArr <structcy__stc__capsense__widget__config__t.html#aa353e792734db1ff3873175bb8e832b9>`__

 

| Pointer to the debounce array of the widget.

 

const uint8_t \* 

`ptrDiplexTable <structcy__stc__capsense__widget__config__t.html#a6224ab7605e28404d786633b2c1c1f6b>`__

 

| Pointer to the diplex table used for Linear slider when Diplex option
  is enabled.

 

uint32_t 

`centroidConfig <structcy__stc__capsense__widget__config__t.html#ad2825a42c272b5c46dbcb15a21ec913f>`__

 

| Configuration of centroids.

 

uint16_t 

`xResolution <structcy__stc__capsense__widget__config__t.html#adfae2ef0d4316555d1e3c54e39c4aeb5>`__

 

| Keeps maximum position value.
  `More... <#adfae2ef0d4316555d1e3c54e39c4aeb5>`__

 

uint16_t 

`yResolution <structcy__stc__capsense__widget__config__t.html#ae527a11d29c8deb400a6c2a58d677aee>`__

 

| For Touchpads Y-Axis maximum position.

 

uint16_t 

`numSns <structcy__stc__capsense__widget__config__t.html#ae61718f5e9f507beaff5d105e03659a0>`__

 

| The total number of sensors: For CSD widgets: WD_NUM_ROWS +
  WD_NUM_COLS. `More... <#ae61718f5e9f507beaff5d105e03659a0>`__

 

uint8_t 

`numCols <structcy__stc__capsense__widget__config__t.html#af281dfdef5176c9db5e88cf162686ad4>`__

 

| For CSD Button and Proximity Widgets, the number of sensors.
  `More... <#af281dfdef5176c9db5e88cf162686ad4>`__

 

uint8_t 

`numRows <structcy__stc__capsense__widget__config__t.html#a93e1fecbb0649d67dee102b1ef2cd1b8>`__

 

| For CSD Touchpad and Matrix Buttons, the number of the row sensors.
  `More... <#a93e1fecbb0649d67dee102b1ef2cd1b8>`__

 

`cy_stc_capsense_touch_t <structcy__stc__capsense__touch__t.html>`__ \* 

`ptrPosFilterHistory <structcy__stc__capsense__widget__config__t.html#a6136eead3f476c9c0d1d3fa1dfd8c189>`__

 

| Pointer to the position filter history.

 

`cy_stc_capsense_csx_touch_history_t <structcy__stc__capsense__csx__touch__history__t.html>`__
\* 

`ptrCsxTouchHistory <structcy__stc__capsense__widget__config__t.html#ac662c2d9016717ff89a9fd8c26c97c02>`__

 

| Pointer to the CSX touchpad history.

 

`cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html>`__
\* 

`ptrCsxTouchBuffer <structcy__stc__capsense__widget__config__t.html#a35afcfaedb37e24127fa5642bab59b45>`__

 

| Pointer to the single CSX buffer needed for CSX touchpad processing.

 

uint16_t \* 

`ptrCsdTouchBuffer <structcy__stc__capsense__widget__config__t.html#a8da5973b3062723df1b1ebf6b7f5bd86>`__

 

| Pointer to the CSD buffer needed for advanced CSD touchpad processing.

 

`cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html>`__
\* 

`ptrGestureConfig <structcy__stc__capsense__widget__config__t.html#a0f4c0e442a86b761a8a00cd76ecc83b5>`__

 

| Pointer to Gesture configuration structure.

 

`cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html>`__
\* 

`ptrGestureContext <structcy__stc__capsense__widget__config__t.html#ad60be1a14aded108976eb2d66daac7f1>`__

 

| Pointer to Gesture context structure.

 

`cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html>`__ 

`ballisticConfig <structcy__stc__capsense__widget__config__t.html#a5fc7dffe9f218833d42b73769d0e6965>`__

 

| The configuration data for position ballistic filter.
  `More... <#a5fc7dffe9f218833d42b73769d0e6965>`__

 

`cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html>`__
\* 

`ptrBallisticContext <structcy__stc__capsense__widget__config__t.html#ad510b796c693e08b4f4b8bf8f76c3fb5>`__

 

| Pointer to Ballistic filter context structure.

 

`cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html>`__ 

`aiirConfig <structcy__stc__capsense__widget__config__t.html#a44c45fc836a80c8fe858ab3d5133d751>`__

 

| The configuration of position adaptive filter.
  `More... <#a44c45fc836a80c8fe858ab3d5133d751>`__

 

`cy_stc_capsense_advanced_touchpad_config_t <structcy__stc__capsense__advanced__touchpad__config__t.html>`__ 

`advConfig <structcy__stc__capsense__widget__config__t.html#a94b1400ac7cf425e79664f4e19f54d0b>`__

 

| The configuration of CSD advanced touchpad.

 

uint32_t 

`posFilterConfig <structcy__stc__capsense__widget__config__t.html#a477c8db37f2ff589fa1967272d1f5260>`__

 

| Position filters configuration.

 

uint16_t 

`rawFilterConfig <structcy__stc__capsense__widget__config__t.html#a67d55b8afd94b5ff3de3587a4241c628>`__

 

| Raw count filters configuration.

 

uint8_t 

`senseMethod <structcy__stc__capsense__widget__config__t.html#a417a8786dfe62d3940fd7d5e265b9e2e>`__

 

| Specifies the widget sensing method.

 

uint8_t 

`wdType <structcy__stc__capsense__widget__config__t.html#a78fb9e1d1b8c8cdad2d4257f5881ddc2>`__

 

| Specifies the widget type.

 

Field Documentation
--------------------

`◆  <#a3837d9686917c0fdfc96cadb261dbc03>`__\ iirCoeff
=====================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------+
      | uint32_t cy_stc_capsense_widget_config_t::iirCoeff |
      +----------------------------------------------------+

   .. container:: memdoc

      Raw count IIR filter coefficient.

      Smaller value leads to higher filtering

`◆  <#adfae2ef0d4316555d1e3c54e39c4aeb5>`__\ xResolution
========================================================

.. container:: memitem

   .. container:: memproto

      +-------------------------------------------------------+
      | uint16_t cy_stc_capsense_widget_config_t::xResolution |
      +-------------------------------------------------------+

   .. container:: memdoc

      Keeps maximum position value.

      For Touchpads X-axis maximum position

`◆  <#ae61718f5e9f507beaff5d105e03659a0>`__\ numSns
===================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------+
      | uint16_t cy_stc_capsense_widget_config_t::numSns |
      +--------------------------------------------------+

   .. container:: memdoc

      The total number of sensors: For CSD widgets: WD_NUM_ROWS +
      WD_NUM_COLS.

      For CSX widgets: WD_NUM_ROWS \* WD_NUM_COLS.

`◆  <#af281dfdef5176c9db5e88cf162686ad4>`__\ numCols
====================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------+
      | uint8_t cy_stc_capsense_widget_config_t::numCols |
      +--------------------------------------------------+

   .. container:: memdoc

      For CSD Button and Proximity Widgets, the number of sensors.

      For CSD Slider Widget, the number of segments. For CSD Touchpad
      and Matrix Button, the number of the column sensors. For CSX
      Button, Touchpad, and Matrix Button, the number of the Rx
      electrodes.

`◆  <#a93e1fecbb0649d67dee102b1ef2cd1b8>`__\ numRows
====================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------+
      | uint8_t cy_stc_capsense_widget_config_t::numRows |
      +--------------------------------------------------+

   .. container:: memdoc

      For CSD Touchpad and Matrix Buttons, the number of the row
      sensors.

      For the CSX Button, the number of the Tx electrodes (constant 1u).
      For CSX Touchpad and Matrix Button, the number of the Tx
      electrodes.

`◆  <#a5fc7dffe9f218833d42b73769d0e6965>`__\ ballisticConfig
============================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_stc_capsense_ballis                                              |
      | tic_config_t <structcy__stc__capsense__ballistic__config__t.html>`__ |
      | cy_stc_capsense_widget_config_t::ballisticConfig                     |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The configuration data for position ballistic filter.

`◆  <#a44c45fc836a80c8fe858ab3d5133d751>`__\ aiirConfig
=======================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_stc_capsense_adaptive_filter_con                                 |
      | fig_t <structcy__stc__capsense__adaptive__filter__config__t.html>`__ |
      | cy_stc_capsense_widget_config_t::aiirConfig                          |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The configuration of position adaptive filter.
