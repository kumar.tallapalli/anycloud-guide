=========================================
cy_stc_capsense_widget_crc_data_t Struct
=========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares the structure that is intended to store the
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
         data structure fields, the CRC checking should be applied for.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`fingerThVal <structcy__stc__capsense__widget__crc__data__t.html#a373d9ef4767624a402f7b5212474c2e7>`__
 
The value of the .fingerTh field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint16_t 
`proxThVal <structcy__stc__capsense__widget__crc__data__t.html#ae33b026cd3a82c07e0dcdc80723b0e77>`__
 
The value of the .proxTh field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint16_t 
`fingerCapVal <structcy__stc__capsense__widget__crc__data__t.html#a2c02fa3c77712c71160208f61b1d7f66>`__
 
The value of the .fingerCap field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint16_t 
`sigPFCVal <structcy__stc__capsense__widget__crc__data__t.html#a2742dd33a65b5791a4aca1dd86f60910>`__
 
The value of the .sigPFC field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint16_t 
`resolutionVal <structcy__stc__capsense__widget__crc__data__t.html#a74ffdaa191f9c9bd3d17ff3170200bac>`__
 
The value of the .resolution field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint16_t 
`lowBslnRstVal <structcy__stc__capsense__widget__crc__data__t.html#a1b9cf99918a603703887386a91037fcb>`__
 
The value of the .lowBslnRst field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint16_t 
`snsClkVal <structcy__stc__capsense__widget__crc__data__t.html#a888a88ed2d857e30e18b5240731fafb7>`__
 
The value of the .snsClk field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint16_t 
`rowSnsClkVal <structcy__stc__capsense__widget__crc__data__t.html#af9f145585e1986461ae4ba26927bbe3c>`__
 
The value of the .rowSnsClk field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint8_t 
`onDebounceVal <structcy__stc__capsense__widget__crc__data__t.html#a656e5777ec25c6e3a3b5b64ba54fc4dc>`__
 
The value of the .onDebounce field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint8_t 
`snsClkSourceVal <structcy__stc__capsense__widget__crc__data__t.html#a56160b9d3867b8b59fda1ebface25359>`__
 
The value of the .snsClkSource field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint8_t 
`idacModVal <structcy__stc__capsense__widget__crc__data__t.html#a1d341f57cd4922055d9b81d2ad84836e>`__
[`CY_CAPSENSE_FREQ_CHANNELS_NUM <group__group__capsense__macros__settings.html#gaf7c608a0139fc64fce397a0ac580e191>`__]
 
The value of the .idacMod field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint8_t 
`idacGainIndexVal <structcy__stc__capsense__widget__crc__data__t.html#a16b1ee33055b4f7d5c1d9c6ff0cdade2>`__
 
The value of the .idacGainIndex field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint8_t 
`rowIdacModVal <structcy__stc__capsense__widget__crc__data__t.html#a0c46fa98ffa822712ffe6adcbf05ae03>`__
[`CY_CAPSENSE_FREQ_CHANNELS_NUM <group__group__capsense__macros__settings.html#gaf7c608a0139fc64fce397a0ac580e191>`__]
 
The value of the .rowIdacMod field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint8_t 
`noiseThVal <structcy__stc__capsense__widget__crc__data__t.html#afcffff36e1c704358bfd5815bbb966cc>`__
 
The value of the .noiseTh field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint8_t 
`nNoiseThVal <structcy__stc__capsense__widget__crc__data__t.html#ac483adcaaae309de3da01df5454bb600>`__
 
The value of the .nNoiseTh field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.
 
uint8_t 
`hysteresisVal <structcy__stc__capsense__widget__crc__data__t.html#adaaf53405b8386927c4957cfc3726926>`__
 
The value of the .hysteresis field of the
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
structure.

