=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - z -
         :name: z--

      -  z :
         `cy_stc_capsense_position_t <structcy__stc__capsense__position__t.html#a9a62c41da414fc72932fd1bcd8fbfc96>`__
      -  zoomDebounce :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a13a289ac07a253455c04f3b124e9a7db>`__
      -  zoomDistanceMin :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#ac80c3e733cdc3999e48f02b94c629f05>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
