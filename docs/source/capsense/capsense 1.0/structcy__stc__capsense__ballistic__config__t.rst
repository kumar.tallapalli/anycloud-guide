==========================================
cy_stc_capsense_ballistic_config_t Struct
==========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares Ballistics Multiplier Configuration data structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint8_t 
`accelCoeff <structcy__stc__capsense__ballistic__config__t.html#af168ee9cd6deb06a3dd59c63cd63be27>`__
 
Acceleration Coefficient.
 
uint8_t 
`speedCoeff <structcy__stc__capsense__ballistic__config__t.html#ad5992e4bc35b511b1d49699ead2d5d18>`__
 
Speed Coefficient.
 
uint8_t 
`divisorValue <structcy__stc__capsense__ballistic__config__t.html#a847c20119361422daf67ca877c6ef456>`__
 
Divisor Value.
 
uint8_t 
`speedThresholdX <structcy__stc__capsense__ballistic__config__t.html#ab4deccb79d0803f081e1283ee7006acd>`__
 
Speed Threshold X.
 
uint8_t 
`speedThresholdY <structcy__stc__capsense__ballistic__config__t.html#adb09b97cfc40452a62b7cc4c92dda9a3>`__
 
Speed Threshold Y.
 
uint8_t 
`reserved0 <structcy__stc__capsense__ballistic__config__t.html#af99a6dbf493de76f2c44a7e001075cd8>`__
 
Reserved field.
 
uint8_t 
`reserved1 <structcy__stc__capsense__ballistic__config__t.html#a69d7a152771429f807f0fb2e3e34998b>`__
 
Reserved field.
 
uint8_t 
`reserved2 <structcy__stc__capsense__ballistic__config__t.html#a51724cd248eff5076374e145950157d7>`__
 
Reserved field.


