=================
Enumerated Types
=================


.. doxygengroup:: group_capsense_enums
   :project: capsense1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: