=======================================
cy_stc_capsense_common_config_t Struct
=======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Common configuration structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 

`cpuClkHz <structcy__stc__capsense__common__config__t.html#a53051c09692058cebfe2d5a4faa935c8>`__

 

| CPU clock in Hz.

 

uint32_t 

`periClkHz <structcy__stc__capsense__common__config__t.html#a885d6f703d5687e6585b929f61265f9c>`__

 

| Peripheral clock in Hz.

 

uint16_t 

`vdda <structcy__stc__capsense__common__config__t.html#a04857de80018693091a81dce3b19bd66>`__

 

| VDDA in mV.

 

uint16_t 

`numPin <structcy__stc__capsense__common__config__t.html#af1b66d5becece2209af864547ac16c65>`__

 

| Total number of IOs. `More... <#af1b66d5becece2209af864547ac16c65>`__

 

uint16_t 

`numSns <structcy__stc__capsense__common__config__t.html#a21d3bc1dab9651fed220ebdfead85453>`__

 

| The total number of sensors.
  `More... <#a21d3bc1dab9651fed220ebdfead85453>`__

 

uint8_t 

`numWd <structcy__stc__capsense__common__config__t.html#a4b2745560f410d1a0dc70797aeec3fab>`__

 

| Total number of widgets.

 

uint8_t 

`csdEn <structcy__stc__capsense__common__config__t.html#a7fddda182e82215e18769618b498357c>`__

 

| CSD sensing method enabled, at least one CSD widget is configured.

 

uint8_t 

`csxEn <structcy__stc__capsense__common__config__t.html#a816a9d211280fa1541cb9d79e790926d>`__

 

| CSX sensing method enabled, at least one CSX widget is configured.

 

uint8_t 

`mfsEn <structcy__stc__capsense__common__config__t.html#a4c99796a489c0221243e079cc01ae1d3>`__

 

| Multi-frequency Scan (MFS) enabled.

 

uint8_t 

`positionFilterEn <structcy__stc__capsense__common__config__t.html#a0ff07b677d89662e56814ba7ef8d550f>`__

 

| Position filtering enabled.

 

uint8_t 

`bistEn <structcy__stc__capsense__common__config__t.html#a945e057e66cff3bcae327e897f3ef1a8>`__

 

| Built-in Self-test (BIST) enabled.

 

uint8_t 

`periDividerType <structcy__stc__capsense__common__config__t.html#ab1171e77b08743ec9cb1a0751f3a8a24>`__

 

| Peripheral clock type (8- or 16-bit type)

 

uint8_t 

`periDividerIndex <structcy__stc__capsense__common__config__t.html#af4c7933702ebe6de8d7a51ead347f8dc>`__

 

| Peripheral divider index.

 

uint8_t 

`analogWakeupDelay <structcy__stc__capsense__common__config__t.html#a625ca1e473af912530d0da3225e0ca40>`__

 

| Time needed to establish correct operation of the CSD HW block after
  power up or System Deep Sleep.
  `More... <#a625ca1e473af912530d0da3225e0ca40>`__

 

uint8_t 

`ssIrefSource <structcy__stc__capsense__common__config__t.html#a0c80e26b5fe28518248152c5c9c6681a>`__

 

| Iref source.

 

uint8_t 

`ssVrefSource <structcy__stc__capsense__common__config__t.html#a9d4938569f715d511d5a92c65337903a>`__

 

| Vref source.

 

uint16_t 

`proxTouchCoeff <structcy__stc__capsense__common__config__t.html#af700535efc4ea5d745cad94153d1d9a6>`__

 

| Proximity touch coefficient in percentage used in SmartSense.

 

uint8_t 

`swSensorAutoResetEn <structcy__stc__capsense__common__config__t.html#a16001bf367c6118143ff1ec3511646fb>`__

 

| Sensor auto reset enabled.

 

uint8_t 

`portCmodPadNum <structcy__stc__capsense__common__config__t.html#a4176f5f4c3a0674e8db380c1cd53b077>`__

 

| Number of port of dedicated Cmod pad.

 

uint8_t 

`pinCmodPad <structcy__stc__capsense__common__config__t.html#a5115d84073d7d01cb4885798dcd8e0eb>`__

 

| Position of the dedicated Cmod pad in the port.

 

uint8_t 

`portCshPadNum <structcy__stc__capsense__common__config__t.html#a6f12b37bfb79c92ac49be9d9a10e1c85>`__

 

| Number of port of dedicated Csh pad.

 

uint8_t 

`pinCshPad <structcy__stc__capsense__common__config__t.html#affacfb2ff9e57a616462f673603d2da9>`__

 

| Position of the dedicated Csh pad in the port.

 

uint8_t 

`portShieldPadNum <structcy__stc__capsense__common__config__t.html#a0757058df63bf046d385f86f141235f2>`__

 

| Number of port of dedicated Shield pad.

 

uint8_t 

`pinShieldPad <structcy__stc__capsense__common__config__t.html#a408b1a89c4eb3f5aec4a24ba2d9942d2>`__

 

| Position of the dedicated Shield pad in the port.

 

uint8_t 

`portVrefExtPadNum <structcy__stc__capsense__common__config__t.html#acd41888daad97f1aa24dfbca213431a6>`__

 

| Number of port of dedicated VrefExt pad.

 

uint8_t 

`pinVrefExtPad <structcy__stc__capsense__common__config__t.html#aff2a6b86b567e1b1c7328dae74aa4c0d>`__

 

| Position of the dedicated VrefExt pad in the port.

 

uint8_t 

`portCmodNum <structcy__stc__capsense__common__config__t.html#ae11a796b0265b7de228505259d4b8a98>`__

 

| Number of port of Cmod pin.

 

`cy_stc_capsense_idac_gain_table_t <structcy__stc__capsense__idac__gain__table__t.html>`__ 

`idacGainTable <structcy__stc__capsense__common__config__t.html#aa420b265a50ab175b9a0902bcc3a0b08>`__
[`CY_CAPSENSE_IDAC_GAIN_NUMBER <group__group__capsense__macros__miscellaneous.html#gabf3b4c535607496679a9ba710bcf91ff>`__]

 

| Table with the supported IDAC gains and corresponding register values.

 

CSD_Type \* 

`ptrCsdBase <structcy__stc__capsense__common__config__t.html#a314b8ab78e6a189daeb4d150d3f51512>`__

 

| Pointer to the CSD HW block register.

 

cy_stc_csd_context_t \* 

`ptrCsdContext <structcy__stc__capsense__common__config__t.html#a633cc2ca6a5528f5a82074e889287b1c>`__

 

| Pointer to the CSD driver context.

 

GPIO_PRT_Type \* 

`portCmod <structcy__stc__capsense__common__config__t.html#ab76476aba113baea0f471f559a36a99c>`__

 

| Pointer to the base port register of the Cmod pin.

 

GPIO_PRT_Type \* 

`portCsh <structcy__stc__capsense__common__config__t.html#ab8b3fc9607ea9d6a5902e69f6fa555eb>`__

 

| Pointer to the base port register of the Csh pin.

 

GPIO_PRT_Type \* 

`portCintA <structcy__stc__capsense__common__config__t.html#a325bae184f92fdb4eface75f45491a0a>`__

 

| Pointer to the base port register of the CintA pin.

 

GPIO_PRT_Type \* 

`portCintB <structcy__stc__capsense__common__config__t.html#ae8e8c568ea38fc8fde93b2dadc5e83f9>`__

 

| Pointer to the base port register of the CintB pin.

 

uint8_t 

`pinCmod <structcy__stc__capsense__common__config__t.html#a552e76fb483b1aef4e079c6a08343622>`__

 

| Position of the Cmod pin in the port.

 

uint8_t 

`portCshNum <structcy__stc__capsense__common__config__t.html#a72dcc792179198a2c8a7a92b38c5af15>`__

 

| Number of port of Csh pin.

 

uint8_t 

`pinCsh <structcy__stc__capsense__common__config__t.html#a1894d1621d3d0b85dcecb541580086b2>`__

 

| Position of the Csh pin in the port.

 

uint8_t 

`pinCintA <structcy__stc__capsense__common__config__t.html#a511bdf3dac96f6d21a99570a0d631a18>`__

 

| Position of the CintA pin in the port.

 

uint8_t 

`pinCintB <structcy__stc__capsense__common__config__t.html#ae19eb70caeeb497808e72757c4dc4d97>`__

 

| Position of the CintB pin in the port.

 

uint8_t 

`csdShieldEn <structcy__stc__capsense__common__config__t.html#ad8fa7f6bb76ee337e4c8650a105e30f4>`__

 

| Shield enabled.

 

uint8_t 

`csdInactiveSnsConnection <structcy__stc__capsense__common__config__t.html#abe52b6e2f195fc5f638808538c0c0df5>`__

 

| Inactive sensor connection state:
  `More... <#abe52b6e2f195fc5f638808538c0c0df5>`__

 

uint8_t 

`csdShieldDelay <structcy__stc__capsense__common__config__t.html#ae4bc5430219c2d2613941168464acc96>`__

 

| Shield signal delay.

 

uint16_t 

`csdVref <structcy__stc__capsense__common__config__t.html#ab982977927531f92c5ac748fa316c06f>`__

 

| Vref for CSD method.

 

uint16_t 

`csdRConst <structcy__stc__capsense__common__config__t.html#a3023ed9a75800748f3af86571b42a5b0>`__

 

| Sensor resistance in series used by SmartSense.

 

uint8_t 

`csdCTankShieldEn <structcy__stc__capsense__common__config__t.html#a74c2e88f6c338ad404e26b6e11e5bbc7>`__

 

| Csh enabled.

 

uint8_t 

`csdShieldNumPin <structcy__stc__capsense__common__config__t.html#add23287dd34a8fdbf49d4e4bfaf9600a>`__

 

| Number of shield IOs.

 

uint8_t 

`csdShieldSwRes <structcy__stc__capsense__common__config__t.html#a9d116265ff9d889b1b76fa80ff292e32>`__

 

| Shield switch resistance.

 

uint8_t 

`csdInitSwRes <structcy__stc__capsense__common__config__t.html#adc75f58a3536e80f4aeddbd1b749ff7b>`__

 

| Switch resistance at coarse initialization.

 

uint8_t 

`csdChargeTransfer <structcy__stc__capsense__common__config__t.html#a5e64ba40e4bc04161f1cfff08365b55b>`__

 

| IDAC sensing configuration.

 

uint8_t 

`csdRawTarget <structcy__stc__capsense__common__config__t.html#ab21e95353a8fa1abae498d2a110db030>`__

 

| Raw count target in percentage for CSD calibration.

 

uint8_t 

`csdAutotuneEn <structcy__stc__capsense__common__config__t.html#a46dabab191776af278a263e931f0c77b>`__

 

| SmartSense enabled.

 

uint8_t 

`csdIdacAutocalEn <structcy__stc__capsense__common__config__t.html#a2ba2ca9a903e0f7414351629b25f0e92>`__

 

| CSD IDAC calibration enabled.

 

uint8_t 

`csdIdacGainInitIndex <structcy__stc__capsense__common__config__t.html#a50f450472fd4ff69bbfcc992cd1e0a03>`__

 

| IDAC gain index per
  `idacGainTable <structcy__stc__capsense__common__config__t.html#aa420b265a50ab175b9a0902bcc3a0b08>`__.

 

uint8_t 

`csdIdacAutoGainEn <structcy__stc__capsense__common__config__t.html#a5302a149824787161c2695232b32c9fa>`__

 

| IDAC gain auto-calibration enabled.

 

uint8_t 

`csdCalibrationError <structcy__stc__capsense__common__config__t.html#a83913223b272b76642a87af8b4e78153>`__

 

| Acceptable calibration error.

 

uint8_t 

`csdIdacMin <structcy__stc__capsense__common__config__t.html#a4fbf20312d5350474cd3b1c6c90c1ce8>`__

 

| Min acceptable IDAC value in CSD calibration.

 

uint8_t 

`csdIdacCompEn <structcy__stc__capsense__common__config__t.html#a437d937b2f790a23c4009af497bd3e35>`__

 

| Compensation IDAC enabled.

 

uint8_t 

`csdFineInitTime <structcy__stc__capsense__common__config__t.html#a937ca9a7b6a86c4eb8969b4ea1b9fa8f>`__

 

| Number of dummy SnsClk periods at fine initialization.

 

uint8_t 

`csdIdacRowColAlignEn <structcy__stc__capsense__common__config__t.html#a3abde8bd7d00d217b698b202dcd2bb34>`__

 

| Row-Column alignment enabled.
  `More... <#a3abde8bd7d00d217b698b202dcd2bb34>`__

 

uint8_t 

`csdMfsDividerOffsetF1 <structcy__stc__capsense__common__config__t.html#a2086ec7840bcff2a38a1f4dbc96297ee>`__

 

| Frequency divider offset for channel 1.
  `More... <#a2086ec7840bcff2a38a1f4dbc96297ee>`__

 

uint8_t 

`csdMfsDividerOffsetF2 <structcy__stc__capsense__common__config__t.html#ae84b54901b2a622357902f34444502ca>`__

 

| Frequency divider offset for channel 2.
  `More... <#ae84b54901b2a622357902f34444502ca>`__

 

uint8_t 

`csxRawTarget <structcy__stc__capsense__common__config__t.html#aa5656822ae92239213960e8c8e8e5d8e>`__

 

| Raw count target in percentage for CSX calibration.

 

uint8_t 

`csxIdacGainInitIndex <structcy__stc__capsense__common__config__t.html#a0cd8747f6c21cb813ecd18edf9d54229>`__

 

| IDAC gain for CSX method.

 

uint8_t 

`csxRefGain <structcy__stc__capsense__common__config__t.html#ac5354e52ee64d8d1afac351a9d8a0428>`__

 

| Refgen gain for CSX method.

 

uint8_t 

`csxIdacAutocalEn <structcy__stc__capsense__common__config__t.html#a0bb810d87bef97c73582aeb5e44f86cc>`__

 

| CSX IDAC calibration enabled.

 

uint8_t 

`csxCalibrationError <structcy__stc__capsense__common__config__t.html#a0be983572fd16014ccef16c90b3521e7>`__

 

| Acceptable calibration error.

 

uint8_t 

`csxFineInitTime <structcy__stc__capsense__common__config__t.html#a5fc5e888dc56992a0c194b4c425a8a5d>`__

 

| Number of dummy TX periods at fine initialization.

 

uint8_t 

`csxInitSwRes <structcy__stc__capsense__common__config__t.html#aaa99b5e4cc5ed3211dc14941064a082a>`__

 

| Switch resistance at fine initialization.

 

uint8_t 

`csxScanSwRes <structcy__stc__capsense__common__config__t.html#a01c2adce03bd2616b2d1c7acd13a158d>`__

 

| Switch resistance at scanning.

 

uint8_t 

`csxInitShieldSwRes <structcy__stc__capsense__common__config__t.html#afde18fd9f1dfb9d859cd24ec0814a57c>`__

 

| Switch resistance at fine initialization.

 

uint8_t 

`csxScanShieldSwRes <structcy__stc__capsense__common__config__t.html#aa6917c3c8a71cb3c44e47cbe32742de5>`__

 

| Switch resistance at scanning.

 

uint8_t 

`csxMfsDividerOffsetF1 <structcy__stc__capsense__common__config__t.html#a07e6050b71ba83dd4a5baf3e1380956f>`__

 

| Frequency divider offset for channel 1.
  `More... <#a07e6050b71ba83dd4a5baf3e1380956f>`__

 

uint8_t 

`csxMfsDividerOffsetF2 <structcy__stc__capsense__common__config__t.html#a9b7eca3dd5ff0f135ce8785fa3fd4f2f>`__

 

| Frequency divider offset for channel 2.
  `More... <#a9b7eca3dd5ff0f135ce8785fa3fd4f2f>`__

 

Field Documentation
--------------------

`◆  <#af1b66d5becece2209af864547ac16c65>`__\ numPin
===================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------+
      | uint16_t cy_stc_capsense_common_config_t::numPin |
      +--------------------------------------------------+

   .. container:: memdoc

      Total number of IOs.

`◆  <#a21d3bc1dab9651fed220ebdfead85453>`__\ numSns
===================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------+
      | uint16_t cy_stc_capsense_common_config_t::numSns |
      +--------------------------------------------------+

   .. container:: memdoc

      The total number of sensors.

      It is equal to the number of objects with raw count.

      -  For CSD widgets: WD_NUM_ROWS + WD_NUM_COLS
      -  For CSX widgets: WD_NUM_ROWS \* WD_NUM_COLS

`◆  <#a625ca1e473af912530d0da3225e0ca40>`__\ analogWakeupDelay
==============================================================

.. container:: memitem

   .. container:: memproto

      +------------------------------------------------------------+
      | uint8_t cy_stc_capsense_common_config_t::analogWakeupDelay |
      +------------------------------------------------------------+

   .. container:: memdoc

      Time needed to establish correct operation of the CSD HW block
      after power up or System Deep Sleep.

`◆  <#abe52b6e2f195fc5f638808538c0c0df5>`__\ csdInactiveSnsConnection
=====================================================================

.. container:: memitem

   .. container:: memproto

      +-------------------------------------------------------------------+
      | uint8_t cy_stc_capsense_common_config_t::csdInactiveSnsConnection |
      +-------------------------------------------------------------------+

   .. container:: memdoc

      Inactive sensor connection state:

      -  CY_CAPSENSE_SNS_CONNECTION_HIGHZ
      -  CY_CAPSENSE_SNS_CONNECTION_SHIELD
      -  CY_CAPSENSE_SNS_CONNECTION_GROUND

`◆  <#a3abde8bd7d00d217b698b202dcd2bb34>`__\ csdIdacRowColAlignEn
=================================================================

.. container:: memitem

   .. container:: memproto

      +---------------------------------------------------------------+
      | uint8_t cy_stc_capsense_common_config_t::csdIdacRowColAlignEn |
      +---------------------------------------------------------------+

   .. container:: memdoc

      Row-Column alignment enabled.

      It adjusts modulator IDAC for rows and for columns to achieve the
      similar sensitivity

`◆  <#a2086ec7840bcff2a38a1f4dbc96297ee>`__\ csdMfsDividerOffsetF1
==================================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------+
      | uint8_t cy_stc_capsense_common_config_t::csdMfsDividerOffsetF1 |
      +----------------------------------------------------------------+

   .. container:: memdoc

      Frequency divider offset for channel 1.

      This value is added to base (channel 0) SnsClk divider to form
      channel 1 frequency

`◆  <#ae84b54901b2a622357902f34444502ca>`__\ csdMfsDividerOffsetF2
==================================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------+
      | uint8_t cy_stc_capsense_common_config_t::csdMfsDividerOffsetF2 |
      +----------------------------------------------------------------+

   .. container:: memdoc

      Frequency divider offset for channel 2.

      This value is added to base (channel 0) SnsClk divider to form
      channel 2 frequency

`◆  <#a07e6050b71ba83dd4a5baf3e1380956f>`__\ csxMfsDividerOffsetF1
==================================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------+
      | uint8_t cy_stc_capsense_common_config_t::csxMfsDividerOffsetF1 |
      +----------------------------------------------------------------+

   .. container:: memdoc

      Frequency divider offset for channel 1.

      This value is added to base (channel 0) Tx divider to form channel
      1 frequency

`◆  <#a9b7eca3dd5ff0f135ce8785fa3fd4f2f>`__\ csxMfsDividerOffsetF2
==================================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------+
      | uint8_t cy_stc_capsense_common_config_t::csxMfsDividerOffsetF2 |
      +----------------------------------------------------------------+

   .. container:: memdoc

      Frequency divider offset for channel 2.

      This value is added to base (channel 0) Tx divider to form channel
      2 frequency

