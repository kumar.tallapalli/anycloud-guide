======================================
cy_stc_capsense_ofrt_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture One Finger Rotate context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__ofrt__context__t.html#a3655e2f6e6fccd33a99ae0919965a02d>`__
 
Touchdown position.
 
uint8_t 
`state <structcy__stc__capsense__ofrt__context__t.html#a15e10b8b2d960028f26f3781bd86d1b2>`__
 
Gesture state.
 
uint8_t 
`history <structcy__stc__capsense__ofrt__context__t.html#a4df4b5e9a02cba4b6bba8e6c10c2c68e>`__
 
History of detected movements.
 
uint8_t 
`debounce <structcy__stc__capsense__ofrt__context__t.html#a3d14b869436d9a7a23c57db47a665140>`__
 
Gesture debounce counter.

