==================================================
cy_stc_capsense_advanced_touchpad_config_t Struct
==================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Configuration structure of advanced touchpad.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 

`penultimateTh <structcy__stc__capsense__advanced__touchpad__config__t.html#a990aa2b11b9bf7779a3de1eff80ded85>`__

 

| Defines a threshold for determining arrival at edges.
  `More... <#a990aa2b11b9bf7779a3de1eff80ded85>`__

 

uint16_t 

`virtualSnsTh <structcy__stc__capsense__advanced__touchpad__config__t.html#ade2331205a63402295830fb1ed8bd3a7>`__

 

| Defines a virtual sensor signal.
  `More... <#ade2331205a63402295830fb1ed8bd3a7>`__

 

uint8_t 

`crossCouplingTh <structcy__stc__capsense__advanced__touchpad__config__t.html#ad86060f2b4310c6150e555277c209791>`__

 

| Defines cross coupling threshold.
  `More... <#ad86060f2b4310c6150e555277c209791>`__

 

uint8_t 

`reserved0 <structcy__stc__capsense__advanced__touchpad__config__t.html#a76405d828e3531855ab1ae70668d76a6>`__

 

| Reserved field.

 

uint8_t 

`reserved1 <structcy__stc__capsense__advanced__touchpad__config__t.html#a64845aacc58c2a1007eace4832b5a362>`__

 

| Reserved field.

 

uint8_t 

`reserved2 <structcy__stc__capsense__advanced__touchpad__config__t.html#ab3ff0d6932c836ccb0b0e005578ef00c>`__

 

| Reserved field.

 

Field Documentation
--------------------

`◆  <#a990aa2b11b9bf7779a3de1eff80ded85>`__\ penultimateTh
==========================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------------------------+
      | uint16_t cy_stc_capsense_advanced_touchpad_config_t::penultimateTh |
      +--------------------------------------------------------------------+

   .. container:: memdoc

      Defines a threshold for determining arrival at edges.

      This value may have to be increased for small diamonds, so that
      the edge handling is initiated sooner. If this number is too high,
      there is jumping at the edge with a smaller finger. If this number
      is too low, there is jumping at the edge with a larger finger.

`◆  <#ade2331205a63402295830fb1ed8bd3a7>`__\ virtualSnsTh
=========================================================

.. container:: memitem

   .. container:: memproto

      +-------------------------------------------------------------------+
      | uint16_t cy_stc_capsense_advanced_touchpad_config_t::virtualSnsTh |
      +-------------------------------------------------------------------+

   .. container:: memdoc

      Defines a virtual sensor signal.

      This value should be set to the value of any sensor when a
      medium-sized finger is placed directly over it. If this value is
      too high, a position is reported nearer the edge than ideal
      position. If this value is too low, a position is reported nearer
      the middle of touchpad.

`◆  <#ad86060f2b4310c6150e555277c209791>`__\ crossCouplingTh
============================================================

.. container:: memitem

   .. container:: memproto

      +---------------------------------------------------------------------+
      | uint8_t cy_stc_capsense_advanced_touchpad_config_t::crossCouplingTh |
      +---------------------------------------------------------------------+

   .. container:: memdoc

      Defines cross coupling threshold.

      It is subtracted from sensor signals at centroid position
      calculation to improve the accuracy. The threshold should be equal
      to a sensor signal when your finger is near the sensor, but not
      touching the sensor. This can be determined by slowly dragging
      your finger across the panel and finding the inflection point of
      the difference counts at the base of the curve. The difference
      value at this point should be the Cross-coupling threshold.

