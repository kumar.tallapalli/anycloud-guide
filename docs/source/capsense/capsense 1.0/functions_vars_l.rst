=================================================================
Cypress CapSense Middleware Library 2.10: Data Fields - Variables
=================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - l -
         :name: l--

      -  largeMovTh :
         `cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html#a61cd3ed526985b0b9d6c2a8a10e15c45>`__
      -  linksMap :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#a7046ab5dc8799356f0cc4730c855ed4a>`__
      -  littleMovTh :
         `cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html#abee594ea7f9a29ee29651de847e43f86>`__
      -  lowBslnRst :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#abcf89959f49d28d2d396d8beaae05710>`__
      -  lowBslnRstVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#a1b9cf99918a603703887386a91037fcb>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
