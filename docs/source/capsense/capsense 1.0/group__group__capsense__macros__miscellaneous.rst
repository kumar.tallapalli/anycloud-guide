=====================
Miscellaneous Macros
=====================


.. doxygengroup:: group_capsense_macros_miscellaneous
   :project: capsense1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: