======================================
cy_stc_capsense_tfsl_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture Two Finger Scroll context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__tfsl__context__t.html#aaf2bf408c8cea2a3ad8331676a487aa3>`__
 
Touchdown position of the first touch.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition2 <structcy__stc__capsense__tfsl__context__t.html#abda60903e50cdc56ca4bc60476c2fedf>`__
 
Touchdown position of the second touch.
 
uint8_t 
`state <structcy__stc__capsense__tfsl__context__t.html#aff4bf63c794f8b5f1c07d5473dbfa670>`__
 
Gesture state.
 
uint8_t 
`debounce <structcy__stc__capsense__tfsl__context__t.html#a716e4c137b9094a50dd9c85cb46327ea>`__
 
Gesture debounce counter.
 
uint8_t 
`direction <structcy__stc__capsense__tfsl__context__t.html#af33814e8e0f9b68b402b92868d440997>`__
 
Last reported direction.

