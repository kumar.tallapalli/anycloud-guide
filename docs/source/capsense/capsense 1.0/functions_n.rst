=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - n -
         :name: n--

      -  negBslnRstCnt :
         `cy_stc_capsense_sensor_context_t <structcy__stc__capsense__sensor__context__t.html#a0dfea7d7b4a9b3858a203a54df8bc3fc>`__
      -  newActiveIdsMask :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#a9267df7cd343b44ee9dddfdf64a4e210>`__
      -  newPeak :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#a8ede0076f02b01e43bdf7d01a90e5d8b>`__
      -  newPeakNumber :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#a497b8e96e84f765fc258f34f071aa999>`__
      -  nNoiseTh :
         `cy_stc_capsense_smartsense_update_thresholds_t <structcy__stc__capsense__smartsense__update__thresholds__t.html#a0e03cc2b5dcfb072e46e2d4a6afb2ef6>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a6498ae887b0e0a6d8a174e79ed5aacf5>`__
      -  nNoiseThVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#ac483adcaaae309de3da01df5454bb600>`__
      -  noiseTh :
         `cy_stc_capsense_smartsense_update_thresholds_t <structcy__stc__capsense__smartsense__update__thresholds__t.html#a6278afa4506e0f81410444bc8bd4ef6e>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a8bd80c8460e60bc9437e229329ce63fc>`__
      -  noiseThVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#afcffff36e1c704358bfd5815bbb966cc>`__
      -  noMovTh :
         `cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html#ab838a235c299b6d9ea57bd3fc41568d8>`__
      -  numCols :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#af281dfdef5176c9db5e88cf162686ad4>`__
      -  numPin :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#af1b66d5becece2209af864547ac16c65>`__
      -  numPins :
         `cy_stc_capsense_electrode_config_t <structcy__stc__capsense__electrode__config__t.html#a2ff8c3a5d8d8bd2fe260cf353064f54e>`__
      -  numPosition :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a5f96cbd3fde2fc2007499bdfc5e8ad59>`__
         ,
         `cy_stc_capsense_touch_t <structcy__stc__capsense__touch__t.html#a06915bc608b7177c425659ab61d38513>`__
      -  numPositionLast :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#adfc86db072daa3b309979efb5768456d>`__
      -  numRows :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a93e1fecbb0649d67dee102b1ef2cd1b8>`__
      -  numSns :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a21d3bc1dab9651fed220ebdfead85453>`__
         ,
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#ae61718f5e9f507beaff5d105e03659a0>`__
      -  numWd :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a4b2745560f410d1a0dc70797aeec3fab>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
