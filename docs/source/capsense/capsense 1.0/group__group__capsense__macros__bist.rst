==========================
Built-in Self-test Macros
==========================


.. doxygengroup:: group_capsense_macros_bist
   :project: capsense1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: