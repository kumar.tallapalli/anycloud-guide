=================================================================
Cypress CapSense Middleware Library 2.10: Data Fields - Variables
=================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - v -
         :name: v--

      -  vdda :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a04857de80018693091a81dce3b19bd66>`__
      -  vddaAcqCycles :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a5d877d6bd67e8cf57bcb411cbf684abe>`__
      -  vddaAzCycles :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a069d1684c20cb8b73c04c4c5c109773e>`__
      -  vddaIdacDefault :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#aa06208613dd9f24d6e61d762b57b7d5a>`__
      -  vddaModClk :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#aa97a347c3230f3b8227df50a734e487c>`__
      -  vddaVoltage :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#acae5fb65a370e8e1c14d13c2da578f12>`__
      -  vddaVrefGain :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ada60c4a93a43cb77e509556a97962655>`__
      -  vddaVrefMv :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a596d6e49bff364072a0ab877e14c90d5>`__
      -  velocity :
         `cy_stc_capsense_csx_touch_history_t <structcy__stc__capsense__csx__touch__history__t.html#a5a32e8b0644a7bc98f45a4b345188810>`__
      -  virtualSnsTh :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#a99998bdcb8adfc186748f5e8ecff0724>`__
         ,
         `cy_stc_capsense_advanced_touchpad_config_t <structcy__stc__capsense__advanced__touchpad__config__t.html#ade2331205a63402295830fb1ed8bd3a7>`__
      -  visitedMap :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#a0c72b5063ccfa7e97c086c06d943ffc9>`__
      -  vRef :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#a50522d0350068cbfba0aeebaff115d9c>`__
      -  vrefGain :
         `cy_stc_capsense_bist_custom_parameters_t <structcy__stc__capsense__bist__custom__parameters__t.html#aa24f7a01ccebf1ad20e600ca27565eac>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
