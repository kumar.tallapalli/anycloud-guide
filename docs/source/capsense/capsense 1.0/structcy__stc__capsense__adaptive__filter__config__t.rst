================================================
cy_stc_capsense_adaptive_filter_config_t Struct
================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares Adaptive Filter configuration parameters.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint8_t 
`maxK <structcy__stc__capsense__adaptive__filter__config__t.html#a917e444f748280dc2e325f062600e878>`__
 
Maximum filter coefficient.
 
uint8_t 
`minK <structcy__stc__capsense__adaptive__filter__config__t.html#a8202d5f56b17bd01a1957a05bea2f489>`__
 
Minimum filter coefficient.
 
uint8_t 
`noMovTh <structcy__stc__capsense__adaptive__filter__config__t.html#ab838a235c299b6d9ea57bd3fc41568d8>`__
 
No-movement threshold.
 
uint8_t 
`littleMovTh <structcy__stc__capsense__adaptive__filter__config__t.html#abee594ea7f9a29ee29651de847e43f86>`__
 
Little movement threshold.
 
uint8_t 
`largeMovTh <structcy__stc__capsense__adaptive__filter__config__t.html#a61cd3ed526985b0b9d6c2a8a10e15c45>`__
 
Large movement threshold.
 
uint8_t 
`divVal <structcy__stc__capsense__adaptive__filter__config__t.html#a9a1c907add092d6754cb3d5669b26b19>`__
 
Divisor value.
 
uint8_t 
`reserved0 <structcy__stc__capsense__adaptive__filter__config__t.html#aeac367c4efeec355b75e991a523dfba1>`__
 
Reserved field.
 
uint8_t 
`reserved1 <structcy__stc__capsense__adaptive__filter__config__t.html#a7b4b8bb5ec1ed763d04cea68627b33f8>`__
 
Reserved field.
