======================================
cy_stc_capsense_ofsl_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture One Finger Scroll context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__ofsl__context__t.html#ab6b900847f84996ed25c000469c6cba4>`__
 
Touchdown position.
 
uint8_t 
`state <structcy__stc__capsense__ofsl__context__t.html#a7df429d86ffa5f133a64f3fa4a9c63bc>`__
 
Gesture state.
 
uint8_t 
`debounce <structcy__stc__capsense__ofsl__context__t.html#a8b38d4d85c4c429162a82f66494bab4c>`__
 
Gesture debounce counter.
 
uint8_t 
`direction <structcy__stc__capsense__ofsl__context__t.html#a16e86b0e4a4e18f39d1d04af5f19f77b>`__
 
Last reported direction.
 

