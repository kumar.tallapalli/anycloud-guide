=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - x -
         :name: x--

      -  x :
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#ae07addb7a565119d5b2244a5bd64dde6>`__
         ,
         `cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html#aa7c7a9666ddec68e4ace38a030ecfdab>`__
         ,
         `cy_stc_capsense_position_t <structcy__stc__capsense__position__t.html#aaa333fb1aa5285e28ffdf6cc47248741>`__
      -  xDelta :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a723b1a11630c801ad30546fd35cec715>`__
      -  xResolution :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#adfae2ef0d4316555d1e3c54e39c4aeb5>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
