===========================================
Low Power Assistant Middleware Library 2.0
===========================================

.. doxygengroup:: index
   :project: lpa 1.1
   :members:

.. toctree::
   
   group__lpautilities.rst