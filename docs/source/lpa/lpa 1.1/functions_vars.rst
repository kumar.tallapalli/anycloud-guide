===================================================================
Data Fields - Variables
===================================================================


.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - a -
         :name: a--

      -  acknum :
         `sock_seq_t <structsock__seq__t.html#a74d598287c7c57706b4f158708282bd6>`__
      -  awake_enable_mask :
         `arp_ol_cfg_t <structarp__ol__cfg__t.html#a6e087be3912880808303389ae172ab1d>`__

      .. rubric:: - b -
         :name: b--

      -  bits :
         `cy_pf_ol_cfg_t <structcy__pf__ol__cfg__t.html#a100ee622db376958b1ba24e114fd2bb9>`__

      .. rubric:: - c -
         :name: c--

      -  cfg :
         `ol_desc_t <structol__desc__t.html#ae2465ecd5a2e0acd0dc6977405542194>`__
         ,
         `pf_ol_t <structpf__ol__t.html#a914b22be2a3c8240fecd61895f36c0b7>`__
         ,
         `tko_ol_t <structtko__ol__t.html#ab9645b98cf75b8e3c694f20e7dbd0cc3>`__
      -  config :
         `arp_ol_t <structarp__ol__t.html#a80b1b8f2693769dc8a768d4ecbd06ee4>`__

      .. rubric:: - d -
         :name: d--

      -  deinit :
         `ol_fns_t <structol__fns.html#a79f79f19d34ce0bfaff153744cdb994d>`__
      -  direction :
         `cy_pf_port_t <structcy__pf__port__t.html#ab4abd771b871b449f9002692206860c3>`__
      -  dst_mac :
         `sock_seq_t <structsock__seq__t.html#a731fd9832c22bb23f1ca2ff5cee5c9d1>`__
      -  dstip :
         `sock_seq_t <structsock__seq__t.html#a1e03cb3c0bea2ed3cad3c3eac7e5c2c0>`__
      -  dstport :
         `sock_seq_t <structsock__seq__t.html#a0055efec1a4faedb80ecba0efc2a81ab>`__

      .. rubric:: - e -
         :name: e--

      -  eth :
         `cy_pf_ol_cfg_t <structcy__pf__ol__cfg__t.html#ac483ab624a58dd622c849221bd6d2077>`__
      -  eth_type :
         `cy_pf_ethtype_cfg_t <structcy__pf__ethtype__cfg__t.html#a6b8fe67c64577674f6870fa084512df1>`__

      .. rubric:: - f -
         :name: f--

      -  feature :
         `cy_pf_ol_cfg_t <structcy__pf__ol__cfg__t.html#ad05225c631490dd3b7a2fd8246db5d66>`__
      -  fns :
         `ol_desc_t <structol__desc__t.html#a38e8e98322cb9bf5373cb635a3a4e58a>`__

      .. rubric:: - i -
         :name: i--

      -  id :
         `cy_pf_ol_cfg_t <structcy__pf__ol__cfg__t.html#a5be6ec05952a01857c89018012cf2574>`__
      -  init :
         `ol_fns_t <structol__fns.html#a20426e784f4c9470faa6bf9bff362491>`__
      -  interval :
         `cy_tko_ol_cfg_t <structcy__tko__ol__cfg__t.html#ac869b5fce49f3fb81b86b0998e40c930>`__
      -  ip :
         `cy_pf_ol_cfg_t <structcy__pf__ol__cfg__t.html#a7f55d2ae11f266560d578f9bd8da9731>`__
         ,
         `ol_info_t <structol__info__t.html#ad7bf118a31201493e8f5c3dd550981d6>`__
      -  ip_address :
         `arp_ol_t <structarp__ol__t.html#a8ade24a1d65f167cd373b8222ebc421b>`__
      -  ip_type :
         `cy_pf_ip_cfg_t <structcy__pf__ip__cfg__t.html#ad1a118337de3c5303357615d5c1d2505>`__

      .. rubric:: - l -
         :name: l--

      -  local_port :
         `cy_tko_ol_connect_t <structcy__tko__ol__connect__t.html#a0c22be2daaa3a4baa61bccc472d7b245>`__

      .. rubric:: - n -
         :name: n--

      -  name :
         `arp_ol_t <structarp__ol__t.html#a5c5d17fd2782ce52a39ebe95c985a849>`__
         ,
         `ol_desc_t <structol__desc__t.html#aa710e639017abd5fa8b0bf08f255935f>`__

      .. rubric:: - o -
         :name: o--

      -  ol :
         `ol_desc_t <structol__desc__t.html#a34c59e179d655f125e8613c0136f97ce>`__
      -  ol_info :
         `olm_t <structolm__t.html#aee9525f191806455dab9798b5d520e29>`__
      -  ol_info_ptr :
         `arp_ol_t <structarp__ol__t.html#a44385b0f96b0108b308c80b2a480d4ee>`__
      -  ol_list :
         `olm_t <structolm__t.html#a2b190f623aefb6f3c6e51cc62b751789>`__

      .. rubric:: - p -
         :name: p--

      -  peerage :
         `arp_ol_cfg_t <structarp__ol__cfg__t.html#a9dd6b07a43c9a15596db5c35d9c3cce1>`__
      -  pf :
         `cy_pf_ol_cfg_t <structcy__pf__ol__cfg__t.html#aa9802e6b0478279eee94213835fc2173>`__
      -  pm :
         `ol_fns_t <structol__fns.html#a8668c8781d64582231c8454f863c8a4b>`__
      -  portnum :
         `cy_pf_pn_cfg_t <structcy__pf__pn__cfg__t.html#aa1128e070c2c60f21901efe96c1b89c4>`__
         ,
         `cy_pf_port_t <structcy__pf__port__t.html#aaee6c242184bccef5dee844c3e7a61bf>`__
      -  ports :
         `cy_tko_ol_cfg_t <structcy__tko__ol__cfg__t.html#ad4979c12e1b68e5432f3c806ef2da447>`__
      -  proto :
         `cy_pf_pn_cfg_t <structcy__pf__pn__cfg__t.html#a81243c2826dd108eedb8b71cf6f29e58>`__

      .. rubric:: - r -
         :name: r--

      -  range :
         `cy_pf_port_t <structcy__pf__port__t.html#ac90eb053e2c28a2506b59dae1f4f7520>`__
      -  remote_ip :
         `cy_tko_ol_connect_t <structcy__tko__ol__connect__t.html#a0b8520047f2469edea33da124e857cf5>`__
      -  remote_port :
         `cy_tko_ol_connect_t <structcy__tko__ol__connect__t.html#a9e92176a589e0faea7665b3fbc1f35da>`__
      -  retry_count :
         `cy_tko_ol_cfg_t <structcy__tko__ol__cfg__t.html#a33a573993dd0a6f5ddb2c247b381c585>`__
      -  retry_interval :
         `cy_tko_ol_cfg_t <structcy__tko__ol__cfg__t.html#a8fab23a40b0f3bc0a4dbc920c9dfaa2f>`__
      -  rx_window :
         `sock_seq_t <structsock__seq__t.html#a6e6a11a613192c4d761bca2df89d1307>`__

      .. rubric:: - s -
         :name: s--

      -  seqnum :
         `sock_seq_t <structsock__seq__t.html#adf9acf90ca9ecc5b6dad93b5e7cce77b>`__
      -  sleep_enable_mask :
         `arp_ol_cfg_t <structarp__ol__cfg__t.html#a4dabc5de5277f083633155e218e6e67c>`__
      -  src_mac :
         `sock_seq_t <structsock__seq__t.html#ab16109c333c6f2db454617d7eca72fb6>`__
      -  srcip :
         `sock_seq_t <structsock__seq__t.html#a8bdf5e64c5112d560db5ab0bbee5e28a>`__
      -  srcport :
         `sock_seq_t <structsock__seq__t.html#a4b8255623d7844722ce842fd87006458>`__
      -  state :
         `arp_ol_t <structarp__ol__t.html#aa2e6c553b6b7c1c536131a5174403e76>`__

      .. rubric:: - u -
         :name: u--

      -  u :
         `cy_pf_ol_cfg_t <structcy__pf__ol__cfg__t.html#a836a44ee62c168b8c0f6651abde969bf>`__

      .. rubric:: - w -
         :name: w--

      -  whd :
         `ol_info_t <structol__info__t.html#a81146b644c5eb9b2c6719976d9ef25d9>`__
         ,
         `pf_ol_t <structpf__ol__t.html#a3fa4c35aca34e12f15bf8ab5ac29af2f>`__
         ,
         `tko_ol_t <structtko__ol__t.html#ad3dadda42d16f6f71396c72e2c0f2987>`__
      -  worker :
         `ol_info_t <structol__info__t.html#ac8e21f872766ab95620106fb4461b8a2>`__
      -  worker_thread :
         `ol_desc_t <structol__desc__t.html#acf6a73a49e43742e5bb6144cff5230bd>`__


