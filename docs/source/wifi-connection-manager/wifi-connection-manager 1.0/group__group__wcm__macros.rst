=======
Macros
=======
.. doxygengroup:: group_wcm_macros
   :project: wifi-connection-manager 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:



.. toctree::
   :hidden:

   group__generic__wcm__defines.rst

