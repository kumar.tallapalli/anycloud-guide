==============
API Reference
==============

.. raw:: html

   <hr>

.. toctree::
   :hidden:

   group__group__wcm__mscs.rst
   group__group__wcm__macros.rst
   group__group__wcm__enums.rst
   group__group__wcm__typedefs.rst
   group__group__wcm__structures.rst
   group__group__wcm__functions.rst
   

The following provides a list of API documentation


+----------------------------------+----------------------------------+
| \ `Message Sequence Charts       |                                  |
| <group__group__wcm__mscs.html>`__|                                  |
+----------------------------------+----------------------------------+
| \ `Macros <gr                    |                                  |
| oup__group__wcm__macros.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `WCM results/error             | Cypress middleware APIs return   |
| codes <group                     | results of type cy_rslt_t and    |
| __generic__wcm__defines.html>`__ | comprise of three parts:         |
+----------------------------------+----------------------------------+
| \ `Enumerated                    |                                  |
| Types <g                         |                                  |
| roup__group__wcm__enums.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Typedefs <grou                |                                  |
| p__group__wcm__typedefs.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Structures <group_            |                                  |
| _group__wcm__structures.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Functions <group              |                                  |
| __group__wcm__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+

