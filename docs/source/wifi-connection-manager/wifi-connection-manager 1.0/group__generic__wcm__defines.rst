========================
WCM results/error codes
========================

.. doxygengroup:: generic_wcm_defines
   :project: wifi-connection-manager 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: