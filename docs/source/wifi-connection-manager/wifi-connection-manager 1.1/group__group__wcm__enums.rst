=================
Enumerated Types
=================

.. doxygengroup:: group_wcm_enums
   :project: wifi-connection-manager 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: