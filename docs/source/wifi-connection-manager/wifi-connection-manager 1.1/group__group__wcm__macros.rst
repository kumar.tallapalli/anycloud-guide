=======
Macros
=======
.. doxygengroup:: group_wcm_macros
   :project: wifi-connection-manager 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:



.. toctree::
   :hidden:

   group__generic__wcm__defines.rst

