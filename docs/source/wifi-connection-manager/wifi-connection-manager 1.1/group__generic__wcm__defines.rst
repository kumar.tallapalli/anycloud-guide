========================
WCM results/error codes
========================

.. doxygengroup:: generic_wcm_defines
   :project: wifi-connection-manager 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: