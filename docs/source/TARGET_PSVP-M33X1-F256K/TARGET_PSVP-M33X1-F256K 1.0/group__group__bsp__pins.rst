=============
Pin Mappings
=============

.. doxygengroup:: group_bsp_pins
   :project: TARGET_PSVP-M33X1-F256K 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
   
.. raw:: html

    <h2>API Reference</h2>
   

.. toctree::

   group__group__bsp__pins__led.rst
   group__group__bsp__pins__btn.rst
   group__group__bsp__pins__comm.rst
 
 
