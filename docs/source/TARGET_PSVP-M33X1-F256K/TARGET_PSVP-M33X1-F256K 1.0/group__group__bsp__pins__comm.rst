===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_PSVP-M33X1-F256K 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: