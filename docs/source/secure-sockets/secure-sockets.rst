Secure Sockets
===============

.. raw:: html

   <script type="text/javascript">
   window.location.href = "secure-sockets 1.0/index.html"
   </script>
   

.. toctree::
   :hidden:

   secure-sockets 1.0/secure-sockets.rst
   secure-sockets 1.1/secure-sockets.rst