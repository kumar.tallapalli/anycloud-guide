=================
Enumerated types
=================

.. doxygengroup:: group_secure_sockets_enums
   :project: secure-sockets 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: