=========
Typedefs
=========






.. doxygengroup:: group_secure_sockets_typedefs
   :project: secure-sockets 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
