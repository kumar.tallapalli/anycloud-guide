================
Cypress TLS API
================


.. doxygengroup:: group_cy_tls
   :project: secure-sockets 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::
   :hidden:
   
   group__group__cy__tls__enums.rst
   group__group__cy__tls__typedefs.rst
   group__group__cy__tls__structures.rst
   group__group__cy__tls__functions.rst
   

API Reference
--------------
| `Enumerations <group__group__cy__tls__enums.html>`__
| `Typedefs <group__group__cy__tls__typedefs.html>`__
| `Structures <group__group__cy__tls__structures.html>`__
| `Functions <group__group__cy__tls__functions.html>`__
| All the API functions except `cy_tls_init <group__group__cy__tls__functions.html#gaa78e119aa59d26d68ff2e879d55cdac8>`__, `cy_tls_deinit <group__group__cy__tls__functions.html#gaccea15eaf0ad36001b914bd53b327a84>`__, `cy_tls_load_global_root_ca_certificates <group__group__cy__tls__functions.html#ga186918ab7a9c658fb7e3cc6f00e0d6d9>`__ and `cy_tls_release_global_root_ca_certificates <group__group__cy__tls__functions.html#ga5e76c0d24388071a0ba60b38a9367bd0>`__ are thread-safe.
 
