===================================
Secure Sockets results/error codes
===================================



.. doxygengroup:: group_secure_sockets_results
   :project: secure-sockets 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
