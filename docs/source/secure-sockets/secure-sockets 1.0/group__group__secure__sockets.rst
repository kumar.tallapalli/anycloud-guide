===========================
Cypress Secure Sockets API
===========================

.. doxygengroup:: Group_secure_sockets
   :project: secure-sockets 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::
   :hidden:
      
   group__group__secure__sockets__mscs.rst
   group__group__secure__sockets__macros.rst
   group__group__secure__sockets__typedefs.rst
   group__group__secure__sockets__enums.rst
   group__group__secure__sockets__structures.rst
   group__group__secure__sockets__functions.rst


API Reference
--------------
 
| `Message Sequence Charts <group__group__secure__sockets__mscs.html>`__
| `Macros <group__group__secure__sockets__macros.html>`__
| `Typedefs <group__group__secure__sockets__typedefs.html>`__
| `Enumerated types <group__group__secure__sockets__enums.html>`__
| `Structures <group__group__secure__sockets__structures.html>`__
| `Functions <group__group__secure__sockets__functions.html>`__
| All API functions except `cy_socket_init <group__group__secure__sockets__functions.html#ga49a49b4e3b4293aa3855f47ce831ebe2>`__ and `cy_socket_deinit <group__group__secure__sockets__functions.html#ga0eb3e8e31c5a1c11c08ac35807bb5680>`__ are thread-safe.
 

