=========
Typedefs
=========






.. doxygengroup:: group_secure_sockets_typedefs
   :project: secure-sockets 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
