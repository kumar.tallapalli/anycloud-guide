=================
Enumerated types
=================

.. doxygengroup:: group_csdidac_enums
   :project: csdidac1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: