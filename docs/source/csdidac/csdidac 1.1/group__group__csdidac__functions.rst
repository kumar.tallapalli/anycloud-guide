==========
Functions
==========

.. doxygengroup:: group_csdidac_functions
   :project: csdidac1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: