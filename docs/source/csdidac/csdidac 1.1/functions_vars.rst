================================================================
Data Fields - Variables
================================================================


.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       

      -  base :
         `cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html#aa729f9b1a91feb0cc616285bdcf3a0ed>`__
      -  cfgCopy :
         `cy_stc_csdidac_context_t <structcy__stc__csdidac__context__t.html#a681f270f640cb595fe8c769b4228f3e4>`__
      -  channelStateA :
         `cy_stc_csdidac_context_t <structcy__stc__csdidac__context__t.html#a409e4f885b160c9de4049dfcfebaedec>`__
      -  channelStateB :
         `cy_stc_csdidac_context_t <structcy__stc__csdidac__context__t.html#a8f1c29f8f75835887928d206f14fcfe0>`__
      -  codeA :
         `cy_stc_csdidac_context_t <structcy__stc__csdidac__context__t.html#a44d7df7bf4b20222be95b71fcca06e61>`__
      -  codeB :
         `cy_stc_csdidac_context_t <structcy__stc__csdidac__context__t.html#a06edde8424810a1d98b850b3fc41282e>`__
      -  configA :
         `cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html#a964cd16c1873e15bb4b57e4ad26bbd36>`__
      -  configB :
         `cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html#acf742f173a42c4b904bf4e566cee520e>`__
      -  cpuClk :
         `cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html#a863024356c94b372958257d2bc4dd261>`__
      -  csdCxtPtr :
         `cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html#aca952be1774cfb9764d60e156ce073ac>`__
      -  csdInitTime :
         `cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html#a2935c4fd44bb10a96e63dd8aeb57679f>`__
      -  ioPcPtr :
         `cy_stc_csdidac_pin_t <structcy__stc__csdidac__pin__t.html#a8b26d10faee8d2524aa368f4f1e9c6ff>`__
      -  lsbA :
         `cy_stc_csdidac_context_t <structcy__stc__csdidac__context__t.html#af6814c1ee31c845fcd7dfb479a3a2b67>`__
      -  lsbB :
         `cy_stc_csdidac_context_t <structcy__stc__csdidac__context__t.html#a06c8e469802ccbd8ec9325e730be5706>`__
      -  pin :
         `cy_stc_csdidac_pin_t <structcy__stc__csdidac__pin__t.html#a7265353914e4d586aca722a613cdde3f>`__
      -  polarityA :
         `cy_stc_csdidac_context_t <structcy__stc__csdidac__context__t.html#aca8105dd6dfecbf98fbbadc5f905d7df>`__
      -  polarityB :
         `cy_stc_csdidac_context_t <structcy__stc__csdidac__context__t.html#ae282aa393ec14c56e3b56c1725de2dfa>`__
      -  ptrPinA :
         `cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html#a2fe45dd3a2b9871a4cbfc3072eaf10fc>`__
      -  ptrPinB :
         `cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html#af7e641e0f444461fd40ea41f7401ad8f>`__


