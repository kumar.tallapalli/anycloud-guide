==========
Functions
==========

.. doxygengroup:: group_csdidac_functions
   :project: csdidac1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: