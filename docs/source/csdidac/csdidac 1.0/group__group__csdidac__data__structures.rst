================
Data Structures
================
.. doxygengroup:: group_csdidac_data_structures
   :project: csdidac1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
.. toctree::
   :hidden:

   structcy__stc__csdidac__pin__t.rst
   structcy__stc__csdidac__config__t.rst
   structcy__stc__csdidac__context__t.rst

