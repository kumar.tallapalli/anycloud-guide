=================
Enumerated types
=================

.. doxygengroup:: group_csdidac_enums
   :project: csdidac1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: