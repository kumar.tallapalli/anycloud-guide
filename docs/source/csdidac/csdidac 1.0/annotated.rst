========================================================
Data Structures
========================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         Here are the data structures with brief descriptions:

      .. container:: directory

         +----------------------------------+----------------------------------+
         |  C\ `cy_                         | The CSDIDAC configuration        |
         | stc_csdidac_config_t <structcy__ | structure                        |
         | stc__csdidac__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_st                       | The CSDIDAC context structure,   |
         | c_csdidac_context_t <structcy__s | that contains the internal       |
         | tc__csdidac__context__t.html>`__ | middleware data                  |
         +----------------------------------+----------------------------------+
         |  C                               | The CSDIDAC pin structure        |
         | \ `cy_stc_csdidac_pin_t <structc |                                  |
         | y__stc__csdidac__pin__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+


