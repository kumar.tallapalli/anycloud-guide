===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_PSOC6-GENERIC 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
