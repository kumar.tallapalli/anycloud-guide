======================
Over the Air(OTA)
======================

.. raw:: html

   <script type="text/javascript">
   window.location.href = "anycloud-ota 1.0/index.html"
   </script>>

.. toctree::
   :hidden:
   
   anycloud-ota 1.0/anycloud-ota.rst
   anycloud-ota 1.1/anycloud-ota.rst