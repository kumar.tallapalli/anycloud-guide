===================
OTA Agent Callback
===================


.. doxygengroup:: group_ota_callback
   :project: anycloud-ota 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: