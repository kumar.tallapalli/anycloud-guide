===============================
Cypress Over The Air (OTA) API
===============================

.. doxygengroup:: group_cy_ota
   :project: anycloud-ota 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

 
.. toctree::

   group__group__ota__callback.rst
   group__group__ota__functions.rst
   group__group__ota__structures.rst
   group__group__ota__macros.rst
   group__group__ota__typedefs.rst
 
