=============
OTA Typedefs
=============




.. doxygengroup:: group_ota_typedefs
   :project: anycloud-ota 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: