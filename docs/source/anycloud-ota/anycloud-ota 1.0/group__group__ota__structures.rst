===============
OTA Structures
===============


.. doxygengroup:: group_ota_structures
   :project: anycloud-ota 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
.. toctree::
   
   structcy__ota__agent__params__s.rst
   structcy__ota__mqtt__params__s.rst
   structcy__ota__network__params__s.rst
   structcy__ota__server__info__s.rst