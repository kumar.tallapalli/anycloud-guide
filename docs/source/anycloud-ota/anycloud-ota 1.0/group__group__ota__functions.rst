==============
OTA Functions
==============


.. doxygengroup:: group_ota_functions
   :project: anycloud-ota 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: