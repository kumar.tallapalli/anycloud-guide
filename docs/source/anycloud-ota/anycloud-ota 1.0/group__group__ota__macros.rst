===========
OTA Macros
===========


.. doxygengroup:: group_ota_macros
   :project: anycloud-ota 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: