===================
OTA Agent Callback
===================


.. doxygengroup:: group_ota_callback
   :project: anycloud-ota 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: