==============
OTA Functions
==============


.. doxygengroup:: group_ota_functions
   :project: anycloud-ota 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: