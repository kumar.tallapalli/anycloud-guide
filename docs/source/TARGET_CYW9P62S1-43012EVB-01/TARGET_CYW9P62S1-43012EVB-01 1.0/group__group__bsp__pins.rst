=============
Pin Mappings
=============
.. doxygengroup:: group_bsp_pins
   :project: TARGET_CYW9P62S1-43012EVB-01 1.0
   :members:
   :protected-members:
   :private-members:
  

.. toctree::

   group__group__bsp__pins__led.rst
   group__group__bsp__pins__btn.rst
   group__group__bsp__pins__comm.rst
   group__group__bsp__pins__arduino.rst
   group__group__bsp__pins__j2.rst

All the pins, other than A0-A5 are No Connects. 
 
