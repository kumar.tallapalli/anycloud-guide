===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CYW9P62S1-43012EVB-01 1.0
   :members:
   :protected-members:
   :private-members: