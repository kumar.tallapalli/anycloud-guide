====================
Arduino Header Pins
====================

.. doxygengroup:: group_bsp_pins_arduino
   :project: TARGET_CY8CKIT-064B0S2-4343W 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: