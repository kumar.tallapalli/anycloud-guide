<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="_r_e_a_d_m_e_8doxygen_8md" kind="file" language="Markdown">
    <compoundname>README.doxygen.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />CY8CKIT-032<sp />shield<sp />support<sp />library</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />CY8CKIT-032<sp />is<sp />an<sp />Arduino<sp />Uno<sp />R3<sp />compatible<sp />shield<sp />intended<sp />as<sp />a<sp />companion<sp />to<sp />add<sp />an<sp />analog<sp />front<sp />end<sp />(AFE)<sp />and<sp />user<sp />interface<sp />to<sp />a<sp />baseboard.<sp />Any<sp />baseboard<sp />with<sp />Arduino<sp />Uno<sp />R3</highlight></codeline>
<codeline><highlight class="normal">compatible<sp />headers<sp />and<sp />I2C<sp />master<sp />functionality<sp />on<sp />the<sp />Arduino<sp />Uno<sp />R3<sp />I2C<sp />pins<sp />is<sp />compatible<sp />with<sp />this<sp />shield.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">A<sp />PSoC<sp />4<sp />Analog<sp />Coprocessor<sp />is<sp />used<sp />on<sp />the<sp />shield<sp />to<sp />read<sp />analog<sp />sensors<sp />(temperature,<sp />humidity,<sp />ambient<sp />light<sp />and<sp />potentiometer<sp />voltage)<sp />and<sp />to<sp />provide<sp />a<sp />CapSense<sp />user<sp />interface.<sp />The<sp />PSoC<sp />4<sp />acts<sp />as<sp />an<sp />I2C<sp />slave<sp />so<sp />that<sp />the<sp />analog<sp />sensor<sp />values<sp />can<sp />be<sp />read<sp />by<sp />the<sp />baseboard.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />shield<sp />library<sp />provides<sp />support<sp />for:</highlight></codeline>
<codeline><highlight class="normal">*<sp />Initializing/freeing<sp />all<sp />of<sp />the<sp />hardware<sp />peripheral<sp />resources<sp />on<sp />the<sp />board</highlight></codeline>
<codeline><highlight class="normal">*<sp />Defining<sp />all<sp />pin<sp />mappings<sp />from<sp />the<sp />Arduino<sp />interface<sp />to<sp />the<sp />different<sp />peripherals</highlight></codeline>
<codeline><highlight class="normal">*<sp />Providing<sp />access<sp />to<sp />each<sp />of<sp />the<sp />underlying<sp />peripherals<sp />on<sp />the<sp />board</highlight></codeline>
<codeline />
<codeline><highlight class="normal">This<sp />library<sp />makes<sp />use<sp />of<sp />the<sp />[display-oled-ssd1306](https://github.com/cypresssemiconductorco/display-oled-ssd1306)<sp />library.<sp />This<sp />can<sp />be<sp />seen<sp />in<sp />the<sp />libs<sp />directory<sp />and<sp />can<sp />be<sp />used<sp />directly<sp />instead<sp />of<sp />through<sp />the<sp />shield<sp />if<sp />desired.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">It<sp />comes<sp />with<sp />the<sp />sensors<sp />and<sp />user<sp />interface<sp />items<sp />below.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">![](board.png)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Analog<sp />Sensors</highlight></codeline>
<codeline><highlight class="normal">The<sp />PSoC<sp />AFE<sp />Shield<sp />implements<sp />the<sp />functionality<sp />of<sp />an<sp />analog<sp />coprocessor<sp />by<sp />measuring<sp />the<sp />analog<sp />sensors<sp />and<sp />transferring<sp />the<sp />digitized<sp />data<sp />to<sp />the<sp />base<sp />board<sp />via<sp />I2C.<sp />It<sp />interfaces<sp />with<sp />the<sp />following<sp />sensors:</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />Temperature<sp />(resistance)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Humidity<sp />(capacitance)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Ambient<sp />Light<sp />(current)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Potentiometer<sp />(voltage)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />User<sp />Interface</highlight></codeline>
<codeline><highlight class="normal">The<sp />PSoC<sp />AFE<sp />Shield<sp />provides<sp />the<sp />following<sp />user<sp />interface<sp />for<sp />the<sp />base<sp />board:</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />4x<sp />CapSense<sp />buttons<sp />(I2C)<sp />+<sp />4<sp />LEDs</highlight></codeline>
<codeline><highlight class="normal">*<sp />1x<sp />CapSense<sp />proximity<sp />sensor<sp />(I2C)<sp />+<sp />1<sp />LED</highlight></codeline>
<codeline><highlight class="normal">*<sp />128x64<sp />graphics<sp />OLED<sp />display<sp />(I2C)</highlight></codeline>
<codeline><highlight class="normal">*<sp />2x<sp />Mechanical<sp />buttons<sp />(GPIO)<sp />+<sp />2<sp />LEDs</highlight></codeline>
<codeline />
<codeline><highlight class="normal">#<sp />Quick<sp />Start<sp />Guide</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />[AFE<sp />application](#afe-application)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[AFE<sp />pinout<sp />examples](#afe-pinout-examples)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Display<sp />usage](https://github.com/cypresssemiconductorco/display-oled-ssd1306#quick-start)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />AFE<sp />application</highlight></codeline>
<codeline><highlight class="normal">Follow<sp />the<sp />steps<sp />below<sp />in<sp />order<sp />to<sp />create<sp />a<sp />simple<sp />application<sp />showing<sp />how<sp />to<sp />read<sp />sensors<sp />and<sp />manipulate<sp />LEDs<sp />on<sp />the<sp />shield.</highlight></codeline>
<codeline><highlight class="normal">1.<sp />Create<sp />an<sp />empty<sp />PSoC<sp />application</highlight></codeline>
<codeline><highlight class="normal">2.<sp />Add<sp />this<sp />library<sp />and<sp />the<sp />retarget-io<sp />library<sp />to<sp />the<sp />application</highlight></codeline>
<codeline><highlight class="normal">4.<sp />Place<sp />the<sp />following<sp />code<sp />in<sp />the<sp />main.c<sp />file:</highlight></codeline>
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cyhal_system.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cybsp.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cy8ckit_032.h"</highlight></codeline>
<codeline><highlight class="normal">#include<sp />"cy_retarget_io.h"</highlight></codeline>
<codeline />
<codeline><highlight class="normal">int<sp />main(void)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy_rslt_t<sp />result;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint8_t<sp />led_states;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint8_t<sp />button_states;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />float<sp />light,<sp />humidity,<sp />voltage,<sp />temperature;</highlight></codeline>
<codeline />
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Initialize<sp />the<sp />device,<sp />board<sp />peripherals,<sp />and<sp />retarget-io<sp />lib<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cybsp_init();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cy8ckit_032_init(NULL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cy_retarget_io_init(CYBSP_DEBUG_UART_TX,<sp />CYBSP_DEBUG_UART_RX,<sp />CY_RETARGET_IO_BAUDRATE);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__enable_irq();</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Gets<sp />the<sp />current<sp />state<sp />of<sp />the<sp />leds.<sp />Since,<sp />at<sp />this<sp />point,<sp />the<sp />led</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp />*<sp />control<sp />mode<sp />is<sp />capsense<sp />mode<sp />the<sp />led<sp />state<sp />will<sp />match<sp />which<sp />buttons<sp />are</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp />*<sp />currently<sp />being<sp />pressed<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy8ckit_032_get_cled_states(&amp;led_states);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />/*<sp />Enables<sp />software<sp />control<sp />of<sp />the<sp />board<sp />leds<sp />and<sp />turns<sp />2<sp />CLEDs<sp />on<sp />*/</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy8ckit_032_set_led_control(CY8CKIT_032_LED_CTRL_SW);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy8ckit_032_set_cled_states(CY8CKIT_032_CLED1<sp />|<sp />CY8CKIT_032_CLED3);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />for(;;)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cy8ckit_032_get_button_states(&amp;button_states);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("CB0<sp />is<sp />%s\r\n",<sp />CY8CKIT_032_BTN_CAPSENSE_B0<sp />&amp;<sp />button_states<sp />?<sp />"pressed"<sp />:<sp />"not<sp />pressed");</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("CB1<sp />is<sp />%s\r\n",<sp />CY8CKIT_032_BTN_CAPSENSE_B1<sp />&amp;<sp />button_states<sp />?<sp />"pressed"<sp />:<sp />"not<sp />pressed");</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("CB2<sp />is<sp />%s\r\n",<sp />CY8CKIT_032_BTN_CAPSENSE_B2<sp />&amp;<sp />button_states<sp />?<sp />"pressed"<sp />:<sp />"not<sp />pressed");</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("CB3<sp />is<sp />%s\r\n",<sp />CY8CKIT_032_BTN_CAPSENSE_B3<sp />&amp;<sp />button_states<sp />?<sp />"pressed"<sp />:<sp />"not<sp />pressed");</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("MB0<sp />is<sp />%s\r\n",<sp />CY8CKIT_032_BTN_MECHANICAL_B1<sp />&amp;<sp />button_states<sp />?<sp />"pressed"<sp />:<sp />"not<sp />pressed");</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("MB1<sp />is<sp />%s\r\n",<sp />CY8CKIT_032_BTN_MECHANICAL_B2<sp />&amp;<sp />button_states<sp />?<sp />"pressed"<sp />:<sp />"not<sp />pressed");</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("PRX<sp />is<sp />%s\r\n",<sp />CY8CKIT_032_BTN_PROXIMITY<sp />&amp;<sp />button_states<sp />?<sp />"pressed"<sp />:<sp />"not<sp />pressed");</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cy8ckit_032_get_ambient_light_lux(&amp;light);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("Light<sp />level:<sp />%2.2flux\r\n",<sp />light);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cy8ckit_032_get_humidity_percent(&amp;humidity);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("Humidity:<sp />%2.2f%%\r\n",<sp />humidity);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cy8ckit_032_get_potentiometer_voltage(&amp;voltage);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("Potentiometer<sp />voltage:<sp />%2.2fV\r\n",<sp />voltage);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cy8ckit_032_get_temperature_c(&amp;temperature);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf("Temperature:<sp />%fC\r\n\r\n",<sp />temperature);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cyhal_system_delay_ms(1000);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline><highlight class="normal">4.<sp />Build<sp />the<sp />application<sp />and<sp />program<sp />the<sp />kit.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />AFE<sp />pinout<sp />examples</highlight></codeline>
<codeline><highlight class="normal">The<sp />following<sp />snippits<sp />of<sp />code<sp />show<sp />how<sp />to<sp />interact<sp />with<sp />other<sp />features<sp />of<sp />the<sp />shield<sp />that<sp />are<sp />exposed<sp />to<sp />the<sp />base<sp />board<sp />through<sp />the<sp />Arduino<sp />pins.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />//ADC<sp />sample<sp />code</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_adc_t<sp />adc;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_adc_channel_t<sp />adcChanLight,<sp />adcChanPot,<sp />adcChanDac;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_adc_init(&amp;adc,<sp />CY8CKIT_032_PIN_DAC_VOLTAGE,<sp />NULL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_adc_channel_init(&amp;adcChanLight,<sp />&amp;adc,<sp />CY8CKIT_032_PIN_LIGHT_SENSOR);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_adc_channel_init(&amp;adcChanPot,<sp />&amp;adc,<sp />CY8CKIT_032_PIN_POTENTIOMETER);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_adc_channel_init(&amp;adcChanDac,<sp />&amp;adc,<sp />CY8CKIT_032_PIN_DAC_VOLTAGE);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint16_t<sp />adcValLight<sp />=<sp />cyhal_adc_read_u16(&amp;adcChanLight);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint16_t<sp />adcValPot<sp />=<sp />cyhal_adc_read_u16(&amp;adcChanPot);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint16_t<sp />adcValDac<sp />=<sp />cyhal_adc_read_u16(&amp;adcChanDac);</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline />
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />//LED/Button<sp />sample<sp />code</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_gpio_init(CY8CKIT_032_PIN_BUTTON1,<sp />CYHAL_GPIO_DIR_INPUT,<sp />CYHAL_GPIO_DRIVE_NONE,<sp />false);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_gpio_init(CY8CKIT_032_PIN_BUTTON2,<sp />CYHAL_GPIO_DIR_INPUT,<sp />CYHAL_GPIO_DRIVE_NONE,<sp />false);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_gpio_init(CY8CKIT_032_PIN_LED1,<sp />CYHAL_GPIO_DIR_OUTPUT,<sp />CYHAL_GPIO_DRIVE_STRONG,<sp />true);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_gpio_init(CY8CKIT_032_PIN_LED2,<sp />CYHAL_GPIO_DIR_OUTPUT,<sp />CYHAL_GPIO_DRIVE_STRONG,<sp />true);</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />bool<sp />btn1<sp />=<sp />cyhal_gpio_read(CY8CKIT_032_PIN_BUTTON1);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />bool<sp />btn2<sp />=<sp />cyhal_gpio_read(CY8CKIT_032_PIN_BUTTON2);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_gpio_write(CY8CKIT_032_PIN_LED1,<sp />!btn1);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_gpio_write(CY8CKIT_032_PIN_LED2,<sp />!btn2);</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline />
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />//<sp />CapSense<sp />ISR<sp />sample<sp />code</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />void<sp />capsense_isr_callback(void<sp />*callback_arg,<sp />cyhal_gpio_event_t<sp />event)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY_UNUSED_PARAMETER(callback_arg);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY_UNUSED_PARAMETER(event);<sp />//Will<sp />always<sp />be<sp />CYHAL_GPIO_IRQ_RISE</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />//Do<sp />something<sp />on<sp />interrupt<sp />trigger</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_gpio_init(CY8CKIT_032_PIN_CAPSENSE_ISR,<sp />CYHAL_GPIO_DIR_INPUT,<sp />CYHAL_GPIO_DRIVE_NONE,<sp />false);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_gpio_register_callback(CY8CKIT_032_PIN_CAPSENSE_ISR,<sp />capsense_isr_callback,<sp />NULL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_gpio_enable_event(CY8CKIT_032_PIN_CAPSENSE_ISR,<sp />CYHAL_GPIO_IRQ_RISE,<sp />CYHAL_ISR_PRIORITY_DEFAULT,<sp />true);</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline />
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />//<sp />GPIO<sp />sample<sp />code</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_gpio_init(CY8CKIT_032_PIN_GPIO,<sp />CYHAL_GPIO_DIR_BIDIRECTIONAL,<sp />CYHAL_GPIO_DRIVE_PULLUPDOWN,<sp />true);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />bool<sp />gpio<sp />=<sp />cyhal_gpio_read(CY8CKIT_032_PIN_BUTTON1);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_gpio_write(CY8CKIT_032_PIN_GPIO,<sp />!gpio);</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline />
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />//<sp />UART<sp />sample<sp />code</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_uart_t<sp />uart;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />static<sp />const<sp />cyhal_uart_cfg_t<sp />UART_CFG<sp />=<sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.data_bits<sp />=<sp />8,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.stop_bits<sp />=<sp />1,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.parity<sp />=<sp />CYHAL_UART_PARITY_NONE,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.rx_buffer<sp />=<sp />NULL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />.rx_buffer_size<sp />=<sp />0,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />const<sp />uint32_t<sp />UART_DESIRED_BUAD<sp />=<sp />115200;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint8_t<sp />uartValue;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />uartActualBaud;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_uart_init(&amp;uart,<sp />CY8CKIT_032_PIN_UART_TX,<sp />CY8CKIT_032_PIN_UART_RX,<sp />NULL,<sp />&amp;UART_CFG);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_uart_set_baud(&amp;uart,<sp />UART_DESIRED_BUAD,<sp />&amp;uartActualBaud);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_uart_getc(&amp;uart,<sp />&amp;uartValue,<sp />0);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_uart_putc(&amp;uart,<sp />uartValue);</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline />
<codeline><highlight class="normal">###<sp />More<sp />information</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />[API<sp />Reference<sp />Guide](https://cypresssemiconductorco.github.io/cy8ckit-032/html/index.html)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[CY8CKIT-032<sp />Documentation](https://www.cypress.com/documentation/development-kitsboards/cy8ckit-032-psoc-analog-front-end-afe-arduino-shield)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[SEGGER<sp />emWin<sp />Middleware<sp />Library](https://github.com/cypresssemiconductorco/emwin)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2019.</highlight></codeline>
    </programlisting>
    <location file="bsp/shields/CY8CKIT-032/README.doxygen.md" />
  </compounddef>
</doxygen>