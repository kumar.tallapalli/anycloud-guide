BMI-160 Inertial Measurement Unit(Motion Sensor)
=================================================

.. raw:: html

   <script type="text/javascript">
   window.location.href = "sensor-motion-bmi160 1.0/index.html"
   </script>
   
   
.. toctree::
   :hidden:


   sensor-motion-bmi160 1.0/sensor-motion-bmi160.rst
   sensor-motion-bmi160 1.1/sensor-motion-bmi160.rst