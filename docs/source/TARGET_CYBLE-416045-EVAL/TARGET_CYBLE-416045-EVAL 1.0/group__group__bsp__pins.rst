=============
Pin Mappings
=============





.. doxygengroup:: group_bsp_pins
   :project: TARGET_CYBLE-416045-EVAL 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

API Reference
--------------   

.. toctree::

   group__group__bsp__pins__led.rst
   group__group__bsp__pins__btn.rst
   group__group__bsp__pins__comm.rst
   group__group__bsp__pins__arduino.rst
   group__group__bsp__pins__j6.rst
 
 
