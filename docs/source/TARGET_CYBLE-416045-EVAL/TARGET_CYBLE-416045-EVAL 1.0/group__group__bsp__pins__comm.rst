===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CYBLE-416045-EVAL 1.0
   :members:
   :protected-members:
   :private-members: