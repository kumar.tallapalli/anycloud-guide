<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="indexpage" kind="group">
    <compoundname>index</compoundname>
    <title>CY8CKIT-028-EPD shield support library</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para>The E-ink Display Shield Board (CY8CKIT-028-EPD) has been designed such that an ultra-low-power E-ink display, sensors and a microphone can interface with PSoC MCUs.</para><para>It comes with the features below to enable everyday objects to connect to the Internet of Things (IoT).</para><para><itemizedlist>
<listitem><para>Ultra-low-power 2.7" E-ink Display (E2271CS021)</para></listitem><listitem><para>Motion Sensor (BMI-160)</para></listitem><listitem><para>Temperature Sensor (NCP18XH103F03RB)</para></listitem><listitem><para>PDM Microphone example code (SPK0838HT4HB)</para></listitem></itemizedlist>
</para><para>The shield library provides support for:<itemizedlist>
<listitem><para>Initializing/freeing all of the hardware peripheral resources on the board</para></listitem><listitem><para>Defining all pin mappings from the Arduino interface to the different peripherals</para></listitem><listitem><para>Providing access to each of the underlying peripherals on the board</para></listitem></itemizedlist>
</para><para>This library makes use of support libraries: <ulink url="https://github.com/cypresssemiconductorco/display-eink-e2271cs021">display-eink-e2271cs021</ulink>, <ulink url="https://github.com/cypresssemiconductorco/sensor-motion-bmi160">sensor-motion-bmi160</ulink>, and <ulink url="https://github.com/cypresssemiconductorco/thermistor">thermistor</ulink>. These can be seen in the libs directory and can be used directly instead of through the shield if desired.</para><para>The E-ink Display Shield Board uses the Arduino Uno pin layout, enabling this shield board to be used with the PSoC 4 and PSoC 6 MCU based Pioneer Kits.</para><para><image name="board.png" type="html" />
</para><para><heading level="1">Quick Start Guide</heading>
</para><para><itemizedlist>
<listitem><para><ulink url="#basic-shield-usage">Basic shield usage</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/display-eink-e2271cs021#quick-start">Display usage</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/sensor-motion-bmi160#quick-start">Motion Sensor usage</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/thermistor#quick-start">Thermistor usage</ulink></para></listitem></itemizedlist>
</para><para><heading level="2">Basic shield usage</heading>
</para><para>The E-ink library can be also used standalone. Follow the steps below to create a simple application which shows an interesting pattern on the display.<orderedlist>
<listitem><para>Create an empty application</para></listitem><listitem><para>Add this library to the application</para></listitem><listitem><para>Place the following code in the main.c file: <programlisting filename=".cpp"><codeline><highlight class="preprocessor">#include<sp />"cyhal.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cybsp.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy8ckit_028_epd_pins.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"mtb_e2271cs021.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal">cyhal_spi_t<sp />spi;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keyword">const</highlight><highlight class="normal"><sp />mtb_e2271cs021_pins_t<sp />pins<sp />=</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_mosi<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1ga911dae0f6089d6f86dfefae8fc336846">CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MOSI</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_miso<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1gaf146e083896cd3110b730f09e20635e8">CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MISO</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_sclk<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1ga9bcf9b27a4cd6a91da8fa8bbdfb608b7">CY8CKIT_028_EPD_PIN_DISPLAY_SPI_SCLK</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.spi_cs<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1gaa0d6d4bee42389e9d652096d3f5fcf92">CY8CKIT_028_EPD_PIN_DISPLAY_CS</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.reset<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1ga079106a513ecc6314397bed74a8026ac">CY8CKIT_028_EPD_PIN_DISPLAY_RST</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.busy<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1gae76062666ba5f9cf480147c87077367f">CY8CKIT_028_EPD_PIN_DISPLAY_BUSY</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.discharge<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1ga786b3f9ebde8391e510b78427aaaf08d">CY8CKIT_028_EPD_PIN_DISPLAY_DISCHARGE</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.enable<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1ga44f19c0216436ce4ec5d758339c3d750">CY8CKIT_028_EPD_PIN_DISPLAY_EN</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.border<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1ga1173cebc72c10d08ba60f1ddbf3fead5">CY8CKIT_028_EPD_PIN_DISPLAY_BORDER</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.io_enable<sp />=<sp /><ref kindref="member" refid="group__group__board__libs__pins_1ga38053ff5e0898cf3632d0f002c7f0662">CY8CKIT_028_EPD_PIN_DISPLAY_IOEN</ref>,</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal">uint8_t<sp />previous_frame[PV_EINK_IMAGE_SIZE]<sp />=<sp />{0};</highlight></codeline>
<codeline><highlight class="normal">uint8_t<sp />current_frame[PV_EINK_IMAGE_SIZE]<sp />=<sp />{0};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">int</highlight><highlight class="normal"><sp />main(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy_rslt_t<sp />result;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />i;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />device<sp />and<sp />board<sp />peripherals<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cybsp_init();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__enable_irq();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />SPI<sp />and<sp />EINK<sp />display<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_spi_init(&amp;spi,<sp /><ref kindref="member" refid="group__group__board__libs__pins_1ga911dae0f6089d6f86dfefae8fc336846">CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MOSI</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__board__libs__pins_1gaf146e083896cd3110b730f09e20635e8">CY8CKIT_028_EPD_PIN_DISPLAY_SPI_MISO</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__board__libs__pins_1ga9bcf9b27a4cd6a91da8fa8bbdfb608b7">CY8CKIT_028_EPD_PIN_DISPLAY_SPI_SCLK</ref>,<sp />NC,<sp />NULL,<sp />8,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CYHAL_SPI_MODE_00_MSB,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(CY_RSLT_SUCCESS<sp />==<sp />result)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_spi_set_frequency(&amp;spi,<sp />20000000);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />mtb_e2271cs021_init(&amp;pins,<sp />&amp;spi);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Fill<sp />the<sp />EINK<sp />buffer<sp />with<sp />an<sp />interesting<sp />pattern<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">for</highlight><highlight class="normal"><sp />(i<sp />=<sp />0;<sp />i<sp />&lt;<sp />PV_EINK_IMAGE_SIZE;<sp />i++)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />current_frame[i]<sp />=<sp />i<sp />%<sp />0xFF;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Update<sp />the<sp />display<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />mtb_e2271cs021_show_frame(previous_frame,<sp />current_frame,<sp />MTB_E2271CS021_FULL_4STAGE,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">for</highlight><highlight class="normal"><sp />(;;)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
</programlisting></para></listitem><listitem><para>Build the application and program the kit.</para></listitem></orderedlist>
</para><para><bold>More information</bold>
</para><para><itemizedlist>
<listitem><para><ulink url="https://cypresssemiconductorco.github.io/CY8CKIT-028-EPD/html/index.html">API Reference Guide</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/documentation/development-kitsboards/e-ink-display-shield-board-cy8ckit-028-epd">CY8CKIT-028-EPD Documentation</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/emwin">SEGGER emWin Middleware Library</ulink></para></listitem><listitem><para><ulink url="http://www.cypress.com">Cypress Semiconductor, an Infineon Technologies Company</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco">Cypress Semiconductor GitHub</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/products/modustoolbox-software-environment">ModusToolbox</ulink> <hruler />
 © Cypress Semiconductor Corporation, 2019-2020. </para></listitem></itemizedlist>
</para>    </detaileddescription>
  </compounddef>
</doxygen>