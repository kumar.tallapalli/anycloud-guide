<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__board__libs__microphone" kind="group">
    <compoundname>group_board_libs_microphone</compoundname>
    <title>Microphone</title>
    <briefdescription>
<para>The microphone uses the HAL PDM_PCM driver for its initialization. </para>    </briefdescription>
    <detaileddescription>
<para>There is no custom driver for this hardware.</para><para><heading level="1">Microphone Setup </heading>
</para><para>To enable the microphone on the shield, these steps must be performed:</para><para><orderedlist>
<listitem><para>Initialize a PLL</para></listitem><listitem><para>Initialize the audio subsystem clock</para></listitem><listitem><para>Configure the PDM support system</para></listitem></orderedlist>
</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;For the microphone, there are two parameters passed to the shield initialization function:&lt;ul&gt;&lt;li&gt;&lt;p&gt;cyhal_clock_t* - audio_clock in the example code below&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;cyhal_pdm_pcm_cfg_t* - configuration settings to use for the PDM&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;If either of these are NULL, the microphone will not be enabled.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para><para><heading level="2">1. Initialize the PLL </heading></para>
 <para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This example code is for a PSoC 6 device.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><programlisting><codeline><highlight class="normal">cyhal_clock_allocate(&amp;pll_clock,<sp />CYHAL_CLOCK_BLOCK_PLL);</highlight></codeline>
<codeline><highlight class="normal">cyhal_clock_set_frequency(&amp;pll_clock,<sp />&amp;lt;AUDIO_SYS_CLOCK_HZ&amp;gt;,<sp />NULL);</highlight></codeline>
<codeline><highlight class="normal">cyhal_clock_set_enabled(&amp;pll_clock,<sp />true,<sp />true);</highlight></codeline>
</programlisting></para><para>The value for &lt;AUDIO_SYS_CLOCK_HZ&gt; is discussed below. A typical value is 16384000.</para><para><heading level="2">2. Initialize the audio subsystem clock </heading>
</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This example code is for a PSoC 6 device.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><programlisting><codeline><highlight class="normal">cyhal_clock_get(&amp;audio_clock,<sp />&amp;<sp />&amp;lt;AUDIO_CLOCK_RESOURCE&amp;gt;);</highlight></codeline>
<codeline><highlight class="normal">cyhal_clock_init(&amp;audio_clock);</highlight></codeline>
<codeline><highlight class="normal">cyhal_clock_set_source(&amp;audio_clock,<sp />&amp;pll_clock);</highlight></codeline>
<codeline><highlight class="normal">cyhal_clock_set_enabled(&amp;audio_clock,<sp />true,<sp />true);</highlight></codeline>
</programlisting></para><para>An appropriate setting for &lt;AUDIO_CLOCK_RESOURCE&gt; is CYHAL_CLOCK_HF[1].</para><para><heading level="2">3. Configure the PDM support system </heading>
</para><para>This involves setting the values in the cyhal_pdm_pcm_cfg_t structure. It also involves setting the value of &lt;AUDIO_SYS_CLOCK_HZ&gt;, referred to in example code above.</para><para>left_gain, right_gain - Typically set to 0. May be set to other values to implement some sort of volume control.</para><para>decimation_rate - Typically set to 32. May be changed to oversample the PDM for non-voice applications. Or to downsample for low power conditions. For example, implementing some kind of pink-noise wake-up.</para><para>mode - Typically set to CYHAL_PDM_PCM_MODE_STEREO.</para><para>sample_rate and &lt;AUDIO_SYS_CLOCK_HZ&gt; - For many applications, setting the sample_rate to 8000 (Hz) and setting &lt;AUDIO_SYS_CLOCK_HZ&gt; to 16384000 will be appropriate.</para><para>The most common audio sample rates are 8, 16, 32, 44.1, and 48 Ksps.</para><para>The minimum frequency for the PLL on PSoC 6 is 12.5 MHz. The value of &lt;AUDIO_SYS_CLOCK_HZ&gt; is determined by the sample_rate multiplied by the decimation_rate multiplied by some value (X) that makes the result greater than 12.5 MHz. So, X needs to be at least 64. And that means that the minimum clock frequency from the PLL to achieve the standard sample rates is 16.384 MHz.</para><para><bold>More information can be found here:</bold></para><para>www.cypress.com/file/399201/download, chapter 29<linebreak />
cypresssemiconductorco.github.io/psoc6hal/html/group__group__hal__pdmpcm.html </para>    </detaileddescription>
  </compounddef>
</doxygen>