=================
Enumerated Types
=================






.. doxygengroup:: group_lvd_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: