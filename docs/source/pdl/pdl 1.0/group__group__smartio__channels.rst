============================
Smart I/O channel selection
============================

.. doxygengroup:: group_smartio_channels
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: