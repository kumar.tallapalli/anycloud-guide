=======
Macros
=======

.. toctree::
 
   group__group__scb__i2c__macros__slave__status.rst
   group__group__scb__i2c__macros__master__status.rst
   group__group__scb__i2c__macros__callback__events.rst
   group__group__scb__i2c__macros__addr__callback__events.rst

.. doxygengroup:: group_scb_i2c_macros 
   :project: pdl1.0
   :members:
