===========
Fast Clock
===========

.. doxygengroup:: group_sysclk_clk_fast
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
==============

.. toctree::

   group__group__sysclk__clk__fast__funcs.rst