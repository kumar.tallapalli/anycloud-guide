=================
Enumerated Types
=================

.. doxygengroup:: group_canfd_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: