==========================
Direct Crypto Core Access
==========================

.. doxygengroup:: group_crypto_lld_api
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


.. toctree::
   
   group__group__crypto__lld__hw.rst
   group__group__crypto__lld__symmetric.rst
   group__group__crypto__lld__asymmetric.rst
   group__group__crypto__lld__sha.rst
   group__group__crypto__lld__mac.rst
   group__group__crypto__lld__crc.rst
   group__group__crypto__lld__rng
   group__group__crypto__lld__vu
   group__group__crypto__lld__mem