================================
PRA (Protected Register Access)
================================

.. doxygengroup:: group_pra
   :project: pdl1.0
   



.. toctree::
   
   group__group__pra__macros.rst
   group__group__pra__functions.rst
   group__group__pra__enums.rst
   group__group__pra__data__structures__cfg.rst
   group__group__pra__data__structures.rst
   
   
