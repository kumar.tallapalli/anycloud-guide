==================================
SCB (Serial Communication Block)
==================================


.. doxygengroup:: group_scb
   :project: pdl1.0
 
.. toctree::
   
   group__group__scb__common.rst
   group__group__scb__ezi2c.rst
   group__group__scb__i2c.rst
   group__group__scb__spi.rst
   group__group__scb__uart.rst
   
   
   
