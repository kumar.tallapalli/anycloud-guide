================
Transfer Errors
================

.. doxygengroup:: group_usbfs_dev_drv_macros_ep_xfer_err
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: