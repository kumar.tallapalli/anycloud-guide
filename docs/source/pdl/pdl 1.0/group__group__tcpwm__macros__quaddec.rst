======
Macros
======

.. doxygengroup:: group_tcpwm_macros_quaddec
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
   
API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__tcpwm__quaddec__mode.rst
   group__group__tcpwm__quaddec__resolution.rst
   group__group__tcpwm__quaddec__capture__mode.rst
   group__group__tcpwm__quaddec__status.rst