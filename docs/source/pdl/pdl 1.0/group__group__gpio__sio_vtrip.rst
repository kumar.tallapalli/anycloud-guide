===========================
SIO input buffer trip-point
===========================

.. doxygengroup:: group_gpio_sioVtrip
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: