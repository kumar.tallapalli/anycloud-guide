=====================================
CAN FD (CAN with Flexible Data-Rate)
=====================================


.. doxygengroup:: group_canfd
   :project: pdl1.0
 

.. toctree::
   
   group__group__canfd__macros.rst
   group__group__canfd__functions.rst
   group__group__canfd__data__structures.rst
   group__group__canfd__enums.rst

   
   


