================
Enumerated Types
================

.. doxygengroup:: group_gpio_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: