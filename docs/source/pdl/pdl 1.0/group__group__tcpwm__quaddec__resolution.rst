==================
QuadDec Resolution
==================

.. doxygengroup:: group_tcpwm_quaddec_resolution
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: