==========
Functions
==========

.. doxygengroup:: group_seglcd_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Referencence
^^^^^^^^^^^^^^
.. toctree::

   group__group__seglcd__functions__config.rst
   group__group__seglcd__functions__frame.rst
   group__group__seglcd__functions__display.rst

