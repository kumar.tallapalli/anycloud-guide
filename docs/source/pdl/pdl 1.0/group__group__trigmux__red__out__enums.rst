=====================================
Reduction Trigger Mutiplexer Outputs
=====================================

.. doxygengroup:: group_trigmux_red_out_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: