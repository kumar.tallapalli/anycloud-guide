======================
Counter CLK Prescalers
======================

.. doxygengroup:: group_tcpwm_counter_clk_prescalers
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: