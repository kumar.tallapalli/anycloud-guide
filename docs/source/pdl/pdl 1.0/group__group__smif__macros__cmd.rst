===============
Command Macros
===============

.. doxygengroup:: group_smif_macros_cmd
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: