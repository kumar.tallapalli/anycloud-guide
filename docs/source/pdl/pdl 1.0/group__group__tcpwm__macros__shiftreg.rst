======
Macros
======

.. doxygengroup:: group_tcpwm_macros_shiftreg
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__tcpwm__shiftreg__clk__prescalers.rst
   group__group__tcpwm__shiftreg__output__line.rst
   group__group__tcpwm__shiftreg__output__invert.rst
   group__group__tcpwm__shiftreg__output__on__disable.rst
   group__group__tcpwm__shiftreg__status.rst