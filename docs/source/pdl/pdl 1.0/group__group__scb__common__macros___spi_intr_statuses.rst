=======================
SPI Interrupt Statuses
=======================




.. doxygengroup:: group_scb_common_macros_SpiIntrStatuses
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: