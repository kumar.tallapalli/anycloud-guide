=====================================
Channel Configuration Register Enums
=====================================


.. doxygengroup:: group_sar_chan_config_register_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: