=================
Global Variables
=================

.. doxygengroup:: group_system_config_globals
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: