=======================================
DMAC (Direct Memory Access Controller)
=======================================


.. doxygengroup:: group_dmac
   :project: pdl1.0
 
.. toctree::
   
   group__group__dmac__macros.rst
   group__group__dmac__descriptor__functions.rst 
   group__group__dmac__functions.rst
   group__group__dmac__data__structures.rst
   group__group__dmac__enums
   
   
   
   
   
