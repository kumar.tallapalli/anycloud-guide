==============================
Sample Control Register Enums
==============================




.. doxygengroup:: group_sar_sample_ctrl_register_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: