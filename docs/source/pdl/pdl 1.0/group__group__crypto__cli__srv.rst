====================
Client-Server Model
====================

.. doxygengroup:: group_crypto_cli_srv
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::
 
   group__group__crypto__cli__srv__macros.rst
   group__group__crypto__cli__srv__functions.rst
   group__group__crypto__cli__srv__data__structures.rst