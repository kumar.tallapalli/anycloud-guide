=====================================
Useful Configuration Query Functions
=====================================


.. doxygengroup:: group_sar_functions_helper
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: