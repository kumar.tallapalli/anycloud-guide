=========================
Initialization Functions
=========================

.. doxygengroup:: group_usbfs_dev_hal_functions_common
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: