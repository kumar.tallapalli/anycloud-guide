===================================
Reduction Trigger Mutiplexer Inputs
===================================

.. doxygengroup:: group_trigmux_red_in_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: