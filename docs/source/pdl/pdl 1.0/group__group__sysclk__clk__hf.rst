=====================
High-Frequency Clocks
=====================

.. doxygengroup:: group_sysclk_clk_hf
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
==============

.. toctree::

   group__group__sysclk__clk__hf__funcs.rst
   group__group__sysclk__clk__hf__enums.rst