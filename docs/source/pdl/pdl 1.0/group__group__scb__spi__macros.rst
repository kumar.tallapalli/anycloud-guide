=======
Macros
=======

.. toctree::

   group__group__scb__spi__macros__tx__fifo__status.rst
   group__group__scb__spi__macros__rx__fifo__status.rst
   group__group__scb__spi__macros__master__slave__status.rst
   group__group__scb__spi__macros__xfer__status.rst
   group__group__scb__spi__macros__callback__events.rst

.. doxygengroup:: group_scb_spi_macros 
   :project: pdl1.0 
   :members: