======================================
SARMUX Switch Control Register Enums
======================================



.. doxygengroup:: group_sar_mux_switch_register_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: