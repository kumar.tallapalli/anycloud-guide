==========
Functions
==========

.. toctree::

   group__group__scb__uart__general__functions.rst
   group__group__scb__uart__high__level__functions.rst
   group__group__scb__uart__low__level__functions.rst
   group__group__scb__uart__interrupt__functions.rst
   group__group__scb__uart__low__power__functions.rst


.. doxygengroup:: group_scb_uart_functions
   :project: pdl1.0
   :members:
