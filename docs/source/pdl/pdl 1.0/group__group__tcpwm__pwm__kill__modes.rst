==============
PWM kill modes
==============

.. doxygengroup:: group_tcpwm_pwm_kill_modes
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: