==========
Functions
==========

.. doxygengroup:: group_trigmux_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: