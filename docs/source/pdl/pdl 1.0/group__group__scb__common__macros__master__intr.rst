==========================
Master Interrupt Statuses
==========================




.. doxygengroup:: group_scb_common_macros_master_intr
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: