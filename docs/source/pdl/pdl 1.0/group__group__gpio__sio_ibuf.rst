=======================
SIO input buffer mode
=======================

.. doxygengroup:: group_gpio_sioIbuf
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: