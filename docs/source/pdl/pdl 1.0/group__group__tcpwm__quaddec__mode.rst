=============
QuadDec Mode
=============

.. doxygengroup:: group_tcpwm_quaddec_mode
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: