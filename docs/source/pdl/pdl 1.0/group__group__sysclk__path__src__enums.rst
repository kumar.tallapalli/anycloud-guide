=================
Enumerated Types
=================

.. doxygengroup:: group_sysclk_path_src_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: