=================
Enumerated Types
=================



.. doxygengroup:: group_dmac_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: