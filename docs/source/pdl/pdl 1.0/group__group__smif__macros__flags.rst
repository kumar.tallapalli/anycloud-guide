======================
External Memory Flags
======================

.. doxygengroup:: group_smif_macros_flags
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: