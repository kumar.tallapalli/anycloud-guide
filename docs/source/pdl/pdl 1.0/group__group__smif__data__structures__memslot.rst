===================================
SMIF Memory Description Structures
===================================

.. doxygengroup:: group_smif_data_structures_memslot
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: