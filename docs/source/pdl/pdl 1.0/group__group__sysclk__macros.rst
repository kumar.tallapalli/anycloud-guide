=======
Macros
=======


API Reference
==============

.. toctree::

   group__group__sysclk__ecostatus.rst

.. doxygengroup:: group_sysclk_macros
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: