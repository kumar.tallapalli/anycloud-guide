=================
Enumerated Types
=================




.. doxygengroup:: group_rtc_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: