=======
Macros
=======

.. doxygengroup:: group_ipc_pipe_macros
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: