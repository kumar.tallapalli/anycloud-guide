=================================
Distribution Trigger Mutiplexers
=================================

API Reference
^^^^^^^^^^^^^^

.. toctree::
   
   group__group__trigmux__dst__in__enums.rst
   group__group__trigmux__dst__out__enums.rst
   
   
.. doxygengroup:: group_trigmux_dst_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: