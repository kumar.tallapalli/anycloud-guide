==========
Functions
==========

.. toctree::
   
   group__group__dmac__block__functions.rst 
   group__group__dmac__channel__functions.rst
   group__group__dmac__descriptor__functions.rst
   
   

.. doxygengroup:: group_dmac_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: