=========================
Card Status (CMD13) Bits
=========================




.. doxygengroup:: group_sd_host_macros_card_status
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: