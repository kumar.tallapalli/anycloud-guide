=============================
PPU Region (RG) v1 Functions
=============================

.. doxygengroup:: group_prot_functions_ppu_rg
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: