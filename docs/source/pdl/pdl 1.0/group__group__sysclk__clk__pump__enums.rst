=================
Enumerated Types
=================

.. doxygengroup:: group_sysclk_clk_pump_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: