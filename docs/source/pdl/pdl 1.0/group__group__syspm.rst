===============================
SysPm (System Power Management)
===============================

.. doxygengroup:: group_syspm
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

.. toctree::

   group__group__syspm__macros.rst
   group__group__syspm__functions.rst
   group__group__syspm__data__structures.rst
   group__group__syspm__data__enumerates.rst
   


