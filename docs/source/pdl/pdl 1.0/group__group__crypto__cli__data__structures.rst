=======================
Client Data Structures
=======================

.. doxygengroup:: group_crypto_cli_data_structures
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: