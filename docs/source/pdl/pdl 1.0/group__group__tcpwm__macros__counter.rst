=======
Macros
=======

.. doxygengroup:: group_tcpwm_macros_counter
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__tcpwm__counter__run__modes.rst
   group__group__tcpwm__counter__direction.rst
   group__group__tcpwm__counter__clk__prescalers.rst
   group__group__tcpwm__counter__compare__capture.rst
   group__group__tcpwm__counter__status.rst