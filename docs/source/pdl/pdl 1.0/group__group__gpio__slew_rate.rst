==================
Slew Rate Mode
==================

.. doxygengroup:: group_gpio_slewRate
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: