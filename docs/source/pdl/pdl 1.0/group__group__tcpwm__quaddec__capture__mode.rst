====================
QuadDec CaptureMode
====================

.. doxygengroup:: group_tcpwm_quaddec_capture_mode
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: