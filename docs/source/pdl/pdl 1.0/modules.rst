==================
PDL API Reference
==================

.. raw:: html

   <hr>

.. toctree::
   :hidden:

   group__group__ble__clk.rst
   group__group__canfd.rst
   group__group__crypto.rst
   group__group__csd.rst
   group__group__ctb.rst
   group__group__ctdac.rst
   group__group__dma.rst
   group__group__dmac.rst
   group__group__efuse.rst
   group__group__flash.rst
   group__group__gpio.rst
   group__group__i2s.rst
   group__group__ipc.rst
   group__group__lpcomp.rst
   group__group__lvd.rst
   group__group__mcwdt.rst
   group__group__pdm__pcm.rst
   group__group__pra.rst
   group__group__energy__profiler.rst
   group__group__prot.rst
   group__group__rtc.rst
   group__group__sar.rst
   group__group__scb.rst
   group__group__sd__host.rst
   group__group__seglcd.rst
   group__group__smartio.rst
   group__group__smif.rst
   group__group__system__config.rst
   group__group__sysanalog.rst
   group__group__sysclk.rst
   group__group__sysint.rst
   group__group__syslib.rst
   group__group__syspm.rst
   group__group__arm__system__timer.rst
   group__group__tcpwm.rst
   group__group__trigmux.rst
   group__group__usbfs__dev__drv.rst
   group__group__wdt.rst
  


The following provides a list of driver API documentation

 +----------------------------------+----------------------------------+
 |  \ `BLE ECO (BLE ECO             | This driver provides an API to   |
 | Clock)                           | manage the BLE ECO clock block   |
 | <group__group__ble__clk.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gro        |                                  |
 | up__ble__clk__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__grou               |                                  |
 | p__ble__clk__data__type.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__              |                                  |
 | group__ble__clk__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `CAN FD (CAN with Flexible    | The CAN FD driver provides an    |
 | Data-Rat                         | easy method to access the CAN FD |
 | e) <group__group__canfd.html>`__ | IP block registers and provides  |
 |                                  | simple functionality for sending |
 |                                  | and receiving data between       |
 |                                  | devices in the CAN FD network    |
 +----------------------------------+----------------------------------+
 |  \ `Macros <grou                 | This section describes the CAN   |
 | p__group__canfd__macros.html>`__ | FD Macros                        |
 +----------------------------------+----------------------------------+
 |  \ `RX Interrupt                 | This section contains interrupt  |
 | masks <group__group__canf        | bit masks to be used with:       |
 | d__rx__interrupt__masks.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `TX Interrupt                 | This section contains interrupt  |
 | masks <group__group__canf        | bit masks to use with:           |
 | d__tx__interrupt__masks.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Error Interrupt              | This section contains interrupt  |
 | masks <group__group__canfd__     | bit masks to be used with:       |
 | error__interrupt__masks.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt line selection     | Interrupt line selection masks   |
 | masks <group__group__canfd_      |                                  |
 | _interrupt__line__masks.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Protocol Status Register     | Masks and bit positions of the   |
 | (PSR)                            | Protocol Status Register (PSR)   |
 | masks <group__group__ca          | fields                           |
 | nfd__last__state__masks.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__           |                                  |
 | group__canfd__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__       |                                  |
 | canfd__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <gro                       |                                  |
 | up__group__canfd__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Crypto                       | The Crypto driver provides a     |
 | (Cryptography                    | public API to perform            |
 | ) <group__group__crypto.html>`__ | cryptographic and hash           |
 |                                  | operations, as well as generate  |
 |                                  | both true and pseudo random      |
 |                                  | numbers                          |
 +----------------------------------+----------------------------------+
 |  \ `Client-Server                | Use the client-server API to     |
 | Model <group__                   | isolate the Crypto hardware from |
 | group__crypto__cli__srv.html>`__ | non-secure application access    |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__group__c      |                                  |
 | rypto__cli__srv__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |                                  |                                  |
 | \ `Functions <group__group__cryp |                                  |
 | to__cli__srv__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Client                       |                                  |
 | Functions <group__group_         |                                  |
 | _crypto__cli__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Server                       |                                  |
 | Functions <group__group_         |                                  |
 | _crypto__srv__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Struc                            |                                  |
 | tures <group__group__crypto__cli |                                  |
 | __srv__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Common Data                  | The Crypto initialization        |
 | Structures <group__group__cr     | configuration                    |
 | ypto__config__structure.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Client Data                  |                                  |
 | Structures <group__group__crypto |                                  |
 | __cli__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Server Data                  |                                  |
 | Structures <group__group__crypto |                                  |
 | __srv__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Direct Crypto Core           | Use the low-level API for direct |
 | Access <group__                  | access to the Crypto hardware    |
 | group__crypto__lld__api.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Control and                  |                                  |
 | Status <group_                   |                                  |
 | _group__crypto__lld__hw.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__cry |                                  |
 | pto__lld__hw__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Symmetric Key Algorithms     |                                  |
 | (AES, DES,                       |                                  |
 | TDES) <group__group_             |                                  |
 | _crypto__lld__symmetric.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Fun                          |                                  |
 | ctions <group__group__crypto__ll |                                  |
 | d__symmetric__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Asymmetric Key Algorithms    |                                  |
 | (RSA, ECP,                       |                                  |
 | ECDSA) <group__group__           |                                  |
 | crypto__lld__asymmetric.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Func                         |                                  |
 | tions <group__group__crypto__lld |                                  |
 | __asymmetric__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__crypto_     |                                  |
 | _lld__asymmetric__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Hash Operations              |                                  |
 | (SHA) <group__                   |                                  |
 | group__crypto__lld__sha.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |                                  |                                  |
 | \ `Functions <group__group__cryp |                                  |
 | to__lld__sha__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Message Authentication Code  |                                  |
 | (CMAC,                           |                                  |
 | HMAC) <group__                   |                                  |
 | group__crypto__lld__mac.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |                                  |                                  |
 | \ `Functions <group__group__cryp |                                  |
 | to__lld__mac__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Cyclic Redundancy Code       |                                  |
 | (CRC) <group__                   |                                  |
 | group__crypto__lld__crc.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |                                  |                                  |
 | \ `Functions <group__group__cryp |                                  |
 | to__lld__crc__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Random Number Generation     |                                  |
 | (TRNG,                           |                                  |
 | PRNG) <group__                   |                                  |
 | group__crypto__lld__rng.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |                                  |                                  |
 | \ `Functions <group__group__cryp |                                  |
 | to__lld__rng__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Vector Unit                  |                                  |
 | (VU) <group_                     |                                  |
 | _group__crypto__lld__vu.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__cry |                                  |
 | pto__lld__vu__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Memory Streaming             |                                  |
 | Functions <group__               |                                  |
 | group__crypto__lld__mem.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |                                  |                                  |
 | \ `Functions <group__group__cryp |                                  |
 | to__lld__mem__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Common Data                  |                                  |
 | Structures <group__group__c      |                                  |
 | rypto__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Common Enumerated            |                                  |
 | Types <grou                      |                                  |
 | p__group__crypto__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `CSD (CapSense Sigma          | The CSD HW block enables         |
 | De                               | multiple sensing capabilities on |
 | lta) <group__group__csd.html>`__ | PSoC devices, including self-cap |
 |                                  | and mutual-cap capacitive touch  |
 |                                  | sensing solutions, a 10-bit ADC, |
 |                                  | IDAC, and Comparator             |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   |                                  |
 | oup__group__csd__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Registers                    |                                  |
 | Constants <group_                |                                  |
 | _group__csd__reg__const.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             |                                  |
 | __group__csd__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group         |                                  |
 | __csd__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <g                         |                                  |
 | roup__group__csd__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `CTB (Continuous Time         | This driver provides API         |
 | Bl                               | functions to configure and use   |
 | ock) <group__group__ctb.html>`__ | the analog CTB                   |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   |                                  |
 | oup__group__ctb__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             |                                  |
 | __group__ctb__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Initialization               | This set of functions are for    |
 | Functions <group__grou           | initializing, enabling, and      |
 | p__ctb__functions__init.html>`__ | disabling the CTB                |
 +----------------------------------+----------------------------------+
 |  \ `Basic Configuration          | This set of functions are for    |
 | Functions <group__group          | configuring basic usage of the   |
 | __ctb__functions__basic.html>`__ | CTB                              |
 +----------------------------------+----------------------------------+
 |  \ `Comparator                   | This set of functions are        |
 | Functions <group__group__ctb     | specific to the comparator mode  |
 | __functions__comparator.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Sample and Hold              | This function enables sample and |
 | Functions <group__group__ctb__   | hold of the CTDAC output         |
 | functions__sample__hold.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    | This set of functions is related |
 | Functions <group__group__ctb     | to the comparator interrupts     |
 | __functions__interrupts.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Switch Control               | This set of functions is for     |
 | Functions <group__group__c       | controlling routing switches     |
 | tb__functions__switches.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Offset and Slope Trim        | These are advanced functions for |
 | Functions <group__grou           | trimming the offset and slope of |
 | p__ctb__functions__trim.html>`__ | the opamps                       |
 +----------------------------------+----------------------------------+
 |  \ `Reference Current Mode       | This set of functions impacts    |
 | Functions <group__grou           | all opamps on the chip           |
 | p__ctb__functions__aref.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Global                       |                                  |
 | Variables <gro                   |                                  |
 | up__group__ctb__globals.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group         |                                  |
 | __ctb__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <g                         |                                  |
 | roup__group__ctb__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `CTDAC (Continuous Time       | The CTDAC driver provides APIs   |
 | Digital to Analog                | to configure the 12-bit          |
 | Converte                         | Continuous-Time DAC              |
 | r) <group__group__ctdac.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <grou                 |                                  |
 | p__group__ctdac__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__           |                                  |
 | group__ctdac__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Initialization               | This set of functions are for    |
 | Functions <group__group_         | initializing, enabling, and      |
 | _ctdac__functions__init.html>`__ | disabling the CTDAC              |
 +----------------------------------+----------------------------------+
 |  \ `Basic Configuration          | This set of functions are for    |
 | Functions <group__group__        | configuring basic usage of the   |
 | ctdac__functions__basic.html>`__ | CTDAC                            |
 +----------------------------------+----------------------------------+
 |  \ `Switch Control               | This set of functions is for     |
 | Functions <group__group__ctd     | controlling the two CTDAC analog |
 | ac__functions__switches.html>`__ | switches, CVD, and CO6           |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    | This set of functions is related |
 | Functions <group__group__ctdac   | to the CTDAC interrupt           |
 | __functions__interrupts.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | This driver supports one SysPm   |
 | Ca                               | callback for Deep Sleep          |
 | llback <group__group__ctdac__fun | transition                       |
 | ctions__syspm__callback.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Global                       |                                  |
 | Variables <group                 |                                  |
 | __group__ctdac__globals.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__       |                                  |
 | ctdac__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <gro                       |                                  |
 | up__group__ctdac__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `DMA (Direct Memory           | Configures a DMA channel and its |
 | Acc                              | descriptor(s)                    |
 | ess) <group__group__dma.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   |                                  |
 | oup__group__dma__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             |                                  |
 | __group__dma__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Block                        |                                  |
 | Functions <group__group          |                                  |
 | __dma__block__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Channel                      |                                  |
 | Functions <group__group__        |                                  |
 | dma__channel__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Descriptor                   |                                  |
 | Functions <group__group__dma     |                                  |
 | __descriptor__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group         |                                  |
 | __dma__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <g                         |                                  |
 | roup__group__dma__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `DMAC (Direct Memory Access   | Configures the DMA Controller    |
 | Controll                         | block, channels and descriptors  |
 | er) <group__group__dmac.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gro                  |                                  |
 | up__group__dmac__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    |                                  |
 | Masks <group__group__dmac__m     |                                  |
 | acros__interrupt__masks.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    |                                  |
 | Masks <group__group__dmac__m     |                                  |
 | acros__interrupt__masks.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group_            |                                  |
 | _group__dmac__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Block                        |                                  |
 | Functions <group__group_         |                                  |
 | _dmac__block__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Channel                      |                                  |
 | Functions <group__group__d       |                                  |
 | mac__channel__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Descriptor                   |                                  |
 | Functions <group__group__dmac    |                                  |
 | __descriptor__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group_        |                                  |
 | _dmac__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <gr                        |                                  |
 | oup__group__dmac__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `eFuse (Electronic            | Electronic Fuses (eFuses) are    |
 | Fuse                             | non-volatile memory where each   |
 | s) <group__group__efuse.html>`__ | bit is one-time programmable     |
 |                                  | (OTP)                            |
 +----------------------------------+----------------------------------+
 |  \ `Macros <grou                 |                                  |
 | p__group__efuse__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__           |                                  |
 | group__efuse__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__       |                                  |
 | efuse__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__e           |                                  |
 | fuse__enumerated__types.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Flash (Flash System          | Internal flash memory            |
 | Routin                           | programming                      |
 | e) <group__group__flash.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <grou                 |                                  |
 | p__group__flash__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Flash general                | Provides general information     |
 | parameters <group__group_        | about flash                      |
 | _flash__general__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Flash                        | Specifies the parameter values   |
 | configuration <group__group      | passed to SROM API               |
 | __flash__config__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__           |                                  |
 | group__flash__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__f           |                                  |
 | lash__enumerated__types.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `GPIO (General Purpose Input  | The GPIO driver provides an API  |
 | Outp                             | to configure and access device   |
 | ut) <group__group__gpio.html>`__ | Input/Output pins                |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gro                  |                                  |
 | up__group__gpio__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Pin drive                    | Constants to be used for setting |
 | mode <group__g                   | the drive mode of the pin        |
 | roup__gpio__drive_modes.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Voltage trip                 | Constants to be used for setting |
 | mode <gr                         | the voltage trip type on the pin |
 | oup__group__gpio__vtrip.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Slew Rate                    | Constants to be used for setting |
 | Mode <group_                     | the slew rate of the pin         |
 | _group__gpio__slew_rate.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Pin drive                    | Constants to be used for setting |
 | strength <group__grou            | the drive strength of the pin    |
 | p__gpio__drive_strength.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt trigger            | Constants to be used for setting |
 | type <group__group__             | the interrupt trigger type on    |
 | gpio__interrupt_trigger.html>`__ | the pin                          |
 +----------------------------------+----------------------------------+
 |  \ `SIO output buffer            | Constants to be used for setting |
 | mode <group                      | the SIO output buffer mode on    |
 | __group__gpio__sio_vreg.html>`__ | the pin                          |
 +----------------------------------+----------------------------------+
 |  \ `SIO input buffer             | Constants to be used for setting |
 | mode <group                      | the SIO input buffer mode on the |
 | __group__gpio__sio_ibuf.html>`__ | pin                              |
 +----------------------------------+----------------------------------+
 |  \ `SIO input buffer             | Constants to be used for setting |
 | trip-point <group_               | the SIO input buffer trip-point  |
 | _group__gpio__sio_vtrip.html>`__ | of the pin                       |
 +----------------------------------+----------------------------------+
 |  \ `SIO reference voltage for    | Constants to be used for setting |
 | input buffer                     | the reference voltage of SIO     |
 | trip-point <group                | input buffer trip-point          |
 | __group__gpio__sio_vref.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Regulated output voltage     | Constants to be used for setting |
 | level (Voh) and input buffer     | the Voh and input buffer         |
 | trip-point of an SIO             | trip-point of an SIO pair        |
 | pair <grou                       |                                  |
 | p__group__gpio__sio_voh.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group_            |                                  |
 | _group__gpio__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Initialization               |                                  |
 | Functions <group__group          |                                  |
 | __gpio__functions__init.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `GPIO                         |                                  |
 | Functions <group__group          |                                  |
 | __gpio__functions__gpio.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SIO                          |                                  |
 | Functions <group__grou           |                                  |
 | p__gpio__functions__sio.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Port Interrupt               |                                  |
 | Functions <group__group__gpi     |                                  |
 | o__functions__interrupt.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group_        |                                  |
 | _gpio__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <gr                        |                                  |
 | oup__group__gpio__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `I2S (Inter-IC                | The I2S driver provides a        |
 | So                               | function API to manage Inter-IC  |
 | und) <group__group__i2s.html>`__ | Sound                            |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   |                                  |
 | oup__group__i2s__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    |                                  |
 | Masks <group__group__i2s__m      |                                  |
 | acros__interrupt__masks.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Current                      |                                  |
 | State <group__group__i2s_        |                                  |
 | _macros__current__state.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             |                                  |
 | __group__i2s__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | The driver supports SysPm        |
 | Callback <group__group__i2s__fun | callback for Deep Sleep          |
 | ctions__syspm__callback.html>`__ | transition                       |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group         |                                  |
 | __i2s__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <g                         |                                  |
 | roup__group__i2s__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `IPC (Inter Process           | The inter-processor              |
 | Communicat                       | communication (IPC) driver       |
 | ion) <group__group__ipc.html>`__ | provides a safe and reliable     |
 |                                  | method to transfer data between  |
 |                                  | CPUs                             |
 +----------------------------------+----------------------------------+
 |  \ `IPC driver layer             | The functions of this layer are  |
 | (IPC_DRV)                        | used in the higher IPC levels    |
 | <group__group__ipc__drv.html>`__ | (Semaphores and Pipes)           |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   | Macro definitions are used in    |
 | oup__group__ipc__macros.html>`__ | the driver                       |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             | Functions are used in the driver |
 | __group__ipc__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         | Data structures are used in the  |
 | Structures <group__group         | driver                           |
 | __ipc__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   | Enumerations are used in the     |
 | Types <g                         | driver                           |
 | roup__group__ipc__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `IPC semaphores layer         | The semaphores layer functions   |
 | (IPC_SEMA) <g                    | made use of a single IPC channel |
 | roup__group__ipc__sema.html>`__  | to allow multiple semaphores     |
 |                                  | that can be used by system or    |
 |                                  | user function calls              |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__g             | Macro definitions are used in    |
 | roup__ipc__sema__macros.html>`__ | the driver                       |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__grou       | Functions are used in the driver |
 | p__ipc__sema__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   | Enumerations are used in the     |
 | Types <group__                   | driver                           |
 | group__ipc__sema__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `IPC pipes layer              | The Pipe functions provide a     |
 | (IPC_PIPE) <g                    | method to transfer one or more   |
 | roup__group__ipc__pipe.html>`__  | words of data between CPUs or    |
 |                                  | tasks                            |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__g             | Macro definitions are used in    |
 | roup__ipc__pipe__macros.html>`__ | the driver                       |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__grou       | Functions are used in the driver |
 | p__ipc__pipe__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         | Data structures are used in the  |
 | Structures <group__group__ipc_   | driver                           |
 | _pipe__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   | Enumerations are used in the     |
 | Types <group__                   | driver                           |
 | group__ipc__pipe__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `LPComp (Low Power            | Provides access to the low-power |
 | Comparator                       | comparators implemented using    |
 | ) <group__group__lpcomp.html>`__ | the fixed-function LP comparator |
 |                                  | block that is present in PSoC 6  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group                |                                  |
 | __group__lpcomp__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__g          |                                  |
 | roup__lpcomp__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | The driver supports SysPm        |
 | Cal                              | callback for Deep Sleep and      |
 | lback <group__group__lpcomp__fun | Hibernate transition             |
 | ctions__syspm__callback.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__l      |                                  |
 | pcomp__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <grou                      |                                  |
 | p__group__lpcomp__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `LVD                          | The LVD driver provides an API   |
 | (Low-Voltage-Det                 | to manage the Low Voltage        |
 | ect) <group__group__lvd.html>`__ | Detection block                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   |                                  |
 | oup__group__lvd__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             |                                  |
 | __group__lvd__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | The driver supports SysPm        |
 | Callback <group__group__lvd__fun | callback for Deep Sleep          |
 | ctions__syspm__callback.html>`__ | transition                       |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <g                         |                                  |
 | roup__group__lvd__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `MCWDT (Multi-Counter         | A MCWDT has two 16-bit counters  |
 | Watchdo                          | and one 32-bit counter           |
 | g) <group__group__mcwdt.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <grou                 |                                  |
 | p__group__mcwdt__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__           |                                  |
 | group__mcwdt__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__       |                                  |
 | mcwdt__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <gro                       |                                  |
 | up__group__mcwdt__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PDM_PCM (PDM-PCM             | The pulse-density modulation to  |
 | Converter)                       | pulse-code modulation (PDM-PCM)  |
 | <group__group__pdm__pcm.html>`__ | driver provides an API to manage |
 |                                  | PDM-PCM conversion               |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__              |                                  |
 | group__pdm__pcm__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    |                                  |
 | Masks <group__group__pdm__pcm__m |                                  |
 | acros__interrupt__masks.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gro        |                                  |
 | up__pdm__pcm__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | The driver supports SysPm        |
 | Callb                            | callback for Deep Sleep          |
 | ack <group__group__pdm__pcm__fun | transition                       |
 | ctions__syspm__callback.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__pdm    |                                  |
 | __pcm__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group_                    |                                  |
 | _group__pdm__pcm__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PRA (Protected Register      | The Protected Register Access    |
 | Acc                              | (PRA) driver used to provice     |
 | ess) <group__group__pra.html>`__ | access to the protected          |
 |                                  | registers to a non-secure        |
 |                                  | application on PSoC 64 devices   |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   |                                  |
 | oup__group__pra__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             |                                  |
 | __group__pra__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <g                         |                                  |
 | roup__group__pra__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__pra    |                                  |
 | __data__structures__cfg.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Group_p                      |                                  |
 | ra_data_structures <group__group |                                  |
 | __pra__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Profile (Energy              | The energy profiler driver is an |
 | Profiler) <group__               | API for configuring and using    |
 | group__energy__profiler.html>`__ | the profile hardware block       |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group_               |                                  |
 | _group__profile__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__profile__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    |                                  |
 | Functions <group__group__profil  |                                  |
 | e__functions__interrupt.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `General                      |                                  |
 | Functions <group__group__prof    |                                  |
 | ile__functions__general.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Counter                      |                                  |
 | Functions <group__group__prof    |                                  |
 | ile__functions__counter.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Calculation                  |                                  |
 | F                                |                                  |
 | unctions <group__group__profile_ |                                  |
 | _functions__calculation.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__pr     |                                  |
 | ofile__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group                     |                                  |
 | __group__profile__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Prot (Protection             | The Protection Unit driver       |
 | Un                               | provides an API to configure the |
 | it) <group__group__prot.html>`__ | Memory Protection Units (MPU),   |
 |                                  | Shared Memory Protection Units   |
 |                                  | (SMPU), and Peripheral           |
 |                                  | Protection Units (PPU)           |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gro                  |                                  |
 | up__group__prot__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group_            |                                  |
 | _group__prot__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Bus Master and PC            |                                  |
 | Functions <group__group__pro     |                                  |
 | t__functions__busmaster.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `MPU                          |                                  |
 | Functions <group__grou           |                                  |
 | p__prot__functions__mpu.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SMPU                         |                                  |
 | Functions <group__group          |                                  |
 | __prot__functions__smpu.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PPU Programmable (PROG) v2   |                                  |
 | Functions <group__group__prot__f |                                  |
 | unctions__ppu__prog__v2.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PPU Fixed (FIXED) v2         |                                  |
 | F                                |                                  |
 | unctions <group__group__prot__fu |                                  |
 | nctions__ppu__fixed__v2.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PPU Programmable (PROG) v1   |                                  |
 | Functions <group__group__pro     |                                  |
 | t__functions__ppu__prog.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PPU Group (GR) v1            |                                  |
 | Functions <group__group__p       |                                  |
 | rot__functions__ppu__gr.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PPU Slave (SL) v1            |                                  |
 | Functions <group__group__p       |                                  |
 | rot__functions__ppu__sl.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PPU Region (RG) v1           |                                  |
 | Functions <group__group__p       |                                  |
 | rot__functions__ppu__rg.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group_        |                                  |
 | _prot__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <gr                        |                                  |
 | oup__group__prot__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `RTC (Real-Time               | The Real-Time Clock (RTC) driver |
 | Cl                               | provides an application          |
 | ock) <group__group__rtc.html>`__ | interface for keeping track of   |
 |                                  | time and date                    |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   |                                  |
 | oup__group__rtc__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Day of the week              | Definitions of days in the week  |
 | definitions <group__group__      |                                  |
 | rtc__day__of__the__week.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Week of month                | Week of Month setting constants  |
 | definitions <group__group__rt    | definitions for Daylight Saving  |
 | c__dst__week__of__month.html>`__ | Time feature                     |
 +----------------------------------+----------------------------------+
 |  \ `Month                        | Constants definition for Months  |
 | definitions <g                   |                                  |
 | roup__group__rtc__month.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Number of days in month      | Definition of days in current    |
 | definitions <group__grou         | month                            |
 | p__rtc__days__in__month.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `RTC Interrupt                | Definitions for RTC interrupt    |
 | sources <group__group__          | sources                          |
 | rtc__macros__interrupts.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `RTC Status                   | Definitions for indicating the   |
 | definitions <group__g            | RTC BUSY bit                     |
 | roup__rtc__busy__status.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             |                                  |
 | __group__rtc__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `General <group__group__      |                                  |
 | rtc__general__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Alarm <group__group          |                                  |
 | __rtc__alarm__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `DST                          |                                  |
 | functions <group__gro            |                                  |
 | up__rtc__dst__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low-Level <group__group__rtc |                                  |
 | __low__level__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt <group__group__rt  |                                  |
 | c__interrupt__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    |                                  |
 | Callbacks <group__group__rtc     |                                  |
 | __low__power__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group         |                                  |
 | __rtc__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <g                         |                                  |
 | roup__group__rtc__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SAR (SAR ADC                 | This driver configures and       |
 | Subsys                           | controls the SAR ADC subsystem   |
 | tem) <group__group__sar.html>`__ | block                            |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   |                                  |
 | oup__group__sar__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             |                                  |
 | __group__sar__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Initialization and Basic     | This set of functions is for     |
 | Functions <group__group          | initialization and basic usage   |
 | __sar__functions__basic.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | This set of functions is for     |
 | Callback <group__group           | Deep Sleep entry and exit        |
 | __sar__functions__power.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Run-time Configuration       | This set of functions allows     |
 | Functions <group__group_         | changes to the SAR configuration |
 | _sar__functions__config.html>`__ | after initialization             |
 +----------------------------------+----------------------------------+
 |  \ `Counts Conversion            | This set of functions performs   |
 | Functions <group__group__s       | counts to \*volts conversions    |
 | ar__functions__countsto.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    | This set of functions are        |
 | Functions <group__group__sa      | related to SAR interrupts        |
 | r__functions__interrupt.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SARMUX Switch Control        | This set of functions is for     |
 | Functions <group__group__s       | controlling/querying the SARMUX  |
 | ar__functions__switches.html>`__ | switches                         |
 +----------------------------------+----------------------------------+
 |  \ `Useful Configuration Query   | This set of functions is for     |
 | Functions <group__group_         | useful configuration query       |
 | _sar__functions__helper.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group         |                                  |
 | __sar__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <g                         |                                  |
 | roup__group__sar__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Control Register             | This set of enumerations aids in |
 | Enums <group__group__sar         | configuring the SAR CTRL         |
 | __ctrl__register__enums.html>`__ | register                         |
 +----------------------------------+----------------------------------+
 |  \ `Sample Control Register      | This set of enumerations are     |
 | Enums <group__group__sar__sample | used in configuring the SAR      |
 | __ctrl__register__enums.html>`__ | SAMPLE_CTRL register             |
 +----------------------------------+----------------------------------+
 |  \ `Sample Time Register         | This set of enumerations aids in |
 | Enums <group__group__sar__sam    | configuring the SAR              |
 | ple__time__shift__enums.html>`__ | SAMPLE_TIME\* registers          |
 +----------------------------------+----------------------------------+
 |  \ `Range Interrupt Register     | This set of enumerations aids in |
 | Enums <group__group__sar__range_ | configuring the SAR RANGE\*      |
 | _thres__register__enums.html>`__ | registers                        |
 +----------------------------------+----------------------------------+
 |  \ `Channel Configuration        | This set of enumerations aids in |
 | Register                         | configuring the SAR CHAN_CONFIG  |
 | Enums <group__group__sar__chan__ | register                         |
 | config__register__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt Mask Register      | This set of enumerations aids in |
 | E                                | configuring the SAR INTR_MASK    |
 | nums <group__group__sar__intr__m | register                         |
 | ask__t__register__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SARMUX Switch Control        | This set of enumerations aids in |
 | Register                         | configuring the `uint32_t        |
 | Enums <group__group__sar__mux__  | muxSw                            |
 | switch__register__enums.html>`__ | itch <group__group__sar.html#gro |
 |                                  | up_sar_init_struct_muxSwitch>`__ |
 |                                  | and `uint32_t                    |
 |                                  | muxSwitchSqCtrl <g               |
 |                                  | roup__group__sar.html#group_sar  |
 |                                  | _init_struct_muxSwitchSqCtrl>`__ |
 |                                  | registers                        |
 +----------------------------------+----------------------------------+
 |  \ `SCB (Serial Communication    | The Serial Communications Block  |
 | Bl                               | (SCB) supports three serial      |
 | ock) <group__group__scb.html>`__ | communication protocols: Serial  |
 |                                  | Peripheral Interface (SPI),      |
 |                                  | Universal Asynchronous Receiver  |
 |                                  | Transmitter (UART), and Inter    |
 |                                  | Integrated Circuit (I2C or IIC)  |
 +----------------------------------+----------------------------------+
 |  \ `Common <gr                   | Common API for the Serial        |
 | oup__group__scb__common.html>`__ | Communication Block              |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__gro           |                                  |
 | up__scb__common__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SCB Interrupt                |                                  |
 | Causes <group__group__scb__comm  |                                  |
 | on__macros__intr__cause.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `TX Interrupt                 |                                  |
 | Statuses <group__group__scb__c   |                                  |
 | ommon__macros__tx__intr.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `RX Interrupt                 |                                  |
 | Statuses <group__group__scb__c   |                                  |
 | ommon__macros__rx__intr.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Slave Interrupt              |                                  |
 | S                                |                                  |
 | tatuses <group__group__scb__comm |                                  |
 | on__macros__slave__intr.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Master Interrupt             |                                  |
 | St                               |                                  |
 | atuses <group__group__scb__commo |                                  |
 | n__macros__master__intr.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `I2C Interrupt                |                                  |
 | Statuses <group__group__scb__co  |                                  |
 | mmon__macros__i2c__intr.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SPI Interrupt                |                                  |
 | Statuses                         |                                  |
 | <group__group__scb__common__mac  |                                  |
 | ros___spi_intr_statuses.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group_     |                                  |
 | _scb__common__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__scb__c |                                  |
 | ommon__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `EZI2C                        | Driver API for EZI2C Slave       |
 | (SCB) <g                         | Peripheral                       |
 | roup__group__scb__ezi2c.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__gr            |                                  |
 | oup__scb__ezi2c__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `EZI2C Activity               | Macros to check current EZI2C    |
 | Status <group__group__scb__ezi2c | activity slave status returned   |
 | __macros__get__activity.html>`__ | by                               |
 |                                  | `Cy_SCB_EZI2C_Get                |
 |                                  | Activity <group__group__scb__ezi |
 |                                  | 2c__slave__functions.html#ga8cd2 |
 |                                  | 4e8c6c0bb996f6266443e1353f39>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group      |                                  |
 | __scb__ezi2c__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `G                            |                                  |
 | eneral <group__group__scb__ez    |                                  |
 | i2c__general__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Slave <group__group__scb__   |                                  |
 | ezi2c__slave__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    |                                  |
 | Cal                              |                                  |
 | lbacks <group__group__scb__ezi2c |                                  |
 | __low__power__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__scb__  |                                  |
 | ezi2c__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__g                  |                                  |
 | roup__scb__ezi2c__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `I2C                          | Driver API for I2C Bus           |
 | (SCB)                            | Peripheral                       |
 | <group__group__scb__i2c.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__              |                                  |
 | group__scb__i2c__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `I2C Slave                    | Macros to check current I2C      |
 | Status <group__group__scb__i2c   | slave status returned by         |
 | __macros__slave__status.html>`__ | `Cy_SCB_I2C_Slav                 |
 |                                  | eGetStatus <group__group__scb__i |
 |                                  | 2c__slave__functions.html#gab3e1 |
 |                                  | c474d6a5c7c5547ea08aa5d59e22>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `I2C Master                   | Macros to check current I2C      |
 | Status <group__group__scb__i2c_  | master status returned by        |
 | _macros__master__status.html>`__ | `Cy_SCB_I2C_MasterGetStatus <gr  |
 |                                  | oup__group__scb__i2c__master__hi |
 |                                  | gh__level__functions.html#ga652a |
 |                                  | 3b9da6db2424fed62c28c6347ef3>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `I2C Callback                 | Macros to check I2C events       |
 | E                                | passed by                        |
 | vents <group__group__scb__i2c__m | `cy_cb_scb_i2c_handl             |
 | acros__callback__events.html>`__ | e_events_t <group__group__scb__i |
 |                                  | 2c__data__structures.html#ga5d6d |
 |                                  | f20d77d2690048aaef8fe9e57e2e>`__ |
 |                                  | callback                         |
 +----------------------------------+----------------------------------+
 |  \ `I2C Address Callback         | Macros to check I2C address      |
 | Events                           | events passed by                 |
 | <group__group__scb__i2c__macros_ | `cy_cb_scb_i2c_han               |
 | _addr__callback__events.html>`__ | dle_addr_t <group__group__scb__i |
 |                                  | 2c__data__structures.html#gab035 |
 |                                  | 1020deb72a39fe1695e88f8aa8c0>`__ |
 |                                  | callback                         |
 +----------------------------------+----------------------------------+
 | \ `Functions <group__gro         |                                  |
 | up__scb__i2c__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `General <group__group__scb__ |                                  |
 | i2c__general__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Slave <group__group__scb     |                                  |
 | __i2c__slave__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Master                       |                                  |
 | High-Level                       |                                  |
 | <group__group__scb__i2c__master_ |                                  |
 | _high__level__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Master                       |                                  |
 | Low-Level                        |                                  |
 | <group__group__scb__i2c__master  |                                  |
 | __low__level__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `I                            |                                  |
 | nterrupt <group__group__scb__i2  |                                  |
 | c__interrupt__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    |                                  |
 | C                                |                                  |
 | allbacks <group__group__scb__i2c |                                  |
 | __low__power__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__scb    |                                  |
 | __i2c__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group_                    |                                  |
 | _group__scb__i2c__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SPI                          | Driver API for SPI Peripheral    |
 | (SCB)                            |                                  |
 | <group__group__scb__spi.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__              |                                  |
 | group__scb__spi__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SPI TX FIFO                  | Macros to check SPI TX FIFO      |
 | Sta                              | status returned by               |
 | tuses <group__group__scb__spi__m | `Cy_SCB_SPI_GetTxFifoS           |
 | acros__tx__fifo__status.html>`__ | tatus <group__group__scb__spi__l |
 |                                  | ow__level__functions.html#ga987f |
 |                                  | dbe86c1fbfbadfa51cdf82f84632>`__ |
 |                                  | function or assign mask for      |
 |                                  | `Cy_SCB_SPI_ClearTxFifoS         |
 |                                  | tatus <group__group__scb__spi__l |
 |                                  | ow__level__functions.html#gadf60 |
 |                                  | 44cb7d9567450502fec59537dad4>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `SPI RX FIFO                  | Macros to check SPI RX FIFO      |
 | Sta                              | status returned by               |
 | tuses <group__group__scb__spi__m | `Cy_SCB_SPI_GetRxFifoS           |
 | acros__rx__fifo__status.html>`__ | tatus <group__group__scb__spi__l |
 |                                  | ow__level__functions.html#ga50ac |
 |                                  | b3879906a729e5821fcc87d4f017>`__ |
 |                                  | function or assign mask for      |
 |                                  | `Cy_SCB_SPI_ClearRxFifoS         |
 |                                  | tatus <group__group__scb__spi__l |
 |                                  | ow__level__functions.html#gae08b |
 |                                  | 31f2964a1c0895531d05487ad383>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `SPI Master and Slave         | Macros to check SPI Mater and    |
 | Statuses                         | Slave status returned by         |
 | <group__group__scb__spi__macros  | `Cy_SCB_SPI_GetSlaveMasterS      |
 | __master__slave__status.html>`__ | tatus <group__group__scb__spi__l |
 |                                  | ow__level__functions.html#gac9e9 |
 |                                  | a4b77a8db35ccbda974e7f9d9d23>`__ |
 |                                  | function or assign mask for      |
 |                                  | `Cy_SCB_SPI_ClearSlaveMasterS    |
 |                                  | tatus <group__group__scb__spi__l |
 |                                  | ow__level__functions.html#ga085a |
 |                                  | 0e7fb73b9feb3b66ca5017449e6b>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `SPI Transfer                 | Macros to check current SPI      |
 | Status <group__group__scb__sp    | transfer status returned by      |
 | i__macros__xfer__status.html>`__ | `Cy_SCB_SPI_GetTransferSt        |
 |                                  | atus <group__group__scb__spi__hi |
 |                                  | gh__level__functions.html#ga9506 |
 |                                  | 9817e25be749ae989e5d7131f8d0>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `SPI Callback                 | Macros to check SPI events       |
 | E                                | passed by                        |
 | vents <group__group__scb__spi__m | `cy_cb_scb_spi_handl             |
 | acros__callback__events.html>`__ | e_events_t <group__group__scb__s |
 |                                  | pi__data__structures.html#gaa02c |
 |                                  | 98c323acc350f3b4780f7833785f>`__ |
 |                                  | callback                         |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gro        |                                  |
 | up__scb__spi__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `General <group__group__scb__ |                                  |
 | spi__general__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Hig                          |                                  |
 | h-Level <group__group__scb__spi_ |                                  |
 | _high__level__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `L                            |                                  |
 | ow-Level <group__group__scb__spi |                                  |
 | __low__level__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `I                            |                                  |
 | nterrupt <group__group__scb__sp  |                                  |
 | i__interrupt__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    |                                  |
 | C                                |                                  |
 | allbacks <group__group__scb__spi |                                  |
 | __low__power__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__scb    |                                  |
 | __spi__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group_                    |                                  |
 | _group__scb__spi__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `UART                         | Driver API for UART              |
 | (SCB) <grou                      |                                  |
 | p__group__scb__uart.html>`__     |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__g             |                                  |
 | roup__scb__uart__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `UART IRDA Low Power          |                                  |
 | Oversample                       |                                  |
 | factors <group__group__scb__uart |                                  |
 | __macros__irda__lp__ovs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `UART RX FIFO                 | Macros to check UART RX FIFO     |
 | sta                              | status returned by               |
 | tus. <group__group__scb__uart__m | `Cy_SCB_UART_GetRxFifoSt         |
 | acros__rx__fifo__status.html>`__ | atus <group__group__scb__uart__l |
 |                                  | ow__level__functions.html#gaa2d7 |
 |                                  | 83e45d8b8a9803bb5b667d71f3b9>`__ |
 |                                  | function or assign mask for      |
 |                                  | `Cy_SCB_UART_ClearRxFifoSt       |
 |                                  | atus <group__group__scb__uart__l |
 |                                  | ow__level__functions.html#ga3f1e |
 |                                  | 6aa6e686a215ab03b5d264510b3f>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `UART TX FIFO                 | Macros to check UART TX FIFO     |
 | Stat                             | status returned by               |
 | uses <group__group__scb__uart__m | `Cy_SCB_UART_GetTxFifoSt         |
 | acros__tx__fifo__status.html>`__ | atus <group__group__scb__uart__l |
 |                                  | ow__level__functions.html#ga7621 |
 |                                  | 8bc016256d8525c9d779a4c2e9aa>`__ |
 |                                  | function or assign mask for      |
 |                                  | `Cy_SCB_UART_ClearTxFifoSt       |
 |                                  | atus <group__group__scb__uart__l |
 |                                  | ow__level__functions.html#gabd46 |
 |                                  | 07b147b35fbd9c47e89f77aa1f89>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `UART Receive                 | Macros to check current UART     |
 | Sta                              | receive status returned by       |
 | tuses <group__group__scb__uart__ | `Cy_SCB_UART_GetReceiveSta       |
 | macros__receive__status.html>`__ | tus <group__group__scb__uart__hi |
 |                                  | gh__level__functions.html#ga4cc7 |
 |                                  | bd2f28ef5e170e663414dba66f8c>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `UART Transmit                | Macros to check current UART     |
 | St                               | transmit status returned by      |
 | atus <group__group__scb__uart__m | `Cy_SCB_UART_GetTransmitSta      |
 | acros__transmit__status.html>`__ | tus <group__group__scb__uart__hi |
 |                                  | gh__level__functions.html#ga92ea |
 |                                  | 29d41db81f995b0ff442d6639191>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `UART Callback                | Macros to check UART events      |
 | Ev                               | passed by                        |
 | ents <group__group__scb__uart__m | `cy_cb_scb_uart_handle           |
 | acros__callback__events.html>`__ | _events_t <group__group__scb__ua |
 |                                  | rt__data__structures.html#ga67e5 |
 |                                  | d87e39311f4d001723f8718210c6>`__ |
 |                                  | callback                         |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__grou       |                                  |
 | p__scb__uart__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |                                  |                                  |
 | \ `General <group__group__scb__u |                                  |
 | art__general__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `High                         |                                  |
 | -Level <group__group__scb__uart_ |                                  |
 | _high__level__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Lo                           |                                  |
 | w-Level <group__group__scb__uart |                                  |
 | __low__level__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `I                            |                                  |
 | nterrupt <group__group__scb__uar |                                  |
 | t__interrupt__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    |                                  |
 | Ca                               |                                  |
 | llbacks <group__group__scb__uart |                                  |
 | __low__power__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__scb_   |                                  |
 | _uart__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__                   |                                  |
 | group__scb__uart__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SD Host (SD Host             | This driver provides the user an |
 | Controller)                      | easy method for accessing        |
 | <group__group__sd__host.html>`__ | standard Host Controller         |
 |                                  | Interface (HCI) registers and    |
 |                                  | provides some simple             |
 |                                  | functionality on top of the HCI  |
 |                                  | for reading and writing data to  |
 |                                  | an SD card, eMMc card or a SDIO  |
 |                                  | device                           |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__              |                                  |
 | group__sd__host__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `General Purpose              |                                  |
 | M                                |                                  |
 | acros <group__group__sd__host__m |                                  |
 | acros__general__purpose.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Card                         | The masks below can be used to   |
 | States <group__group__sd__hos    | check the CURRENT_STATE bitfield |
 | t__macros__card__states.html>`__ | of the                           |
 |                                  | `Cy_SD_Host_GetCardS             |
 |                                  | tatus <group__group__sd__host__l |
 |                                  | ow__level__functions.html#gacfa5 |
 |                                  | 88e2d3797fab335cc8880d59d0ee>`__ |
 |                                  | function return value            |
 +----------------------------------+----------------------------------+
 |  \ `Card Status (CMD13)          | The masks below can be used with |
 | Bits <group__group__sd__hos      | the                              |
 | t__macros__card__status.html>`__ | `Cy_SD_Host_GetCardS             |
 |                                  | tatus <group__group__sd__host__l |
 |                                  | ow__level__functions.html#gacfa5 |
 |                                  | 88e2d3797fab335cc8880d59d0ee>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `SCR Register                 | The masks below can be used with |
 | Masks <group__group              | the                              |
 | __sd__host__macros__scr.html>`__ | `Cy_SD_Host_G                    |
 |                                  | etScr <group__group__sd__host__l |
 |                                  | ow__level__functions.html#ga4dd9 |
 |                                  | d7e623f1c27c04490c48e9698e54>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `CID Register                 | The masks below can be used with |
 | Masks <group__group              | the                              |
 | __sd__host__macros__cid.html>`__ | `Cy_SD_Host_G                    |
 |                                  | etCid <group__group__sd__host__l |
 |                                  | ow__level__functions.html#ga696e |
 |                                  | f05ed804ae1fb21147bdded903a9>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `CSD Register                 | The masks below are for CSD      |
 | Masks <group__group              | Register Version 2.0 and can be  |
 | __sd__host__macros__csd.html>`__ | used with the                    |
 |                                  | `Cy_SD_Host_G                    |
 |                                  | etCsd <group__group__sd__host__l |
 |                                  | ow__level__functions.html#ga1942 |
 |                                  | 6622f0a7b92579030e0c954f4bc9>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `SD Host                      | The constants below can be used  |
 | Events <group__group__s          | with                             |
 | d__host__macros__events.html>`__ | `Cy_SD_Host_GetNormalInterruptS  |
 |                                  | tatus <group__group__sd__host__i |
 |                                  | nterrupt__functions.html#gad4a09 |
 |                                  | 951afc276a483c93e5e158528f3>`__, |
 |                                  | `                                |
 |                                  | Cy_SD_Host_ClearNormalInterruptS |
 |                                  | tatus <group__group__sd__host__i |
 |                                  | nterrupt__functions.html#ga95975 |
 |                                  | e199a0669a600e2fe5ada69b892>`__, |
 |                                  | `Cy_SD_Host_GetErrorInterrupt    |
 |                                  | Status <group__group__sd__host__ |
 |                                  | interrupt__functions.html#ga41fa |
 |                                  | 86242b7f84442e589055d3130f03>`__ |
 |                                  | and                              |
 |                                  | `Cy_SD_Host_ClearErrorInterrupt  |
 |                                  | Status <group__group__sd__host__ |
 |                                  | interrupt__functions.html#ga1ade |
 |                                  | 65f4d6a71808cf12a4b0957213a1>`__ |
 |                                  | functions                        |
 +----------------------------------+----------------------------------+
 |  \ `SD Host Present              | The constants below can be used  |
 | Status <group__group__sd__host__ | with the                         |
 | macros__present__status.html>`__ | `Cy_SD_Host_GetPresent           |
 |                                  | State <group__group__sd__host__l |
 |                                  | ow__level__functions.html#gaf357 |
 |                                  | 1becec1bbf64c3ba0fae36fe65fc>`__ |
 |                                  | function                         |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gro        |                                  |
 | up__sd__host__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Hig                          |                                  |
 | h-Level <group__group__sd__host_ |                                  |
 | _high__level__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `L                            |                                  |
 | ow-Level <group__group__sd__host |                                  |
 | __low__level__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    |                                  |
 | <group__group__sd__hos           |                                  |
 | t__interrupt__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__sd_    |                                  |
 | _host__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group_                    |                                  |
 | _group__sd__host__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SegLCD (Segment              | The Segment LCD Driver provides  |
 | LCD                              | an API to configure and operate  |
 | ) <group__group__seglcd.html>`__ | the MXLCD hardware block         |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group                |                                  |
 | __group__seglcd__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__g          |                                  |
 | roup__seglcd__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Block Configuration          |                                  |
 | Functions <group__group__se      |                                  |
 | glcd__functions__config.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Frame/Pixel Management       |                                  |
 | Functions <group__group__s       |                                  |
 | eglcd__functions__frame.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Display/Character Management |                                  |
 | Functions <group__group__seg     |                                  |
 | lcd__functions__display.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__s      |                                  |
 | eglcd__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Global                       |                                  |
 | Data <group_                     |                                  |
 | _group__seglcd__globals.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <grou                      |                                  |
 | p__group__seglcd__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SmartIO (Smart               | The Smart I/O driver provides an |
 | I/O)                             | API to configure and access the  |
 | <group__group__smartio.html>`__  | Smart I/O hardware present       |
 |                                  | between the GPIOs (pins) and     |
 |                                  | HSIOMs (pin muxes) on select     |
 |                                  | device ports                     |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group_               |                                  |
 | _group__smartio__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Smart I/O channel            | Constants for selecting the      |
 | selection <group__g              | Smart I/O channels               |
 | roup__smartio__channels.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__smartio__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Initialization               |                                  |
 | Functions <group__group__s       |                                  |
 | martio__functions__init.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `General                      |                                  |
 | Functions <group__group__smar    |                                  |
 | tio__functions__general.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `LUT                          |                                  |
 | Functions <group__group__        |                                  |
 | smartio__functions__lut.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data Unit                    |                                  |
 | Functions <group__group_         |                                  |
 | _smartio__functions__du.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__sm     |                                  |
 | artio__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group                     |                                  |
 | __group__smartio__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SMIF (Serial Memory          | The SPI-based communication      |
 | Interfa                          | interface to the external quad   |
 | ce) <group__group__smif.html>`__ | SPI (QSPI) high-speed memory     |
 |                                  | devices                          |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gro                  |                                  |
 | up__group__smif__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Status                       |                                  |
 | Macros <group__grou              |                                  |
 | p__smif__macros__status.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Command                      |                                  |
 | Macros <group__g                 |                                  |
 | roup__smif__macros__cmd.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `External Memory              |                                  |
 | Flags <group__gro                |                                  |
 | up__smif__macros__flags.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SFDP                         |                                  |
 | Macros <group__gr                |                                  |
 | oup__smif__macros__sfdp.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    |                                  |
 | Macros <group__g                 |                                  |
 | roup__smif__macros__isr.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group_            |                                  |
 | _group__smif__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Level                    | Basic flow for read/write        |
 | Functions <group__group__smif    | commands using                   |
 | __low__level__functions.html>`__ | `Cy_SMIF_Transmi                 |
 |                                  | tCommand <group__group__smif__lo |
 |                                  | w__level__functions.html#ga5abea |
 |                                  | 630fca93c72faece8b23549cd11>`__, |
 |                                  | `Cy_SMIF_Tran                    |
 |                                  | smitData <group__group__smif__lo |
 |                                  | w__level__functions.html#gae84cd |
 |                                  | f32e45e492e80dea38f9d3a7586>`__, |
 |                                  | `Cy_SMIF_Re                      |
 |                                  | ceiveData <group__group__smif__l |
 |                                  | ow__level__functions.html#ga1b36 |
 |                                  | 5e1400ccd6580533a0c92deb48c7>`__ |
 |                                  | and                              |
 |                                  | `Cy_SMIF_SendDu                  |
 |                                  | mmyCycles <group__group__smif__l |
 |                                  | ow__level__functions.html#gae40a |
 |                                  | 30c84a9a5956d03279b9ee8c3999>`__ |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | The driver supports SysPm        |
 | C                                | callback for Deep Sleep and      |
 | allback <group__group__smif__fun | Hibernate transition             |
 | ctions__syspm__callback.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Memory Slot                  |                                  |
 | Functions <group__group__smi     |                                  |
 | f__mem__slot__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | The driver supports SysPm        |
 | C                                | callback for Deep Sleep and      |
 | allback <group__group__smif__fun | Hibernate transition             |
 | ctions__syspm__callback.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group_        |                                  |
 | _smif__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SMIF Memory Description      | General hierarchy of memory      |
 | St                               | structures are:                  |
 | ructures <group__group__smif__da |                                  |
 | ta__structures__memslot.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <gr                        |                                  |
 | oup__group__smif__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Startup (System              | Provides device startup, system  |
 | Configuration                    | configuration, and linker script |
 | Files) <group                    | files                            |
 | __group__system__config.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macro <group__group          |                                  |
 | __system__config__macro.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `System <group__group__system |                                  |
 | __config__system__macro.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Cortex-M4                    |                                  |
 | S                                |                                  |
 | tatus <group__group__system__con |                                  |
 | fig__cm4__status__macro.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `User                         |                                  |
 | Settin                           |                                  |
 | gs <group__group__system__config |                                  |
 | __user__settings__macro.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__sy  |                                  |
 | stem__config__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `System                       |                                  |
 | <group__group__system__co        |                                  |
 | nfig__system__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Cortex-M4                    |                                  |
 | Control <group__group__system_   |                                  |
 | _config__cm4__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Global                       |                                  |
 | Variables <group__group__        |                                  |
 | system__config__globals.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SysAnalog (System Analog     | This driver provides an          |
 | Reference                        | interface for configuring the    |
 | Block) <grou                     | Analog Reference (AREF) block    |
 | p__group__sysanalog.html>`__     | and querying the INTR_CAUSE      |
 |                                  | register of the PASS             |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__g             |                                  |
 | roup__sysanalog__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__grou       |                                  |
 | p__sysanalog__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Global                       |                                  |
 | Variables <group__gr             |                                  |
 | oup__sysanalog__globals.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__sysa   |                                  |
 | nalog__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__                   |                                  |
 | group__sysanalog__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SysClk (System               | The System Clock (SysClk) driver |
 | Clock                            | contains the API for configuring |
 | ) <group__group__sysclk.html>`__ | system and peripheral clocks     |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group                |                                  |
 | __group__sysclk__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `ECO                          | Constants used for expressing    |
 | status <group__g                 | ECO status                       |
 | roup__sysclk__ecostatus.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `General Enumerated           |                                  |
 | Types <grou                      |                                  |
 | p__group__sysclk__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Function return              |                                  |
 | values <group_                   |                                  |
 | _group__sysclk__returns.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `External Clock Source        | The External Clock Source        |
 | (EXTCLK) <gr                     | (EXTCLK) is a clock source       |
 | oup__group__sysclk__ext.html>`__ | routed into PSoC through a GPIO  |
 |                                  | pin                              |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__sysclk__ext__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `External Crystal Oscillator  | The External Crystal Oscillator  |
 | (ECO) <gr                        | (ECO) is a clock source that     |
 | oup__group__sysclk__eco.html>`__ | consists of an oscillator        |
 |                                  | circuit that drives an external  |
 |                                  | crystal through its dedicated    |
 |                                  | ECO pins                         |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__sysclk__eco__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Clock Path                   | Clock paths are a series of      |
 | Source <group__g                 | multiplexers that allow a source |
 | roup__sysclk__path__src.html>`__ | clock to drive multiple clocking |
 |                                  | resources down the chain         |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__s   |                                  |
 | ysclk__path__src__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__s           |                                  |
 | ysclk__path__src__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Frequency Locked Loop        | The FLL is a clock generation    |
 | (FLL) <gr                        | circuit that can be used to      |
 | oup__group__sysclk__fll.html>`__ | produce a higher frequency clock |
 |                                  | from a reference clock           |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__sysclk__fll__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__grou          |                                  |
 | p__sysclk__fll__structs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__gr                 |                                  |
 | oup__sysclk__fll__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Phase Locked Loop            | The PLL is a clock generation    |
 | (PLL) <gr                        | circuit that can be used to      |
 | oup__group__sysclk__pll.html>`__ | produce a higher frequency clock |
 |                                  | from a reference clock           |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__sysclk__pll__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__grou          |                                  |
 | p__sysclk__pll__structs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Internal Low-Speed           | The ILO operates with no         |
 | Oscillator                       | external components and outputs  |
 | (ILO) <gr                        | a stable clock at 32.768 kHz     |
 | oup__group__sysclk__ilo.html>`__ | nominal                          |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__sysclk__ilo__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Precision Internal           | PILO provides a higher accuracy  |
 | Low-Speed Oscillator             | 32.768 kHz clock than the        |
 | (PILO) <gro                      | `ILO <gr                         |
 | up__group__sysclk__pilo.html>`__ | oup__group__sysclk__ilo.html>`__ |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gro        |                                  |
 | up__sysclk__pilo__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Clock                        | These functions measure the      |
 | Measurement <group               | frequency of a specified clock   |
 | __group__sysclk__calclk.html>`__ | relative to a reference clock    |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group      |                                  |
 | __sysclk__calclk__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group              |                                  |
 | __sysclk__calclk__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Clock Trim (ILO,             | These functions perform a single |
 | PILO) <gro                       | trim operation on the ILO or     |
 | up__group__sysclk__trim.html>`__ | PILO                             |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gro        |                                  |
 | up__sysclk__trim__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | Entering and exiting low power   |
 | Callback <g                      | modes require compatible clock   |
 | roup__group__sysclk__pm.html>`__ | configurations to be set before  |
 |                                  | entering low power and restored  |
 |                                  | upon wake-up and exit            |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__g          |                                  |
 | roup__sysclk__pm__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Watch Crystal Oscillator     | The WCO is a highly accurate     |
 | (WCO) <gr                        | 32.768 kHz clock source capable  |
 | oup__group__sysclk__wco.html>`__ | of operating in all power modes  |
 |                                  | (excluding the Off mode)         |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__sysclk__wco__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__gr                 |                                  |
 | oup__sysclk__wco__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `High-Frequency               | Multiple high frequency clocks   |
 | Clocks <group_                   | (CLK_HF) are available in the    |
 | _group__sysclk__clk__hf.html>`__ | device                           |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group_     |                                  |
 | _sysclk__clk__hf__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group_             |                                  |
 | _sysclk__clk__hf__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Fast                         | The fast clock drives the "fast" |
 | Clock <group__g                  | processor (e.g                   |
 | roup__sysclk__clk__fast.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__s   |                                  |
 | ysclk__clk__fast__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Peripheral                   | The peripheral clock is a        |
 | Clock <group__g                  | divided clock of CLK_HF0 (`HF    |
 | roup__sysclk__clk__peri.html>`__ | Clocks <group__                  |
 |                                  | group__sysclk__clk__hf.html>`__) |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__s   |                                  |
 | ysclk__clk__peri__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Peripherals Clock            | There are multiple peripheral    |
 | Dividers <group__group__         | clock dividers that, in effect,  |
 | sysclk__clk__peripheral.html>`__ | create multiple separate         |
 |                                  | peripheral clocks                |
 +----------------------------------+----------------------------------+
 |  \ `Functions                    |                                  |
 |  <group__group__sysclk_          |                                  |
 | _clk__peripheral__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__sysclk_     |                                  |
 | _clk__peripheral__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Slow                         | The slow clock is the source     |
 | Clock <group__g                  | clock for the "slow" processor   |
 | roup__sysclk__clk__slow.html>`__ | (e.g                             |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__s   |                                  |
 | ysclk__clk__slow__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Alternative High-Frequency   | In the BLE-enabled PSoC6         |
 | Clock <group_                    | devices, the `BLE ECO (BLE ECO   |
 | _group__sysclk__alt__hf.html>`__ | Clock)                           |
 |                                  | <group__group__ble__clk.html>`__ |
 |                                  | clock is connected to the system |
 |                                  | Alternative High-Frequency Clock |
 |                                  | input                            |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group_     |                                  |
 | _sysclk__alt__hf__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low-Frequency                | The low-frequency clock is the   |
 | Clock <group_                    | source clock for the `MCWDT      |
 | _group__sysclk__clk__lf.html>`__ | (Multi-Counter                   |
 |                                  | Watchdo                          |
 |                                  | g) <group__group__mcwdt.html>`__ |
 |                                  | and can be the source clock for  |
 |                                  | `Backup Domain                   |
 |                                  | Clock <group__g                  |
 |                                  | roup__sysclk__clk__bak.html>`__, |
 |                                  | which drives the `RTC (Real-Time |
 |                                  | Cl                               |
 |                                  | ock) <group__group__rtc.html>`__ |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group_     |                                  |
 | _sysclk__clk__lf__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group_             |                                  |
 | _sysclk__clk__lf__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Timer                        | The timer clock can be a source  |
 | Clock <group__gr                 | for the alternative clock        |
 | oup__sysclk__clk__timer.html>`__ | driving the `SysTick (ARM System |
 |                                  | Timer) <group__gr                |
 |                                  | oup__arm__system__timer.html>`__ |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__sy  |                                  |
 | sclk__clk__timer__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__sy          |                                  |
 | sclk__clk__timer__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Pump                         | The pump clock is a clock source |
 | Clock <group__g                  | used to provide analog precision |
 | roup__sysclk__clk__pump.html>`__ | in low voltage applications      |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__s   |                                  |
 | ysclk__clk__pump__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__s           |                                  |
 | ysclk__clk__pump__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Backup Domain                | The backup domain clock drives   |
 | Clock <group__                   | the `RTC (Real-Time              |
 | group__sysclk__clk__bak.html>`__ | Cl                               |
 |                                  | ock) <group__group__rtc.html>`__ |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__    |                                  |
 | sysclk__clk__bak__funcs.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__            |                                  |
 | sysclk__clk__bak__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Medium Frequency Domain      | The Medium Frequency Domain      |
 | Clock <group__g                  | Clock is present only in         |
 | roup__sysclk__mf__funcs.html>`__ | SRSS_ver1_3                      |
 +----------------------------------+----------------------------------+
 |  \ `SysInt (System               | The SysInt driver provides an    |
 | Interrupt                        | API to configure the device      |
 | ) <group__group__sysint.html>`__ | peripheral interrupts            |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group                |                                  |
 | __group__sysint__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Global                       |                                  |
 | variables <group_                |                                  |
 | _group__sysint__globals.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__g          |                                  |
 | roup__sysint__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__s      |                                  |
 | ysint__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <grou                      |                                  |
 | p__group__sysint__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SysLib (System               | The system libraries provide     |
 | Library                          | APIs that can be called in the   |
 | ) <group__group__syslib.html>`__ | user application to handle the   |
 |                                  | timing, logical checking or      |
 |                                  | register                         |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group                |                                  |
 | __group__syslib__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Status                       | Function status type codes       |
 | codes <group__group__syslib      |                                  |
 | __macros__status__codes.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Assert Classes and           | Defines for the Assert Classes   |
 | Levels <group__group_            | and Levels                       |
 | _syslib__macros__assert.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Reset                        | Define RESET_CAUSE mask values   |
 | cause <group__group__sysli       |                                  |
 | b__macros__reset__cause.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Unique                       | Unique ID fields positions       |
 | ID <group__group__sys            |                                  |
 | lib__macros__unique__id.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__g          |                                  |
 | roup__syslib__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__s      |                                  |
 | yslib__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__sy          |                                  |
 | slib__enumerated__types.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SysPm (System Power          | Use the System Power Management  |
 | Managemen                        | (SysPm) driver to change power   |
 | t) <group__group__syspm.html>`__ | modes and reduce system power    |
 |                                  | consumption in power sensitive   |
 |                                  | designs                          |
 +----------------------------------+----------------------------------+
 |  \ `Macros <grou                 |                                  |
 | p__group__syspm__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `The Power Mode Status        | Defines for the CPU and system   |
 | Defines <group__group            | power modes status               |
 | __syspm__return__status.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Defines to skip the          | Defines for the SysPm callbacks  |
 | callbacks                        | modes that can be skipped during |
 | modes <group__group__syspm       | execution                        |
 | __skip__callback__modes.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__           |                                  |
 | group__syspm__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `General <group__group__sy    |                                  |
 | spm__functions__general.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Power                        |                                  |
 | Modes <group__group__            |                                  |
 | syspm__functions__power.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Power                        |                                  |
 | Status <group__group__syspm__f   |                                  |
 | unctions__power__status.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `I/Os                         |                                  |
 | Freeze <group__group__sys        |                                  |
 | pm__functions__iofreeze.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Core Voltage                 |                                  |
 | Regul                            |                                  |
 | ation <group__group__syspm__func |                                  |
 | tions__core__regulators.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `LDO <group__group            |                                  |
 | __syspm__functions__ldo.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Buck <group__group_          |                                  |
 | _syspm__functions__buck.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PMIC <group__group_          |                                  |
 | _syspm__functions__pmic.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Backup                       |                                  |
 | Domain <group__group__s          |                                  |
 | yspm__functions__backup.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    |                                  |
 | Callbacks <group__group__sys     |                                  |
 | pm__functions__callback.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__       |                                  |
 | syspm__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group__            |                                  |
 | syspm__data__enumerates.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `SysTick (ARM System          | Provides vendor-specific SysTick |
 | Timer) <group__gr                | API                              |
 | oup__arm__system__timer.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group_               |                                  |
 | _group__systick__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__systick__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__sy     |                                  |
 | stick__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `TCPWM (Timer Counter         | The TCPWM driver is a            |
 | PW                               | multifunction driver that        |
 | M) <group__group__tcpwm.html>`__ | implements Timer Counter, PWM,   |
 |                                  | and Quadrature Decoder           |
 |                                  | functionality using the TCPWM    |
 |                                  | block                            |
 +----------------------------------+----------------------------------+
 |  \ `Common <grou                 | Common API for the Timer Counter |
 | p__group__tcpwm__common.html>`__ | PWM Block                        |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__group         |                                  |
 | __tcpwm__macros__common.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `TCPWM Input                  | Selects which input to use       |
 | Selection <group__group__        |                                  |
 | tcpwm__input__selection.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Input                        | Configures how TCPWM inputs      |
 | Modes <group__gro                | behave                           |
 | up__tcpwm__input__modes.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Output Trigger               | Configures how TCPWM output      |
 | Modes <group__group__tcpwm_      | triggers behave (TCPWM_v2 only)  |
 | _output__trigger__modes.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    | Interrupt Sources                |
 | Sources <group__group__tc        |                                  |
 | pwm__interrupt__sources.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Default registers            | Default constants for CNT        |
 | constants <group__g              | Registers                        |
 | roup__tcpwm__reg__const.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__t   |                                  |
 | cpwm__functions__common.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | St                               |                                  |
 | ructures <group__group__tcpwm__d |                                  |
 | ata__structures__common.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <gro                       |                                  |
 | up__group__tcpwm__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Timer/Counter                | Driver API for Timer/Counter     |
 | (TCPWM) <group                   |                                  |
 | __group__tcpwm__counter.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__group_        |                                  |
 | _tcpwm__macros__counter.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Counter Run                  | Run modes for the counter timer  |
 | Modes <group__group__tcp         |                                  |
 | wm__counter__run__modes.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Counter                      | The counter directions           |
 | Direction <group__group__tc      |                                  |
 | pwm__counter__direction.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Counter CLK                  | The clock prescaler values       |
 | Pr                               |                                  |
 | escalers <group__group__tcpwm__c |                                  |
 | ounter__clk__prescalers.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Counter Compare              | A compare or capture mode        |
 | Capture <group__group__tcpwm__co |                                  |
 | unter__compare__capture.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Counter                      | The counter status               |
 | Status <group__group_            |                                  |
 | _tcpwm__counter__status.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__tc  |                                  |
 | pwm__functions__counter.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Str                              |                                  |
 | uctures <group__group__tcpwm__da |                                  |
 | ta__structures__counter.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PWM                          | Driver API for PWM               |
 | (TCPWM) <g                       |                                  |
 | roup__group__tcpwm__pwm.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__gr            |                                  |
 | oup__tcpwm__macros__pwm.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PWM run                      | Run modes for the pwm timer      |
 | modes <group__group_             |                                  |
 | _tcpwm__pwm__run__modes.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PWM                          | Sets the PWM modes               |
 | modes <group__g                  |                                  |
 | roup__tcpwm__pwm__modes.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PWM                          | Sets the alignment of the PWM    |
 | Alignment <group__group          |                                  |
 | __tcpwm__pwm__alignment.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PWM kill                     | Sets the kill mode for the PWM   |
 | modes <group__group__            |                                  |
 | tcpwm__pwm__kill__modes.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PWM Disabled                 | Specifies the behavior of the    |
 | Output <group__group__tcpwm__p   | PWM outputs while PWM is         |
 | wm__output__on__disable.html>`__ | disabled                         |
 +----------------------------------+----------------------------------+
 |  \ `PWM Output                   | Specifies the PWM output line    |
 | Lines <group__group__t           |                                  |
 | cpwm__pwm__output__line.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PWM CLK Prescaler            | Clock prescaler values           |
 | values <group__group__tcpw       |                                  |
 | m__pwm__clk__prescalers.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PWM output                   | Output invert modes              |
 | invert <group__group__tcp        |                                  |
 | wm__pwm__output__invert.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `PWM                          | The counter status               |
 | Status <group__gr                |                                  |
 | oup__tcpwm__pwm__status.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group      |                                  |
 | __tcpwm__functions__pwm.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Structures <group__group__tcpwm  |                                  |
 | __data__structures__pwm.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Quadrature Decoder           | Driver API for Quadrature        |
 | (TCPWM) <group                   | Decoder                          |
 | __group__tcpwm__quaddec.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__group_        |                                  |
 | _tcpwm__macros__quaddec.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `QuadDec                      | The quadrature decoder mode      |
 | Mode <group__grou                |                                  |
 | p__tcpwm__quaddec__mode.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `QuadDec                      | The quadrature decoder           |
 | Resolution <group__group__tcp    | resolution                       |
 | wm__quaddec__resolution.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `QuadDec                      | The quadrature capture modes     |
 | C                                |                                  |
 | aptureMode <group__group__tcpwm_ |                                  |
 | _quaddec__capture__mode.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `QuadDec                      | The counter status               |
 | Status <group__group_            |                                  |
 | _tcpwm__quaddec__status.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__tc  |                                  |
 | pwm__functions__quaddec.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Str                              |                                  |
 | uctures <group__group__tcpwm__da |                                  |
 | ta__structures__quaddec.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Shift Register               | Driver API for Shift Register    |
 | (TCPWM) <group_                  |                                  |
 | _group__tcpwm__shiftreg.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__group__       |                                  |
 | tcpwm__macros__shiftreg.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Shift Register CLK Prescaler | Clock prescaler values           |
 | values <group__group__tcpwm__sh  |                                  |
 | iftreg__clk__prescalers.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Shift Register Output        | Specifies the Shift Register     |
 | Lines <group__group__tcpwm_      | output line                      |
 | _shiftreg__output__line.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Shift Register output        | Output invert modes              |
 | invert <group__group__tcpwm__s   |                                  |
 | hiftreg__output__invert.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Shift Register Disabled      | Specifies the behavior of the    |
 | Out                              | Shift Register outputs while     |
 | put <group__group__tcpwm__shiftr | Shift Register is disabled       |
 | eg__output__on__disable.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Shift Register               | The Shift Register status        |
 | Status <group__group__           |                                  |
 | tcpwm__shiftreg__status.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__group__tcp |                                  |
 | wm__functions__shiftreg.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Stru                             |                                  |
 | ctures <group__group__tcpwm__dat |                                  |
 | a__structures__shiftreg.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `TrigMux (Trigger             | The trigger multiplexer provides |
 | Multiplexer)                     | access to the multiplexer that   |
 | <group__group__trigmux.html>`__  | selects a set of trigger output  |
 |                                  | signals from different           |
 |                                  | peripheral blocks to route them  |
 |                                  | to the specific trigger input of |
 |                                  | another peripheral block         |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group_               |                                  |
 | _group__trigmux__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group__gr         |                                  |
 | oup__trigmux__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group                     |                                  |
 | __group__trigmux__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Reduction Trigger            |                                  |
 | Mutiplexers <group__gro          |                                  |
 | up__trigmux__red__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Reduction Trigger Mutiplexer |                                  |
 | Inputs <group__group__           |                                  |
 | trigmux__red__in__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Reduction Trigger Mutiplexer |                                  |
 | Outputs <group__group__t         |                                  |
 | rigmux__red__out__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Distribution Trigger         |                                  |
 | Mutiplexers <group__gro          |                                  |
 | up__trigmux__dst__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Distribution Trigger         |                                  |
 | Mutiplexer                       |                                  |
 | Inputs <group__group__           |                                  |
 | trigmux__dst__in__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Distribution Trigger         |                                  |
 | Mutiplexer                       |                                  |
 | Outputs <group__group__t         |                                  |
 | rigmux__dst__out__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Trigger Mutiplexer           |                                  |
 | Inputs <group__gr                |                                  |
 | oup__trigmux__in__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Trigger Mutiplexer           |                                  |
 | Outputs <group__gro              |                                  |
 | up__trigmux__out__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `One-To-One Trigger           |                                  |
 | Lines <group__grou               |                                  |
 | p__trigmux__1to1__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `USBFS (USB Full-Speed        | The USBFS driver provides an API |
 | Device) <group_                  | to interact with a               |
 | _group__usbfs__dev__drv.html>`__ | fixed-function USB block         |
 +----------------------------------+----------------------------------+
 |  \ `Macros <group__group__       |                                  |
 | usbfs__dev__drv__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    |                                  |
 | Le                               |                                  |
 | vel <group__group__usbfs__dev__d |                                  |
 | rv__macros__intr__level.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    |                                  |
 | Ca                               |                                  |
 | use <group__group__usbfs__dev__d |                                  |
 | rv__macros__intr__cause.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Transfer                     |                                  |
 | Error                            |                                  |
 | s <group__group__usbfs__dev__drv |                                  |
 | __macros__ep__xfer__err.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |                                  |                                  |
 | \ `Functions <group__group__usb  |                                  |
 | fs__dev__drv__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Initialization               | The Initialization functions     |
 | Func                             | provide an API to begin the      |
 | tions <group__group__usbfs__dev_ | USBFS driver operation           |
 | _hal__functions__common.html>`__ | (configure and enable) and to    |
 |                                  | stop operation (disable and      |
 |                                  | de-initialize)                   |
 +----------------------------------+----------------------------------+
 |  \ `Interrupt                    | The Functions Interrupt          |
 | Function                         | functions provide an API to      |
 | s <group__group__usbfs__dev__drv | register callbacks for interrupt |
 | __functions__interrupts.html>`__ | events provided by the USB       |
 |                                  | block, interrupt handler, and    |
 |                                  | configuration functions          |
 +----------------------------------+----------------------------------+
 |  \ `Endpoint 0 Service           | The Endpoint 0 Service functions |
 | Functions                        | provide an API to establish      |
 | <group__group__usbfs__dev__hal__ | communication with the USB Host  |
 | functions__ep0__service.html>`__ | using control endpoint 0         |
 +----------------------------------+----------------------------------+
 |  \ `Data Endpoint Configuration  | The Data Endpoint Configuration  |
 | Functions <gro                   | Functions provide an API to      |
 | up__group__usbfs__dev__hal__func | allocate and release hardware    |
 | tions__endpoint__config.html>`__ | resources and override the       |
 |                                  | memcpy function for the data     |
 |                                  | endpoints                        |
 +----------------------------------+----------------------------------+
 |  \ `Data Endpoint Transfer       | The Data Endpoint Transfer       |
 | Function                         | functions provide an API to      |
 | s <group__group__usbfs__dev__hal | establish communication with the |
 | __functions__data__xfer.html>`__ | USB Host using data endpoint     |
 +----------------------------------+----------------------------------+
 |  \ `Low Power                    | The Low-power functions provide  |
 | Function                         | an API to implement Low-power    |
 | s <group__group__usbfs__dev__drv | callback at the application      |
 | __functions__low__power.html>`__ | level                            |
 +----------------------------------+----------------------------------+
 |  \ `LPM (Link Power Management)  | The LPM functions provide an API |
 | F                                | to use the LPM feature available |
 | unctions <group__group__usbfs__d | in the USB block                 |
 | ev__drv__functions__lpm.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Data                         |                                  |
 | Stru                             |                                  |
 | ctures <group__group__usbfs__dev |                                  |
 | __drv__data__structures.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Enumerated                   |                                  |
 | Types <group__group_             |                                  |
 | _usbfs__dev__drv__enums.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `WDT (Watchdog                | The Watchdog timer (WDT) has a   |
 | Ti                               | 16-bit free-running up-counter   |
 | mer) <group__group__wdt.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Macros <gr                   |                                  |
 | oup__group__wdt__macros.html>`__ |                                  |
 +----------------------------------+----------------------------------+
 |  \ `Functions <group             |                                  |
 | __group__wdt__functions.html>`__ |                                  |
 +----------------------------------+----------------------------------+
		




		
		

   
	


