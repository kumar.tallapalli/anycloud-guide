=======================
Interrupt trigger type
=======================

.. doxygengroup:: group_gpio_interruptTrigger
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: