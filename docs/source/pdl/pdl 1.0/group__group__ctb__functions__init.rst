=========================
Initialization Functions
=========================



.. doxygengroup:: group_ctb_functions_init
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: