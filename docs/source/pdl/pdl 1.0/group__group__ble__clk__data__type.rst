==================================================
Enumerated Types
==================================================

.. doxygengroup:: group_ble_clk_data_type
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: