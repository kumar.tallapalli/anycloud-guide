========================
Common Data Structures
========================


.. doxygengroup:: group_crypto_data_structures
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: