=================
Enumerated Types
=================

.. doxygengroup:: group_smif_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: