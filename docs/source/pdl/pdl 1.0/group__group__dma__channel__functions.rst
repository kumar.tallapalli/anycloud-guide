===================
Channel Functions
===================


.. doxygengroup:: group_dma_channel_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: