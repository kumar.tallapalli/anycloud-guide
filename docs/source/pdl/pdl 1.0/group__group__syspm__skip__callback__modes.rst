====================================
Defines to skip the callbacks modes
====================================

.. doxygengroup:: group_syspm_skip_callback_modes
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: