====================================
Shift Register CLK Prescaler values
====================================

.. doxygengroup:: group_tcpwm_shiftreg_clk_prescalers
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   