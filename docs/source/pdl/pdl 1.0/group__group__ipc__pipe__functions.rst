==========
Functions
==========

.. doxygengroup:: group_ipc_pipe_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: