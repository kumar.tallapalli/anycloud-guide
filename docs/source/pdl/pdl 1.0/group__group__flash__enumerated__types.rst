================
Enumerated Types
================

.. doxygengroup:: group_flash_enumerated_types
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: