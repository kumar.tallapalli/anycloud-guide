=================
Cortex-M4 Status
=================

.. doxygengroup:: group_system_config_cm4_status_macro
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   