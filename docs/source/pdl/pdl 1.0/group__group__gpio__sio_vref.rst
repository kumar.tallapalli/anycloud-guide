=================================================
SIO reference voltage for input buffer trip-point
=================================================

.. doxygengroup:: group_gpio_sioVref
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: