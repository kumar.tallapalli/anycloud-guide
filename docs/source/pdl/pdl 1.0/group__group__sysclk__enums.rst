=========================
General Enumerated Types
=========================

.. doxygengroup:: group_sysclk_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
==============

.. toctree::

   group__group__sysclk__returns.rst