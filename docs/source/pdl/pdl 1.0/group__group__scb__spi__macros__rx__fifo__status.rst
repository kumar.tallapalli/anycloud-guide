=====================
SPI RX FIFO Statuses
=====================


.. doxygengroup:: group_scb_spi_macros_rx_fifo_status 
   :project: pdl1.0 
   :members:
