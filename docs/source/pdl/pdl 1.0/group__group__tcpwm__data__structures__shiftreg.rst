===============
Data Structures
===============

.. doxygengroup:: group_tcpwm_data_structures_shiftreg
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: