==============================
Reduction Trigger Mutiplexers
==============================

.. toctree::
   
   group__group__trigmux__red__in__enums.rst
   group__group__trigmux__red__out__enums.rst

.. doxygengroup:: group_trigmux_red_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: