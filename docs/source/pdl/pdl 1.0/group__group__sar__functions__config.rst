=================================
Run-time Configuration Functions
=================================



.. doxygengroup:: group_sar_functions_config
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: