=================================
Alternative High-Frequency Clock
=================================

.. doxygengroup:: group_sysclk_alt_hf
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
==============

.. toctree::

   group__group__sysclk__alt__hf__funcs.rst