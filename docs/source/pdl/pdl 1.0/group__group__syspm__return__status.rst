==============================
The Power Mode Status Defines
==============================

.. doxygengroup:: group_syspm_return_status
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: