=======
Macros
=======

.. doxygengroup:: group_tcpwm_macros_pwm
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__tcpwm__pwm__run__modes.rst
   group__group__tcpwm__pwm__modes.rst
   group__group__tcpwm__pwm__alignment.rst
   group__group__tcpwm__pwm__kill__modes.rst
   group__group__tcpwm__pwm__output__on__disable.rst
   group__group__tcpwm__pwm__output__line.rst
   group__group__tcpwm__pwm__clk__prescalers.rst
   group__group__tcpwm__pwm__output__invert.rst
   group__group__tcpwm__pwm__status.rst