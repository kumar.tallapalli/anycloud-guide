========================
Flash general parameters
========================

.. doxygengroup:: group_flash_general_macros
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: