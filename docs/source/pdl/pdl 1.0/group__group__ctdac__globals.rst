=================
Global Variables
=================

.. doxygengroup:: group_ctdac_globals
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: