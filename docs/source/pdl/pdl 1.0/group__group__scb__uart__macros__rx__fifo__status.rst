====================
UART RX FIFO status
====================

.. doxygengroup:: group_scb_uart_macros_rx_fifo_status 
   :project: pdl1.0
   :members:
