<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__i2s__functions__syspm__callback" kind="group">
    <compoundname>group_i2s_functions_syspm_callback</compoundname>
    <title>Low Power Callback</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__i2s__functions__syspm__callback_1ga42567129d1e578c5c554100c74bc0ace" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_I2S_DeepSleepCallback</definition>
        <argsstring>(cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)</argsstring>
        <name>Cy_I2S_DeepSleepCallback</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref> *</type>
          <declname>callbackParams</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>This is a callback function to be used at the application layer to manage an I2S operation during the Deep Sleep cycle. </para>        </briefdescription>
        <detaileddescription>
<para>It stores the I2S state (Tx/Rx enabled/disabled/paused) into the context structure and stops the communication before entering into Deep Sleep power mode and restores the I2S state after waking up.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>callbackParams</parametername>
</parameternamelist>
<parameterdescription>
<para>- The pointer to the callback parameters structure, see <ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Callback mode, see <ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>the SysPm callback status <ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Use the &lt;a href="structcy__stc__i2s__context__t.html#"&gt;cy_stc_i2s_context_t&lt;/a&gt; data type for definition of the *context element of the &lt;a href="structcy__stc__syspm__callback__params__t.html#"&gt;cy_stc_syspm_callback_params_t&lt;/a&gt; structure.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />I2S<sp />hardware<sp />must<sp />be<sp />suitably<sp />prepared<sp />to<sp />enter<sp />device<sp />Deep<sp />Sleep<sp />Mode.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />It<sp />must<sp />stop<sp />all<sp />communication<sp />before<sp />Deep<sp />Sleep<sp />Mode<sp />entry<sp />and<sp />continue<sp />communication<sp />upon<sp />Deep<sp />Sleep<sp />Mode<sp />exit.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Context<sp />structure<sp />for<sp />Cy_I2S_DeepSleepCallback<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__i2s__context__t">cy_stc_i2s_context_t</ref><sp />i2sDeepSleepCallbackContext;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Parameters<sp />structure<sp />for<sp />Cy_I2S_DeepSleepCallback<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref><sp />i2sDeepSleepCallbackParams<sp />=<sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">*)<sp />I2S0,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">*)<sp />&amp;i2sDeepSleepCallbackContext</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialization<sp />structure<sp />for<sp />Cy_I2S_DeepSleepCallback<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__syspm__callback__t">cy_stc_syspm_callback_t</ref><sp />i2sDeepSleepCallback<sp />=<sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />&amp;<ref kindref="member" refid="group__group__i2s__functions__syspm__callback_1ga42567129d1e578c5c554100c74bc0ace">Cy_I2S_DeepSleepCallback</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0abc51d74deff0ceea4304b01b2d57bd9d">CY_SYSPM_DEEPSLEEP</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Skip<sp />execution<sp />for<sp />CHECK_READY<sp />and<sp />CHECK_FAIL<sp />events<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__skip__callback__modes_1ga7080a9d98d07b0f7b44eda4899c1d26f">CY_SYSPM_SKIP_CHECK_READY</ref><sp />|<sp /><ref kindref="member" refid="group__group__syspm__skip__callback__modes_1gac7d1c97ade02b87e789c76b6a58f8c0f">CY_SYSPM_SKIP_CHECK_FAIL</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />&amp;i2sDeepSleepCallbackParams,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />NULL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />NULL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />0</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Register<sp />the<sp />Cy_I2S_DeepSleepCallback<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__syspm__data__enumerates_1gga601b1cb722cb091133caf33d8ab235caaf00d22f1ee891a8a56f8bbf58132e775">CY_SYSPM_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>(&amp;i2sDeepSleepCallback))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enter<sp />chip<sp />Deep<sp />Sleep<sp />Mode<sp />using<sp />the<sp />SysPm<sp />driver<sp />API<sp />*/</highlight><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="297" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_i2s.c" bodystart="241" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_i2s.h" line="516" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>The driver supports SysPm callback for Deep Sleep transition. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>