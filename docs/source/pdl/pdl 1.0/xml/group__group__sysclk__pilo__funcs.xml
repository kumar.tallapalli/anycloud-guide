<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sysclk__pilo__funcs" kind="group">
    <compoundname>group_sysclk_pilo_funcs</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sysclk__pilo__funcs_1ga4a6570666d4462b97d86e05f4e9fd492" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SysClk_PiloEnable</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_PiloEnable</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Enables the PILO. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function blocks for 1 millisecond between enabling the PILO and releasing the PILO reset.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />PILO<sp />needs<sp />to<sp />source<sp />the<sp />LFCLK.<sp /><sp />All<sp />peripherals<sp />clocked<sp />by</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />the<sp />LFCLK<sp />are<sp />disabled<sp />and<sp />the<sp />Watchdog<sp />timer<sp />(WDT)<sp />is<sp />unlocked.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />PILO.<sp />Note:<sp />Blocks<sp />for<sp />1<sp />millisecond.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga4a6570666d4462b97d86e05f4e9fd492">Cy_SysClk_PiloEnable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />LFCLK<sp />source<sp />to<sp />the<sp />PILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__lf__funcs_1ga4d8aaf9e391eafb90aa8897d95525eb4">Cy_SysClk_ClkLfSetSource</ref>(<ref kindref="member" refid="group__group__sysclk__clk__lf__enums_1gga8bfd6f85870f0d30d15e73b3ed820db6a51a3252aefb2b8198451b0d1589e47bf">CY_SYSCLK_CLKLF_IN_PILO</ref>);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1387" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="1376" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1357" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__pilo__funcs_1ga5884a1be24c8d6680c80f4f857d31bb2" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE bool</type>
        <definition>__STATIC_INLINE bool Cy_SysClk_PiloIsEnabled</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_PiloIsEnabled</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Reports the Enabled/Disabled status of the PILO. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>Boolean status of PILO: true - Enabled, false - Disabled.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />LFCLK<sp />needs<sp />to<sp />be<sp />sourced<sp />by<sp />the<sp />ILO<sp />instead<sp />of<sp />the<sp />PILO.<sp />All</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />peripherals<sp />clocked<sp />by<sp />the<sp />LFCLK<sp />are<sp />disabled<sp />and<sp />the<sp />Watchdog</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />timer<sp />(WDT)<sp />is<sp />unlocked.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga5884a1be24c8d6680c80f4f857d31bb2">Cy_SysClk_PiloIsEnabled</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Disable<sp />the<sp />PILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga954f6432534797a063e1485f50d6db79">Cy_SysClk_PiloDisable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__ilo__funcs_1gaabb2612adc103562da7d6dc451a33caa">Cy_SysClk_IloEnable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />LFCLK<sp />source<sp />to<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__lf__funcs_1ga4d8aaf9e391eafb90aa8897d95525eb4">Cy_SysClk_ClkLfSetSource</ref>(<ref kindref="member" refid="group__group__sysclk__clk__lf__enums_1gga8bfd6f85870f0d30d15e73b3ed820db6a724311d4501aedcbd0699a3302aabaf9">CY_SYSCLK_CLKLF_IN_ILO</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1405" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="1402" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1358" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__pilo__funcs_1ga954f6432534797a063e1485f50d6db79" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SysClk_PiloDisable</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_PiloDisable</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Disables the PILO. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />LFCLK<sp />needs<sp />to<sp />be<sp />sourced<sp />by<sp />the<sp />ILO<sp />instead<sp />of<sp />the<sp />PILO.<sp />All</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />peripherals<sp />clocked<sp />by<sp />the<sp />LFCLK<sp />are<sp />disabled<sp />and<sp />the<sp />Watchdog</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />timer<sp />(WDT)<sp />is<sp />unlocked.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga5884a1be24c8d6680c80f4f857d31bb2">Cy_SysClk_PiloIsEnabled</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Disable<sp />the<sp />PILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga954f6432534797a063e1485f50d6db79">Cy_SysClk_PiloDisable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__ilo__funcs_1gaabb2612adc103562da7d6dc451a33caa">Cy_SysClk_IloEnable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />LFCLK<sp />source<sp />to<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__lf__funcs_1ga4d8aaf9e391eafb90aa8897d95525eb4">Cy_SysClk_ClkLfSetSource</ref>(<ref kindref="member" refid="group__group__sysclk__clk__lf__enums_1gga8bfd6f85870f0d30d15e73b3ed820db6a724311d4501aedcbd0699a3302aabaf9">CY_SYSCLK_CLKLF_IN_ILO</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1429" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="1418" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1359" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__pilo__funcs_1ga3ad33424ef17a1ee78ce67e12273d763" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SysClk_PiloSetTrim</definition>
        <argsstring>(uint32_t trimVal)</argsstring>
        <name>Cy_SysClk_PiloSetTrim</name>
        <param>
          <type>uint32_t</type>
          <declname>trimVal</declname>
        </param>
        <briefdescription>
<para>Sets the PILO trim bits, which adjusts the PILO frequency. </para>        </briefdescription>
        <detaileddescription>
<para>This is typically done after measuring the PILO frequency; see <ref kindref="member" refid="group__group__sysclk__calclk__funcs_1ga0a87e123411d5711344780ddfa492f37">Cy_SysClk_StartClkMeasurementCounters()</ref>.</para><para><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />PILO<sp />needs<sp />to<sp />be<sp />trimmed<sp />to<sp />+/-<sp />2%<sp />of<sp />nominal<sp />32.768<sp />kHz<sp />using</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />the<sp />ECO<sp />as<sp />the<sp />source.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />ECO_FREQ<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />24000000UL<sp /><sp /></highlight><highlight class="comment">/*<sp />24<sp />MHz<sp />ECO<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />PILO_NOMINAL<sp /><sp /><sp /><sp />32768UL<sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />32.768<sp />kHz<sp />PILO<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />PILO_ACCURACY<sp /><sp /><sp />655UL<sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />PILO_NOMINAL<sp />*<sp />0.02<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />trimStatus<sp />=<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">while</highlight><highlight class="normal">(!trimStatus)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Start<sp />the<sp />PILO<sp />clock<sp />measurement<sp />using<sp />the<sp />IMO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__sysclk__calclk__funcs_1ga0a87e123411d5711344780ddfa492f37">Cy_SysClk_StartClkMeasurementCounters</ref>(CY_SYSCLK_MEAS_CLK_PILO,<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Counter<sp />1<sp />clock<sp /><sp /><sp /><sp /><sp /><sp />=<sp />PILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />0x3FFUL,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Counter<sp />1<sp />init<sp />value<sp />=<sp />1024<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY_SYSCLK_MEAS_CLK_ECO);<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Counter<sp />2<sp />clock<sp /><sp /><sp /><sp /><sp /><sp />=<sp />ECO<sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Wait<sp />for<sp />counter<sp />1<sp />to<sp />reach<sp />0<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">while</highlight><highlight class="normal"><sp />(!<ref kindref="member" refid="group__group__sysclk__calclk__funcs_1ga60a2cef8db1753b5fbc12e1067a71c25">Cy_SysClk_ClkMeasurementCountersDone</ref>()){};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Measure<sp />clock<sp />1<sp />with<sp />the<sp />ECO<sp />clock<sp />cycles<sp />(counter<sp />2)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp />measuredFreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__calclk__funcs_1ga979f2ef437908618291bf3a935488412">Cy_SysClk_ClkMeasurementCountersGetFreq</ref>(</highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />ECO_FREQ);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Increment/decrement<sp />the<sp />trim<sp />depending<sp />on<sp />the<sp />returned<sp />result<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />((PILO_NOMINAL<sp />+<sp />PILO_ACCURACY)<sp />&lt;<sp />measuredFreq)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga3ad33424ef17a1ee78ce67e12273d763">Cy_SysClk_PiloSetTrim</ref>(<ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga5847234c409635c28170eb61eb5ae20a">Cy_SysClk_PiloGetTrim</ref>()<sp />-<sp />1UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal"><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(measuredFreq<sp />&lt;<sp />(PILO_NOMINAL<sp />-<sp />PILO_ACCURACY))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga3ad33424ef17a1ee78ce67e12273d763">Cy_SysClk_PiloSetTrim</ref>(<ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga5847234c409635c28170eb61eb5ae20a">Cy_SysClk_PiloGetTrim</ref>()<sp />+<sp />1UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Trimmed<sp />within<sp />limits<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />trimStatus<sp />=<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1450" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="1443" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1360" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__pilo__funcs_1ga5847234c409635c28170eb61eb5ae20a" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_SysClk_PiloGetTrim</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_PiloGetTrim</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Reports the current PILO trim bits value. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />PILO<sp />needs<sp />to<sp />be<sp />trimmed<sp />to<sp />+/-<sp />2%<sp />of<sp />nominal<sp />32.768<sp />kHz<sp />using</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />the<sp />ECO<sp />as<sp />the<sp />source.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />ECO_FREQ<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />24000000UL<sp /><sp /></highlight><highlight class="comment">/*<sp />24<sp />MHz<sp />ECO<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />PILO_NOMINAL<sp /><sp /><sp /><sp />32768UL<sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />32.768<sp />kHz<sp />PILO<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />PILO_ACCURACY<sp /><sp /><sp />655UL<sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />PILO_NOMINAL<sp />*<sp />0.02<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />trimStatus<sp />=<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">while</highlight><highlight class="normal">(!trimStatus)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Start<sp />the<sp />PILO<sp />clock<sp />measurement<sp />using<sp />the<sp />IMO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__sysclk__calclk__funcs_1ga0a87e123411d5711344780ddfa492f37">Cy_SysClk_StartClkMeasurementCounters</ref>(CY_SYSCLK_MEAS_CLK_PILO,<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Counter<sp />1<sp />clock<sp /><sp /><sp /><sp /><sp /><sp />=<sp />PILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />0x3FFUL,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Counter<sp />1<sp />init<sp />value<sp />=<sp />1024<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />CY_SYSCLK_MEAS_CLK_ECO);<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Counter<sp />2<sp />clock<sp /><sp /><sp /><sp /><sp /><sp />=<sp />ECO<sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Wait<sp />for<sp />counter<sp />1<sp />to<sp />reach<sp />0<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">while</highlight><highlight class="normal"><sp />(!<ref kindref="member" refid="group__group__sysclk__calclk__funcs_1ga60a2cef8db1753b5fbc12e1067a71c25">Cy_SysClk_ClkMeasurementCountersDone</ref>()){};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Measure<sp />clock<sp />1<sp />with<sp />the<sp />ECO<sp />clock<sp />cycles<sp />(counter<sp />2)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp />measuredFreq<sp />=<sp /><ref kindref="member" refid="group__group__sysclk__calclk__funcs_1ga979f2ef437908618291bf3a935488412">Cy_SysClk_ClkMeasurementCountersGetFreq</ref>(</highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />ECO_FREQ);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Increment/decrement<sp />the<sp />trim<sp />depending<sp />on<sp />the<sp />returned<sp />result<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />((PILO_NOMINAL<sp />+<sp />PILO_ACCURACY)<sp />&lt;<sp />measuredFreq)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga3ad33424ef17a1ee78ce67e12273d763">Cy_SysClk_PiloSetTrim</ref>(<ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga5847234c409635c28170eb61eb5ae20a">Cy_SysClk_PiloGetTrim</ref>()<sp />-<sp />1UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal"><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(measuredFreq<sp />&lt;<sp />(PILO_NOMINAL<sp />-<sp />PILO_ACCURACY))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga3ad33424ef17a1ee78ce67e12273d763">Cy_SysClk_PiloSetTrim</ref>(<ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga5847234c409635c28170eb61eb5ae20a">Cy_SysClk_PiloGetTrim</ref>()<sp />+<sp />1UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Trimmed<sp />within<sp />limits<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />trimStatus<sp />=<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1466" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="1463" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1361" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>