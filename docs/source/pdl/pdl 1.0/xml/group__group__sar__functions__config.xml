<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sar__functions__config" kind="group">
    <compoundname>group_sar_functions_config</compoundname>
    <title>Run-time Configuration Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sar__functions__config_1gaabd68168933e0922c739ee8ebe3cddd6" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SAR_SetConvertMode</definition>
        <argsstring>(SAR_Type *base, cy_en_sar_sample_ctrl_trigger_mode_t mode)</argsstring>
        <name>Cy_SAR_SetConvertMode</name>
        <param>
          <type><ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1ga62ecb4e368dcea23cfbfc41bd0497fc6">cy_en_sar_sample_ctrl_trigger_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>Set the mode in which conversions are triggered. </para>        </briefdescription>
        <detaileddescription>
<para>This function does not start any conversions; it only configures the mode for subsequent conversions.</para><para>There are three modes:<itemizedlist>
<listitem><para>firmware only; hardware triggering is disabled</para></listitem><listitem><para>firmware and edge sensitive hardware triggering</para></listitem><listitem><para>firmware and level sensitive hardware triggering</para></listitem></itemizedlist>
</para><para>Note that firmware triggering is always enabled.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>A value of the enum <ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1ga62ecb4e368dcea23cfbfc41bd0497fc6">cy_en_sar_sample_ctrl_trigger_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Allow<sp />a<sp />rising<sp />edge<sp />on<sp />a<sp />hardware<sp />trigger<sp />signal<sp />to<sp />initiate</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />a<sp />single<sp />conversion.<sp />FW<sp />triggering<sp />is<sp />still<sp />supported.<sp />The<sp />trigger<sp />signal</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />can<sp />come<sp />from<sp />a<sp />device<sp />pin<sp />or<sp />an<sp />internal<sp />block.<sp />Make<sp />sure<sp />route<sp />the<sp />hardware<sp />signal</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />to<sp />the<sp />SAR<sp />trigger<sp />input.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__config_1gaabd68168933e0922c739ee8ebe3cddd6">Cy_SAR_SetConvertMode</ref>(SAR,<sp /><ref kindref="member" refid="group__group__sar__sample__ctrl__register__enums_1gga62ecb4e368dcea23cfbfc41bd0497fc6a7eed8528ba801da55b7727c170d361e1">CY_SAR_TRIGGER_MODE_FW_AND_HWEDGE</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="478" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="470" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1416" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__config_1ga4808c2e7e72e2648c0efbf3eb025366f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SAR_SetChanMask</definition>
        <argsstring>(SAR_Type *base, uint32_t enableMask)</argsstring>
        <name>Cy_SAR_SetChanMask</name>
        <param>
          <type><ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>enableMask</declname>
        </param>
        <briefdescription>
<para>Set the enable/disable mask for the channels. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>enableMask</parametername>
</parameternamelist>
<parameterdescription>
<para>Channel enable/disable mask. Each bit corresponds to a channel.<itemizedlist>
<listitem><para>0: the corresponding channel is disabled.</para></listitem><listitem><para>1: the corresponding channel is enabled; it will be included in the next scan.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Channels<sp />0<sp />and<sp />1<sp />have<sp />been<sp />configured<sp />during<sp />initialization.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Channel<sp />1<sp />only<sp />needs<sp />to<sp />be<sp />scanned<sp />periodically.<sp />Disable<sp />it<sp />and<sp />only<sp />scan<sp />channel<sp />0.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__config_1ga4808c2e7e72e2648c0efbf3eb025366f">Cy_SAR_SetChanMask</ref>(SAR,<sp />1UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1613" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" bodystart="1608" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1417" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__config_1gaff69993327b0aba19490c7ac9708138c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SAR_SetLowLimit</definition>
        <argsstring>(SAR_Type *base, uint32_t lowLimit)</argsstring>
        <name>Cy_SAR_SetLowLimit</name>
        <param>
          <type><ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>lowLimit</declname>
        </param>
        <briefdescription>
<para>Set the low threshold for range detection. </para>        </briefdescription>
        <detaileddescription>
<para>The values are interpreted as signed or unsigned according to the channel configuration. Range detection is done on the value stored in the result register. That is, after averaging, shifting sign extension, and left/right alignment.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>lowLimit</parametername>
</parameternamelist>
<parameterdescription>
<para>The low threshold for range detection</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Range<sp />detection<sp />interrupts<sp />have<sp />been<sp />enabled<sp />for<sp />all<sp />unsigned<sp />channels.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Adjust<sp />the<sp />the<sp />low<sp />limit<sp />of<sp />the<sp />range<sp />detection<sp />to<sp />0x300<sp />and<sp />the<sp />high<sp />limit<sp />to<sp />0xC00.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__config_1gaff69993327b0aba19490c7ac9708138c">Cy_SAR_SetLowLimit</ref>(SAR,<sp />0x300UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__config_1ga1010c3aa3528648ac3715481c0339e28">Cy_SAR_SetHighLimit</ref>(SAR,<sp />0xC00UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="768" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="763" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1418" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__config_1ga1010c3aa3528648ac3715481c0339e28" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SAR_SetHighLimit</definition>
        <argsstring>(SAR_Type *base, uint32_t highLimit)</argsstring>
        <name>Cy_SAR_SetHighLimit</name>
        <param>
          <type><ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>highLimit</declname>
        </param>
        <briefdescription>
<para>Set the high threshold for range detection. </para>        </briefdescription>
        <detaileddescription>
<para>The values are interpreted as signed or unsigned according to the channel configuration. Range detection is done on the value stored in the result register. That is, after averaging, shifting sign extension, and left/right alignment.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>highLimit</parametername>
</parameternamelist>
<parameterdescription>
<para>The high threshold for range detection</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Range<sp />detection<sp />interrupts<sp />have<sp />been<sp />enabled<sp />for<sp />all<sp />unsigned<sp />channels.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Adjust<sp />the<sp />the<sp />low<sp />limit<sp />of<sp />the<sp />range<sp />detection<sp />to<sp />0x300<sp />and<sp />the<sp />high<sp />limit<sp />to<sp />0xC00.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__config_1gaff69993327b0aba19490c7ac9708138c">Cy_SAR_SetLowLimit</ref>(SAR,<sp />0x300UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__config_1ga1010c3aa3528648ac3715481c0339e28">Cy_SAR_SetHighLimit</ref>(SAR,<sp />0xC00UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="798" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sar.c" bodystart="793" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1419" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sar__functions__config_1ga7fc7fe5bfdf2f43c218f839592f3afa8" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SAR_SetRangeCond</definition>
        <argsstring>(SAR_Type *base, cy_en_sar_range_detect_condition_t cond)</argsstring>
        <name>Cy_SAR_SetRangeCond</name>
        <param>
          <type><ref kindref="compound" refid="struct_s_a_r___type">SAR_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sar__range__thres__register__enums_1gaccf649d65a86e17939c3d20a684cd9cd">cy_en_sar_range_detect_condition_t</ref></type>
          <declname>cond</declname>
        </param>
        <briefdescription>
<para>Set the condition in which range detection interrupts are triggered. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>cond</parametername>
</parameternamelist>
<parameterdescription>
<para>A value of the enum <ref kindref="member" refid="group__group__sar__range__thres__register__enums_1gaccf649d65a86e17939c3d20a684cd9cd">cy_en_sar_range_detect_condition_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Range<sp />detection<sp />interrupts<sp />have<sp />been<sp />enabled<sp />for<sp />all<sp />unsigned<sp />channels.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Set<sp />the<sp />range<sp />condition<sp />to<sp />be<sp />outside;<sp />a<sp />range<sp />interrupt<sp />is<sp />detected</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />when<sp />(result<sp />&lt;<sp />lowLimit)<sp />||<sp />(highLimit<sp />&lt;=<sp />result).<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sar__functions__config_1ga7fc7fe5bfdf2f43c218f839592f3afa8">Cy_SAR_SetRangeCond</ref>(SAR,<sp /><ref kindref="member" refid="group__group__sar__range__thres__register__enums_1ggaccf649d65a86e17939c3d20a684cd9cda05ef1644deb92e634e163532a3b2179e">CY_SAR_RANGE_COND_OUTSIDE</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1640" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" bodystart="1635" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sar.h" line="1420" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This set of functions allows changes to the SAR configuration after initialization. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>