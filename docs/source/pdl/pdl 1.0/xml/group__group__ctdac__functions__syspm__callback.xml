<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ctdac__functions__syspm__callback" kind="group">
    <compoundname>group_ctdac_functions_syspm_callback</compoundname>
    <title>Low Power Callback</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__syspm__callback_1gae3b956a4896232dd0a2e635e178db621" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_CTDAC_DeepSleepCallback</definition>
        <argsstring>(cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)</argsstring>
        <name>Cy_CTDAC_DeepSleepCallback</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref> *</type>
          <declname>callbackParams</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>Callback to prepare the CTDAC before entering and after exiting Deep Sleep mode. </para>        </briefdescription>
        <detaileddescription>
<para>If deglitching is used, it is disabled before entering Deep Sleep to ensure the deglitch switches are closed. This is needed only if the CTDAC will be enabled in DeepSleep. Upon wakeup, deglitching will be re-enabled if it was previously used.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>callbackParams</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure of type <ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref>. The context pointer should point to <ref kindref="compound" refid="structcy__stc__ctdac__context__t">cy_stc_ctdac_context_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Callback mode, see <ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>See <ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />CTDAC<sp />has<sp />been<sp />configured<sp />to<sp />stay<sp />on<sp />in<sp />Deep<sp />Sleep<sp />mode.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Before<sp />putting<sp />the<sp />device<sp />into<sp />Deep<sp />Sleep<sp />mode,<sp />the<sp />CTDAC<sp />Deep<sp />Sleep</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />callback<sp />must<sp />be<sp />registered<sp />to<sp />ensure<sp />proper<sp />operation<sp />during<sp />and<sp />after<sp />Deep<sp />Sleep<sp />mode.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />base<sp />address<sp />pointer<sp />to<sp />the<sp />CTDAC<sp />hardware<sp />block.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">static</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref><sp />DeepSleepCallbackParams;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">static</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcy__stc__ctdac__context__t">cy_stc_ctdac_context_t</ref><sp />ctdacContext;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />DeepSleepCallbackParams.<ref kindref="member" refid="structcy__stc__syspm__callback__params__t_1a56076e99358e2b7c99681312108b09af">base</ref><sp />=<sp />CTDAC0;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />DeepSleepCallbackParams.<ref kindref="member" refid="structcy__stc__syspm__callback__params__t_1abac771f244791efe23d4964a40bbf8c8">context</ref><sp />=<sp />&amp;ctdacContext;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Attach<sp />the<sp />Cy_CTDAC_DeepSleepCallback<sp />function<sp />and<sp />set<sp />the<sp />callback<sp />parameters.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__syspm__callback__t">cy_stc_syspm_callback_t</ref><sp />DeepSleepCallbackStruct;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />DeepSleepCallbackStruct.<ref kindref="member" refid="structcy__stc__syspm__callback__t_1afc2210e66cc5a0513e42761df9a2021a">callback</ref><sp />=<sp />&amp;<ref kindref="member" refid="group__group__ctdac__functions__syspm__callback_1gae3b956a4896232dd0a2e635e178db621">Cy_CTDAC_DeepSleepCallback</ref>;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />DeepSleepCallbackStruct.<ref kindref="member" refid="structcy__stc__syspm__callback__t_1a566582fa98bed63f19108164d62e6c8f">type</ref><sp />=<sp /><ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0abc51d74deff0ceea4304b01b2d57bd9d">CY_SYSPM_DEEPSLEEP</ref>;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />DeepSleepCallbackStruct.<ref kindref="member" refid="structcy__stc__syspm__callback__t_1a497711725618616da0b168743702dc03">callbackParams</ref><sp />=<sp />&amp;DeepSleepCallbackParams;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Register<sp />the<sp />callback<sp />before<sp />entering<sp />Deep<sp />Sleep<sp />mode.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>(&amp;DeepSleepCallbackStruct);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Put<sp />the<sp />device<sp />into<sp />Deep<sp />Sleep<sp />mode<sp />and<sp />wait<sp />for<sp />an<sp />interrupt<sp />to<sp />wake<sp />up.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />Cy_SysPm_DeepSleep(<ref kindref="member" refid="group__group__syspm__data__enumerates_1ggaae6a9f528630a2d69bb70b3bced1f0acadf47e50b6700c08b6f9e7e70bb525541">CY_SYSPM_WAIT_FOR_INTERRUPT</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="741" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctdac.c" bodystart="714" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="722" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This driver supports one SysPm callback for Deep Sleep transition. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>