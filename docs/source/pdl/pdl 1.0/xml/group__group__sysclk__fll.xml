<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sysclk__fll" kind="group">
    <compoundname>group_sysclk_fll</compoundname>
    <title>Frequency Locked Loop (FLL)</title>
    <innergroup refid="group__group__sysclk__fll__funcs">Functions</innergroup>
    <innergroup refid="group__group__sysclk__fll__structs">Data Structures</innergroup>
    <innergroup refid="group__group__sysclk__fll__enums">Enumerated Types</innergroup>
    <briefdescription>
<para>The FLL is a clock generation circuit that can be used to produce a higher frequency clock from a reference clock. </para>    </briefdescription>
    <detaileddescription>
<para>The output clock exhibits some characteristics of the reference clock such as the accuracy of the source. However other attributes such as the clock phase are not preserved. The FLL is similar in purpose to a (Phase locked loop) PLL but they are not equivalent.</para><para><itemizedlist>
<listitem><para>They may have different frequency ranges.</para></listitem><listitem><para>The FLL starts up (locks) faster and consumes less current than the PLL.</para></listitem><listitem><para>The FLL accepts a source clock with lower frequency than PLL, such as the WCO (32 KHz).</para></listitem><listitem><para>The FLL does not lock phase. The hardware consist of a counter with a current-controlled oscillator (CCO). The counter counts the number of output clock edges in a reference clock period and adjusts the CCO until the expected ratio is achieved (locked). After initial lock, the CCO is adjusted dynamically to keep the ratio within tolerance. The lock tolerance is user-adjustable. <image name="sysclk_fll.png" type="html" />
</para></listitem></itemizedlist>
</para><para>The SysClk driver supports two models for configuring the FLL. The first model is to call the <ref kindref="member" refid="group__group__sysclk__fll__funcs_1gad9d9c36d022475746375bddaba2b2065">Cy_SysClk_FllConfigure()</ref> function, which calculates the necessary parameters for the FLL at run-time. This may be necessary for dynamic run-time changes to the FLL. However this method is slow as it needs to perform the calculation before configuring the FLL. The other model is to call <ref kindref="member" refid="group__group__sysclk__fll__funcs_1ga0e2e272b670cc52ab984291afae6a1fa">Cy_SysClk_FllManualConfigure()</ref> function with pre-calculated parameter values. This method is faster but requires prior knowledge of the necessary parameters. Consult the device TRM for the FLL calculation equations. </para>    </detaileddescription>
  </compounddef>
</doxygen>