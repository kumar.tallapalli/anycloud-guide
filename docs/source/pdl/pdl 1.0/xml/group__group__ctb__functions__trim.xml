<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ctb__functions__trim" kind="group">
    <compoundname>group_ctb_functions_trim</compoundname>
    <title>Offset and Slope Trim Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__trim_1ga0cbe0e749f65fe1e11a810b23479c12d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTB_OpampSetOffset</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_opamp_sel_t opampNum, uint32_t trim)</argsstring>
        <name>Cy_CTB_OpampSetOffset</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>opampNum</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>trim</declname>
        </param>
        <briefdescription>
<para>Override the CTB opamp offset factory trim. </para>        </briefdescription>
        <detaileddescription>
<para>The trim is a six bit value and the MSB is a direction bit.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Bit 5 &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Bits 4:0 &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Note  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;0 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;00000 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Negative trim direction - minimum setting  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;0 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;11111 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Negative trim direction - maximum setting  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;1 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;00000 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Positive trim direction - minimum setting  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;1 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;11111 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Positive trim direction - maximum setting  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>opampNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref>, <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref>, or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref></para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>trim</parametername>
</parameternamelist>
<parameterdescription>
<para>Trim value from 0 to 63</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Calibrate<sp />the<sp />offset<sp />trim<sp />by<sp />iterating<sp />through<sp />all<sp />the<sp />possible<sp />trim<sp />values.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint8_t<sp />trim;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">for</highlight><highlight class="normal"><sp />(trim<sp />=<sp />0u;<sp />trim<sp />&lt;=<sp />0x3F;<sp />trim++)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__trim_1ga0cbe0e749f65fe1e11a810b23479c12d">Cy_CTB_OpampSetOffset</ref>(CTBM0,<sp />CY_CTB_OPAMP_0,<sp />trim);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Do<sp />something<sp />after<sp />changing<sp />the<sp />offset<sp />trim.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1081" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctb.c" bodystart="1067" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1077" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__trim_1gae63790af2bbcded2b073a1d5a59f169d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_CTB_OpampGetOffset</definition>
        <argsstring>(const CTBM_Type *base, cy_en_ctb_opamp_sel_t opampNum)</argsstring>
        <name>Cy_CTB_OpampGetOffset</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>opampNum</declname>
        </param>
        <briefdescription>
<para>Return the current CTB opamp offset trim value. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>opampNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref> or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Offset trim value</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Save<sp />the<sp />factory<sp />offset<sp />trim<sp />before<sp />changing<sp />it.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />factoryOffset;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />factoryOffset<sp />=<sp /><ref kindref="member" refid="group__group__ctb__functions__trim_1gae63790af2bbcded2b073a1d5a59f169d">Cy_CTB_OpampGetOffset</ref>(CTBM0,<sp />CY_CTB_OPAMP_0);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1118" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctb.c" bodystart="1102" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1078" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__trim_1gaa1b401598b7f5295e7a5de1c5574def5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTB_OpampSetSlope</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_opamp_sel_t opampNum, uint32_t trim)</argsstring>
        <name>Cy_CTB_OpampSetSlope</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>opampNum</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>trim</declname>
        </param>
        <briefdescription>
<para>Override the CTB opamp slope factory trim. </para>        </briefdescription>
        <detaileddescription>
<para>The offset of the opamp will vary across temperature. This trim compensates for the slope of the offset across temperature. This compensation uses a bias current from the Analog Reference block. To disable it, set the trim to 0.</para><para>The trim is a six bit value and the MSB is a direction bit.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Bit 5 &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Bits 4:0 &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Note  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;0 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;00000 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Negative trim direction - minimum setting  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;0 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;11111 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Negative trim direction - maximum setting  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;1 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;00000 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Positive trim direction - minimum setting  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;1 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;11111 &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Positive trim direction - maximum setting  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>opampNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref>, <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref>, or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref></para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>trim</parametername>
</parameternamelist>
<parameterdescription>
<para>Trim value from 0 to 63</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Calibrate<sp />the<sp />slope<sp />trim<sp />by<sp />iterating<sp />through<sp />all<sp />the<sp />possible<sp />trim<sp />values.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint8_t<sp />trim;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">for</highlight><highlight class="normal"><sp />(trim<sp />=<sp />0u;<sp />trim<sp />&lt;=<sp />0x3F;<sp />trim++)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__trim_1gaa1b401598b7f5295e7a5de1c5574def5">Cy_CTB_OpampSetSlope</ref>(CTBM0,<sp />CY_CTB_OPAMP_0,<sp />trim);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Do<sp />something<sp />after<sp />changing<sp />the<sp />slope<sp />trim.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1190" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctb.c" bodystart="1176" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1079" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__trim_1ga4913a1454b0f4553c477ffebb20dd3a4" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_CTB_OpampGetSlope</definition>
        <argsstring>(const CTBM_Type *base, cy_en_ctb_opamp_sel_t opampNum)</argsstring>
        <name>Cy_CTB_OpampGetSlope</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>opampNum</declname>
        </param>
        <briefdescription>
<para>Return the CTB opamp slope trim value. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>opampNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref> or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Slope trim value</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Save<sp />the<sp />factory<sp />slope<sp />trim<sp />before<sp />changing<sp />it.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />factorySlope;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />factorySlope<sp />=<sp /><ref kindref="member" refid="group__group__ctb__functions__trim_1ga4913a1454b0f4553c477ffebb20dd3a4">Cy_CTB_OpampGetSlope</ref>(CTBM0,<sp />CY_CTB_OPAMP_0);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1227" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctb.c" bodystart="1211" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1080" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>These are advanced functions for trimming the offset and slope of the opamps. </para>    </briefdescription>
    <detaileddescription>
<para>Most users do not need to call these functions and can use the factory trimmed values. </para>    </detaileddescription>
  </compounddef>
</doxygen>