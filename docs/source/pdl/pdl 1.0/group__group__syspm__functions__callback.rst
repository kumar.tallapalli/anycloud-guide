====================
Low Power Callbacks
====================

.. doxygengroup:: group_syspm_functions_callback
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: