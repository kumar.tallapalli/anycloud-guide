==========================
IPC pipes layer (IPC_PIPE)
==========================

.. doxygengroup:: group_ipc_pipe
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__ipc__pipe__macros.rst
   group__group__ipc__pipe__functions.rst
   group__group__ipc__pipe__data__structures.rst
   group__group__ipc__pipe__enums.rst
