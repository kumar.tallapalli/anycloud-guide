==================
Client Functions
==================

.. doxygengroup:: group_crypto_cli_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: