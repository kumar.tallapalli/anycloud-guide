==============================
Basic Configuration Functions
==============================



.. doxygengroup:: group_ctb_functions_basic
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: