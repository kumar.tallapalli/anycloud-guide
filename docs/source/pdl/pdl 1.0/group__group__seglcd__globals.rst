============
Global Data
============

.. doxygengroup:: group_seglcd_globals
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
