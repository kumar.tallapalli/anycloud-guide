=======
Macros
=======

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__syspm__return__status.rst
   group__group__syspm__skip__callback__modes.rst

.. doxygengroup:: group_syspm_macros
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
   
