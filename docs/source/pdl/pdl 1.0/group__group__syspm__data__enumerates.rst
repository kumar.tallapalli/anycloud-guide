=================
Enumerated Types
=================

.. doxygengroup:: group_syspm_data_enumerates
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: