==================
Low Power Callback
==================

.. doxygengroup:: group_i2s_functions_syspm_callback
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: