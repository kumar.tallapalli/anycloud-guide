==============================
SMIF (Serial Memory Interface)
==============================

.. doxygengroup:: group_smif
   :project: pdl1.0
   

.. toctree::

   group__group__smif__macros.rst
   group__group__smif__functions.rst
   group__group__smif__data__structures.rst
   group__group__smif__enums.rst
   


