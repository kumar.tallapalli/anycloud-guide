========================
Core Voltage Regulation
========================

.. doxygengroup:: group_syspm_functions_core_regulators
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
   
API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__syspm__functions__ldo.rst
   group__group__syspm__functions__buck.rst