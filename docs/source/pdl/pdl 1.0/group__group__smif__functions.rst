==========
Functions
==========

.. doxygengroup:: group_smif_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__smif__low__level__functions.rst
   group__group__smif__mem__slot__functions.rst
   group__group__smif__functions__syspm__callback.rst