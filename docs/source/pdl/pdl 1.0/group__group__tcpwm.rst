=========================
TCPWM (Timer Counter PWM)
=========================

.. doxygengroup:: group_tcpwm
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

.. toctree::

   group__group__tcpwm__common.rst
   group__group__tcpwm__counter.rst
   group__group__tcpwm__pwm.rst
   group__group__tcpwm__quaddec.rst
   group__group__tcpwm__shiftreg.rst
   


