======
Macro
======

.. doxygengroup:: group_system_config_macro
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
==============

.. toctree::

   group__group__system__config__system__macro.rst
   group__group__system__config__cm4__status__macro.rst
   group__group__system__config__user__settings__macro.rst
   
