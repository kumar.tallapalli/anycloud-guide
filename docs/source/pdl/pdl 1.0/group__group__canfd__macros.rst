=======
Macros
=======

.. doxygengroup:: group_canfd_macros
   :project: pdl1.0


.. toctree::
   
   group__group__canfd__rx__interrupt__masks.rst
   group__group__canfd__tx__interrupt__masks.rst
   group__group__canfd__error__interrupt__masks.rst 
   group__group__canfd__interrupt__line__masks.rst  
   group__group__canfd__last__state__masks.rst
  
  
   
