==================
Enumerated Types
==================

.. doxygengroup:: group_prot_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: