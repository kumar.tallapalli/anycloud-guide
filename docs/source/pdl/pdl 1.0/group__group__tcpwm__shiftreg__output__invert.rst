=============================
Shift Register output invert
=============================

.. doxygengroup:: group_tcpwm_shiftreg_output_invert
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   