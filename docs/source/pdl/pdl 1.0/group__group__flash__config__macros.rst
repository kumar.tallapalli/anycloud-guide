========================
Flash configuration
========================

.. doxygengroup:: group_flash_config_macros
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: