=====================
Shift Register Status
=====================

.. doxygengroup:: group_tcpwm_shiftreg_status
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   