==========
UART (SCB)
==========


.. doxygengroup:: group_scb_uart 
   :project: pdl1.0 
   :members:

.. toctree::

   group__group__scb__uart__macros.rst
   group__group__scb__uart__functions.rst
   group__group__scb__uart__data__structures.rst
   group__group__scb__uart__enums.rst
