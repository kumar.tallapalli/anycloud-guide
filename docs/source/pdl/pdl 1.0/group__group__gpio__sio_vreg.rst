=======================
SIO output buffer mode
=======================

.. doxygengroup:: group_gpio_sioVreg
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: