=======================
Function return values
=======================

.. doxygengroup:: group_sysclk_returns
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: