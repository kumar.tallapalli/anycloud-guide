===========================
Quadrature Decoder (TCPWM)
===========================

.. doxygengroup:: group_tcpwm_quaddec
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__tcpwm__macros__quaddec.rst
   group__group__tcpwm__functions__quaddec.rst
   group__group__tcpwm__data__structures__quaddec.rst
   


