=====================
Descriptor Functions
=====================


.. doxygengroup:: group_dma_descriptor_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: