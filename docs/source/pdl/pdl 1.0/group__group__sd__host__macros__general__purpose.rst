========================
General Purpose Macros
========================



.. doxygengroup:: group_sd_host_macros_general_purpose
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: