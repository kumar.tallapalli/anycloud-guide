=========
Functions
=========

.. toctree::
 
   group__group__scb__i2c__general__functions.rst
   group__group__scb__i2c__slave__functions.rst
   group__group__scb__i2c__master__high__level__functions.rst
   group__group__scb__i2c__master__low__level__functions.rst
   group__group__scb__i2c__interrupt__functions.rst
   group__group__scb__i2c__low__power__functions.rst

.. doxygengroup:: group_scb_i2c_functions 
   :project: pdl1.0 
   :members: