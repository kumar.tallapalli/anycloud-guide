========================================
Message Authentication Code (CMAC, HMAC)
========================================

.. toctree::
   
   group__group__crypto__lld__mac__functions.rst

.. doxygengroup:: group_crypto_lld_mac
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

