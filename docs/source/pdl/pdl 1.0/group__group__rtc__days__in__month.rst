=====================================
Number of days in month definitions
=====================================



.. doxygengroup:: group_rtc_days_in_month
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: