=============================
Endpoint 0 Service Functions
=============================

.. doxygengroup:: group_usbfs_dev_hal_functions_ep0_service
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: