=================
Global variables
=================

.. doxygengroup:: group_sysint_globals
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: