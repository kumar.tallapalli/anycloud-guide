=========================
Switch Control Functions
=========================



.. doxygengroup:: group_ctb_functions_switches
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: