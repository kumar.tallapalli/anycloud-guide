===========
Functions
===========

.. doxygengroup:: group_scb_ezi2c_functions 
   :project: pdl1.0

.. toctree::
 
   group__group__scb__ezi2c__general__functions.rst
   group__group__scb__ezi2c__slave__functions.rst
   group__group__scb__ezi2c__low__power__functions.rst