====================
Low Power Callbacks
====================



.. doxygengroup:: group_rtc_low_power_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: