==========
I2C (SCB)
==========

.. doxygengroup:: group_scb_i2c
   :project: pdl1.0 
   :members:

.. toctree::

   group__group__scb__i2c__macros.rst
   group__group__scb__i2c__functions.rst
   group__group__scb__i2c__data__structures.rst
   group__group__scb__i2c__enums.rst  

