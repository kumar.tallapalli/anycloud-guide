============
Status codes
============

.. doxygengroup:: group_syslib_macros_status_codes
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: