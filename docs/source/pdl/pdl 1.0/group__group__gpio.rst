===================================
GPIO (General Purpose Input Output)
===================================

.. doxygengroup:: group_gpio
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

.. toctree::

   group__group__gpio__macros.rst
   group__group__gpio__functions.rst
   group__group__gpio__data__structures.rst
   group__group__gpio__enums.rst
   


