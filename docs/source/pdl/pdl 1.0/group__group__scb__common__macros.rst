=======
Macros
=======


.. toctree::
   
   group__group__scb__common__macros__intr__cause.rst
   group__group__scb__common__macros__tx__intr.rst
   group__group__scb__common__macros__rx__intr.rst
   group__group__scb__common__macros__slave__intr.rst
   group__group__scb__common__macros__master__intr.rst
   group__group__scb__common__macros__i2c__intr.rst
   group__group__scb__common__macros___spi_intr_statuses.rst
   
   
   
   

.. doxygengroup:: group_scb_common_macros
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: