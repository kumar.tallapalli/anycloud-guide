============================
Default registers constants
============================

.. doxygengroup:: group_tcpwm_reg_const
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: