=========================
PWM CLK Prescaler values
=========================

.. doxygengroup:: group_tcpwm_pwm_clk_prescalers
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: