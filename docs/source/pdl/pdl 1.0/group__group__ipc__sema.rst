===============================
IPC semaphores layer (IPC_SEMA)
===============================

.. doxygengroup:: group_ipc_sema
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__ipc__sema__macros.rst
   group__group__ipc__sema__functions.rst
   group__group__ipc__sema__enums.rst
