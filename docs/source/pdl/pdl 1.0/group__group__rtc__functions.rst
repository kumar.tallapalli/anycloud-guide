===========
Functions
===========





.. toctree::
   
   group__group__rtc__general__functions.rst
   group__group__rtc__alarm__functions.rst
   group__group__rtc__dst__functions.rst
   group__group__rtc__low__level__functions.rst
   group__group__rtc__interrupt__functions.rst
   group__group__rtc__low__power__functions.rst
   
   
   

.. doxygengroup:: group_rtc_functions
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: