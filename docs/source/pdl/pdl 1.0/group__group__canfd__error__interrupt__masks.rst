======================
Error Interrupt masks
======================

.. doxygengroup:: group_canfd_error_interrupt_masks
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: