================
Enumerated Types
================

.. doxygengroup:: group_i2s_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: