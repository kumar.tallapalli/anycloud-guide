=======================
RTC Status definitions
=======================



.. doxygengroup:: group_rtc_busy_status
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: