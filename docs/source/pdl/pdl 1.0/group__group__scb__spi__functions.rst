==========
Functions
==========

.. toctree::

   group__group__scb__spi__general__functions.rst
   group__group__scb__spi__high__level__functions.rst
   group__group__scb__spi__low__level__functions.rst
   group__group__scb__spi__interrupt__functions.rst
   group__group__scb__spi__low__power__functions.rst


.. doxygengroup:: group_scb_spi_functions 
   :project: pdl1.0 
   :members: