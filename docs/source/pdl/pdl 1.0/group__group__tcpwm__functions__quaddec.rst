=========
Functions
=========

.. doxygengroup:: group_tcpwm_functions_quaddec
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: