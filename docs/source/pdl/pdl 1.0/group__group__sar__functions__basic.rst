===================================
Initialization and Basic Functions
===================================




.. doxygengroup:: group_sar_functions_basic
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: