=========================
One-To-One Trigger Lines
=========================

.. doxygengroup:: group_trigmux_1to1_enums
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: