=================================
Data Endpoint Transfer Functions
=================================

.. doxygengroup:: group_usbfs_dev_hal_functions_data_xfer
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: