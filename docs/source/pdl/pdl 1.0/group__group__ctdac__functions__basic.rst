==============================
Basic Configuration Functions
==============================

.. doxygengroup:: group_ctdac_functions_basic
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: