===============================
Interrupt line selection masks
===============================

.. doxygengroup:: group_canfd_interrupt_line_masks
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

