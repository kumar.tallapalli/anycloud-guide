==================
Voltage trip mode
==================

.. doxygengroup:: group_gpio_vtrip
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: