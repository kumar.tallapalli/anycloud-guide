======================================
Data Endpoint Configuration Functions
======================================

.. doxygengroup:: group_usbfs_dev_hal_functions_endpoint_config
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: