=================================
IPC (Inter Process Communication)
=================================

.. doxygengroup:: group_ipc
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

.. toctree::

   group__group__ipc__drv.rst
   group__group__ipc__sema.rst
   group__group__ipc__pipe.rst
   


