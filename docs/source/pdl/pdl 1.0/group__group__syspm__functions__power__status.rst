============
Power Status
============

.. doxygengroup:: group_syspm_functions_power_status
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: