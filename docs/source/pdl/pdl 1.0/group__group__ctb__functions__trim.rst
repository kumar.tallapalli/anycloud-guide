================================
Offset and Slope Trim Functions
================================




.. doxygengroup:: group_ctb_functions_trim
   :project: pdl1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: