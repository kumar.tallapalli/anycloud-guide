==================
BSP API Reference
==================

.. toctree::
   :hidden:

   group__group__bsp__settings.rst
   group__group__bsp__pin__state.rst
   group__group__bsp__macros.rst
   group__group__bsp__functions.rst


| `BSP Settings <group__group__bsp__settings.html>`__  Peripheral Default HAL Settings:

  
+---------+-------------+----------------+--------+
|Resource |Parameter    |Value           |Remarks |
+=========+=============+================+========+
|         |Flow control |No flow control |        |
|UART     +-------------+----------------+--------+
|         |Data format  |8N1             |        |
|         +-------------+----------------+--------+
|         |Baud rate    |115200          |        |
+---------+-------------+----------------+--------+


| `Pin States <group__group__bsp__pin__state.html>`__
| `Macros <group__group__bsp__macros.html>`__
| `Functions <group__group__bsp__functions.html>`__

