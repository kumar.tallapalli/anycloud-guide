====================
WiFi Initialization
====================

.. doxygengroup:: group_bsp_wifi
   :project: whd-bsp-integration 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: