====================
WiFi Initialization
====================

.. doxygengroup:: group_bsp_wifi
   :project: whd-bsp-integration 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: