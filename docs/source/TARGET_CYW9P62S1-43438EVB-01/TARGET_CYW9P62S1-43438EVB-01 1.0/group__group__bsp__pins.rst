=============
Pin Mappings
=============



.. doxygengroup:: group_bsp_pins
   :project: TARGET_CYW9P62S1-43438EVB-01 1.0
   :members:
   :protected-members:
   :private-members:


.. raw:: html

    <h2>API Reference</h2>

.. toctree::

   group__group__bsp__pins__led.rst
   group__group__bsp__pins__btn.rst
   group__group__bsp__pins__comm.rst
   group__group__bsp__pins__arduino.rst
   group__group__bsp__pins__j2.rst
 
 
