====================
Arduino Header Pins
====================

.. doxygengroup:: group_bsp_pins_arduino
   :project: TARGET_CYW9P62S1-43438EVB-01 1.0
   :members:
   :protected-members:
   :private-members:
