==========
Functions
==========

.. doxygengroup:: group_usb_dev_audio_functions
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: