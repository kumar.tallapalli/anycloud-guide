=========================================
cy_stc_usb_dev_control_transfer_t Struct
=========================================

.. doxygenstruct:: cy_stc_usb_dev_control_transfer_t
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
