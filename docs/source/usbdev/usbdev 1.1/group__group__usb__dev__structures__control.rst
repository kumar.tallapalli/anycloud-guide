=================
Control Transfer
=================


.. doxygengroup:: group_usb_dev_structures_control
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::

   structcy__stc__usb__dev__bm__request.rst
   structcy__stc__usb__dev__setup__packet__t.rst
   structcy__stc__usb__dev__control__transfer__t.rst
  

