=================================
Vendor Request Support Functions
=================================

.. doxygengroup:: group_usb_dev_functions_vendor_support
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
