===============================================
cy_stc_usb_dev_setup_packet_t Struct Reference
===============================================

.. doxygenstruct:: cy_stc_usb_dev_setup_packet_t
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
