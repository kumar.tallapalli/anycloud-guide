======================================
cy_stc_usb_dev_class_ll_item_t Struct
======================================

.. doxygenstruct:: cy_stc_usb_dev_class_ll_item_t
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
