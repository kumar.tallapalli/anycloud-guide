================
Data Structures
================

.. doxygengroup:: group_usb_dev_data_structures
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::

   group__group__usb__dev__structures__device.rst
   group__group__usb__dev__structures__device__descr.rst
   group__group__usb__dev__structures__control.rst
   group__group__usb__dev__structures__class.rst
   group__group__usb__dev__structures__func__ptr.rst
   structcy__stc__usb__dev__config__t.rst
   structcy__stc__usb__dev__context__t.rst


