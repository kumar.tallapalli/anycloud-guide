==========
Functions
==========

.. doxygengroup:: group_usb_dev_hid_functions
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
