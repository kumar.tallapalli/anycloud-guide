==================
Service Functions
==================

.. doxygengroup:: group_usb_dev_functions_service
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
