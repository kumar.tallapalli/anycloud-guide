=========================
Initialization Functions
=========================

.. doxygengroup:: group_usb_dev_functions_common
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
