============
Descriptors
============

.. doxygengroup:: group_usb_dev_macros_device_descr
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: