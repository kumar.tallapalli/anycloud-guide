==========================================================
Cypress USB Device Middleware Library 2.0: Data Structures
==========================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress USB Device Middleware  |
      |                                   |    Library 2.0                    |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         Here are the data structures with brief descriptions:

      .. container:: directory

         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb                  | USBFS Device Alternate Interface |
         | _dev_alternate_t <structcy__stc_ | structure                        |
         | _usb__dev__alternate__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_dev_audi         | Audio class context structure    |
         | o_context_t <structcy__stc__usb_ |                                  |
         | _dev__audio__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_u                    | Parsed bmRequest                 |
         | sb_dev_bm_request <structcy__stc |                                  |
         | __usb__dev__bm__request.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_de               | CDC class configuration          |
         | v_cdc_config_t <structcy__stc__u | structure                        |
         | sb__dev__cdc__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_dev_             | CDC class context structure      |
         | cdc_context_t <structcy__stc__us |                                  |
         | b__dev__cdc__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_dev_class        | Class linked list element        |
         | _ll_item_t <structcy__stc__usb__ |                                  |
         | dev__class__ll__item__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy                          | Class support definition - each  |
         | _stc_usb_dev_class_t <structcy__ | class must provide this          |
         | stc__usb__dev__class__t.html>`__ | structure                        |
         +----------------------------------+----------------------------------+
         |  C\ `cy_s                        | USB Device configuration         |
         | tc_usb_dev_config_t <structcy__s | structure                        |
         | tc__usb__dev__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_dev_con          | USBFS Device Configuration       |
         | figuration_t <structcy__stc__usb | structure                        |
         | __dev__configuration__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc                      | USB Device context structure     |
         | _usb_dev_context_t <structcy__st |                                  |
         | c__usb__dev__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_dev_control_tr   | Execute transfer use this        |
         | ansfer_t <structcy__stc__usb__de | structure                        |
         | v__control__transfer__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_s                        | USBFS Device structure           |
         | tc_usb_dev_device_t <structcy__s |                                  |
         | tc__usb__dev__device__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_u                    | USBFS Device endpointDescriptor  |
         | sb_dev_endpoint_t <structcy__stc | structure                        |
         | __usb__dev__endpoint__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_de               | HID class configuration          |
         | v_hid_config_t <structcy__stc__u | structure                        |
         | sb__dev__hid__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_dev_             | HID class context structure      |
         | hid_context_t <structcy__stc__us |                                  |
         | b__dev__hid__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\                              | USBFS Device HID structure       |
         |  `cy_stc_usb_dev_hid_t <structcy |                                  |
         | __stc__usb__dev__hid__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb                  | USBFS Device Interface structure |
         | _dev_interface_t <structcy__stc_ |                                  |
         | _usb__dev__interface__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_dev_ms_          | USBFS Device MS OS String        |
         | os_string_t <structcy__stc__usb_ | Descriptors structure            |
         | _dev__ms__os__string__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usb_dev_se           | Store request                    |
         | tup_packet_t <structcy__stc__usb |                                  |
         | __dev__setup__packet__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_s                        | USBFS Device String Descriptors  |
         | tc_usb_dev_string_t <structcy__s | structure                        |
         | tc__usb__dev__string__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usbdev_              | Configuration descriptor         |
         | config_descr_t <structcy__stc__u |                                  |
         | sbdev__config__descr__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usbdev_              | Device descriptor                |
         | device_descr_t <structcy__stc__u |                                  |
         | sbdev__device__descr__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usbdev_endp          | Endpoint descriptor              |
         | oint_descr_t <structcy__stc__usb |                                  |
         | dev__endpoint__descr__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_usbdev_interf        | Interface descriptor             |
         | ace_descr_t <structcy__stc__usbd |                                  |
         | ev__interface__descr__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
