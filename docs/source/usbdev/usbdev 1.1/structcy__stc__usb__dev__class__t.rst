========================================
cy_stc_usb_dev_class_t Struct Reference
========================================

.. doxygenstruct:: cy_stc_usb_dev_class_t
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
