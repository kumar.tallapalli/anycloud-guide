=====================================
cy_stc_usb_dev_audio_context_t Struct
=====================================

.. doxygenstruct:: cy_stc_usb_dev_audio_context_t
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
