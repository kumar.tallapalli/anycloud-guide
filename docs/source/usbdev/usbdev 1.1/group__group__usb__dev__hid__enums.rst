=================
Enumerated Types
=================

.. doxygengroup:: group_usb_dev_hid_enums
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
