===================================
cy_stc_usb_dev_hid_config_t Struct
===================================

.. doxygenstruct:: cy_stc_usb_dev_hid_config_t
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
