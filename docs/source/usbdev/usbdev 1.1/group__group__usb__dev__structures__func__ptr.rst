==================
Function Pointers
==================

.. doxygengroup:: group_usb_dev_structures_func_ptr
   :project: usbdev1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: