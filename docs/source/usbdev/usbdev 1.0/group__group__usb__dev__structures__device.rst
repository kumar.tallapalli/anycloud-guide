===================
Device Information
===================
.. doxygengroup:: group_usb_dev_structures_device
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
.. toctree::

   structcy__stc__usb__dev__endpoint__t.rst
   structcy__stc__usb__dev__hid__t.rst
   structcy__stc__usb__dev__alternate__t.rst
   structcy__stc__usb__dev__interface__t.rst
   structcy__stc__usb__dev__configuration__t.rst
   structcy__stc__usb__dev__ms__os__string__t.rst
   structcy__stc__usb__dev__string__t.rst
   structcy__stc__usb__dev__device__t.rst
   

