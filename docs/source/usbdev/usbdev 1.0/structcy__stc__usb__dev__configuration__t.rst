================================================
cy_stc_usb_dev_configuration_t Struct Reference
================================================

.. doxygenstruct:: cy_stc_usb_dev_configuration_t
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
