================================================
cy_stc_usbdev_endpoint_descr_t Struct Reference
================================================

.. doxygenstruct:: cy_stc_usbdev_endpoint_descr_t
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
