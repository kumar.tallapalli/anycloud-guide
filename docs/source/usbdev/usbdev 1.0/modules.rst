==============
API Reference
==============

.. raw:: html

   <hr>

.. toctree::
   :hidden:

   group__group__usb__device.rst
   group__group__usb__dev__audio.rst
   group__group__usb__dev__cdc.rst
   group__group__usb__dev__hid.rst


Provides the list of API Reference

+----------------------------------+----------------------------------+
| \ `Device <gr                    | This section provides an API     |
| oup__group__usb__device.html>`__ | description for the core         |
|                                  | functionality of the USB Device: |
|                                  | initialization, status           |
|                                  | information, data transfers, and |
|                                  | class support                    |
+----------------------------------+----------------------------------+
| \ `Macros <group__               |                                  |
| group__usb__dev__macros.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Des                           |                                  |
| criptors <group__group__usb__dev |                                  |
| __macros__device__descr.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Functions <group__gro         |                                  |
| up__usb__dev__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Initialization                |                                  |
| Functions <group__group__usb_    |                                  |
| _dev__functions__common.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Service                       |                                  |
| Functions <group__group__usb__   |                                  |
| dev__functions__service.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Data Transfer                 |                                  |
| Funct                            |                                  |
| ions <group__group__usb__dev__fu |                                  |
| nctions__data__transfer.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Vendor Request Support        |                                  |
| Functi                           |                                  |
| ons <group__group__usb__dev__fun |                                  |
| ctions__vendor__support.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Class Support                 |                                  |
| Funct                            |                                  |
| ions <group__group__usb__dev__fu |                                  |
| nctions__class__support.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Data                          |                                  |
| Structures <group__group__usb    |                                  |
| __dev__data__structures.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Device                        |                                  |
| Information <group__group__usb__ |                                  |
| dev__structures__device.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Device                        |                                  |
| Descrip                          |                                  |
| tors <group__group__usb__dev__st |                                  |
| ructures__device__descr.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Control                       |                                  |
| Transfer <group__group__usb__d   |                                  |
| ev__structures__control.html>`__ |                                  |
+----------------------------------+----------------------------------+
|  \ `Class                        |                                  |
| Support <group__group__usb_      |                                  |
| _dev__structures__class.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Function                      |                                  |
| Pointers <group__group__usb__dev |                                  |
| __structures__func__ptr.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Enumerated                    |                                  |
| Types <group_                    |                                  |
| _group__usb__dev__enums.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Audio                         | This section provides API        |
| Class <group_                    | description for the Audio class  |
| _group__usb__dev__audio.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Macros <group__group__        |                                  |
| usb__dev__audio__macros.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Functions <group__group__usb  |                                  |
| __dev__audio__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Data Structures               |                                  |
| <group__group__usb__dev__        |                                  |
| audio__data__structures.html>`__ |                                  |
|                                  |                                  |
+----------------------------------+----------------------------------+
| \ `CDC Class                     | This section provides API        |
| <group__group__                  | description for the CDC class    |
| usb__dev__cdc.html>`__           |                                  |
+----------------------------------+----------------------------------+
| \ `Macros <group__group          |                                  |
| __usb__dev__cdc__macros.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Functions <group__group__u    |                                  |
| sb__dev__cdc__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Data Structures               |                                  |
| <group__group__usb__dev          |                                  |
| __cdc__data__structures.html>`__ |                                  |
|                                  |                                  |
+----------------------------------+----------------------------------+
| \ `Enumerated                    |                                  |
| Types <group__grou               |                                  |
| p__usb__dev__cdc__enums.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `HID                           | This section provides the API    |
| Class <grou                      | description for the HID class    |
| p__group__usb__dev__hid.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Macros <group__group          |                                  |
| __usb__dev__hid__macros.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Functions <group__group__u    |                                  |
| sb__dev__hid__functions.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Data Structures               |                                  |
| <group__group__usb__dev          |                                  |
| __hid__data__structures.html>`__ |                                  |
|                                  |                                  |
+----------------------------------+----------------------------------+
| \ `Enumerated                    |                                  |
| Types <group__group              |                                  |
| __usb__dev__hid__enums.html>`__  |                                  |
+----------------------------------+----------------------------------+
