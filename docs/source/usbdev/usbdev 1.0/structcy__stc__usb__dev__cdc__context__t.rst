====================================
cy_stc_usb_dev_cdc_context_t Struct
====================================

.. doxygenstruct:: cy_stc_usb_dev_cdc_context_t
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
