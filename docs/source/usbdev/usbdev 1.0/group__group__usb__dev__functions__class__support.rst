========================
Class Support Functions
========================

.. doxygengroup:: group_usb_dev_functions_class_support
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
