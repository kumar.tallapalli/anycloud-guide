================
Data Structures
================
.. doxygengroup:: group_usb_dev_hid_data_structures
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::

   structcy__stc__usb__dev__hid__config__t.rst
   structcy__stc__usb__dev__hid__context__t.rst
   


