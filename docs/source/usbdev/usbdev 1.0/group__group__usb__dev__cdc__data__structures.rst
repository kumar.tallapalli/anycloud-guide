================
Data Structures
================


.. doxygengroup:: group_usb_dev_cdc_data_structures
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::

   structcy__stc__usb__dev__cdc__config__t.rst
   structcy__stc__usb__dev__cdc__context__t.rst
   


