============
Descriptors
============

.. doxygengroup:: group_usb_dev_macros_device_descr
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: