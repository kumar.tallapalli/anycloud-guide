==========
Functions
==========


.. doxygengroup:: group_usb_dev_functions
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::

   group__group__usb__dev__functions__common.rst
   group__group__usb__dev__functions__service.rst
   group__group__usb__dev__functions__data__transfer.rst
   group__group__usb__dev__functions__vendor__support.rst
   group__group__usb__dev__functions__class__support.rst
   
   
