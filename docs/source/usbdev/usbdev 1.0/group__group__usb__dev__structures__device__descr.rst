===================
Device Descriptors
===================


.. doxygengroup:: group_usb_dev_structures_device_descr
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::

   structcy__stc__usbdev__device__descr__t.rst
   structcy__stc__usbdev__config__descr__t.rst
   structcy__stc__usbdev__interface__descr__t.rst
   structcy__stc__usbdev__endpoint__descr__t.rst


