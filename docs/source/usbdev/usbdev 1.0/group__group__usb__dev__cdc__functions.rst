==========
Functions
==========

.. doxygengroup:: group_usb_dev_cdc_functions
   :project: usbdev1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: