================
Data Structures
================
   
.. doxygengroup:: group_csdadc_data_structures
   :project: csdadc1.0

   
.. toctree::
   :hidden:

   structcy__stc__csdadc__ch__pin__t.rst
   structcy__stc__csdadc__config__t.rst
   structcy__stc__csdadc__result__t.rst
   structcy__stc__csdadc__context__t.rst