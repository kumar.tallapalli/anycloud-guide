===========================================================
Cypress CSDADC Middleware Library 2.0: Data Structure Index
===========================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CSDADC Middleware      |
      |                                   |    Library 2.0                    |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: qindex

         `c <#letter_c>`__

      +-----------------------------------------------------------------------+
      | .. container:: ah                                                     |
      |                                                                       |
      |      c                                                                |
      +-----------------------------------------------------------------------+

`cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html>`__   
`cy_stc_csdadc_result_t <structcy__stc__csdadc__result__t.html>`__   
`cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html>`__   
`cy_stc_csdadc_ch_pin_t <structcy__stc__csdadc__ch__pin__t.html>`__   

.. container:: qindex

   `c <#letter_c>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
