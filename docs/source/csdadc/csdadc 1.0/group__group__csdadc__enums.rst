=================
Enumerated types
=================
   
.. doxygengroup:: group_csdadc_enums
   :project: csdadc1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: