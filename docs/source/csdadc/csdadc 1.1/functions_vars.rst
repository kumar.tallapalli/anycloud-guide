==============================================================
Cypress CSDADC Middleware Library 2.0: Data Fields - Variables
==============================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CSDADC Middleware      |
      |                                   |    Library 2.0                    |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - a -
         :name: a--

      -  acqCycles :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#ac7fe40ef8aa327f04cbd243b4dabdd3d>`__
      -  acqTime :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#ad90f7ff0f0609800d03664c9d318e119>`__
      -  activeCh :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a5ae94561284c9ae3b7be9c0fda75c1f2>`__
      -  adcResult :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a6e383bc56a5d2f62797f6c403f1eeba7>`__
      -  azCycles :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a885b36110ce22b1cfd674355ff74e9ac>`__
      -  azTime :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#ab2acac32d7240e0c4ca0e1656d64802d>`__

      .. rubric:: - b -
         :name: b--

      -  base :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a1f7d7b2ae9bf58c2ed4754ee1ef130cc>`__

      .. rubric:: - c -
         :name: c--

      -  calibrInterval :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a5f3794db656b8dcabeec9314cc8c1056>`__
      -  cfgCopy :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a4f8d78cb554f542f9a75092dd0e74e66>`__
      -  chMask :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#ae0f76d5f90a88c848185cb498b07a8c6>`__
      -  code :
         `cy_stc_csdadc_result_t <structcy__stc__csdadc__result__t.html#a6b22704dfb24e5af0435277b3dc27bd1>`__
      -  codeMax :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#aab3a9f8d792737f5df397ea8d76dbd2d>`__
      -  counter :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#aefe62db3203b9c75ad63dc2b86151c34>`__
      -  cpuClk :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a9fca865bbb6a6eac1d74f4eca5730011>`__
      -  csdCxtPtr :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#ac22807f5b6caf29aaef89bbed27ade3e>`__
      -  csdInitTime :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a4d4ba449bf0340bb424d18b4c345f24b>`__

      .. rubric:: - i -
         :name: i--

      -  idac :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a0eb0a009aefebab573606ac86659d18c>`__
      -  idacCalibrationEn :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#aae6659775d8a017da3672160ba7de9bf>`__
      -  ioPcPtr :
         `cy_stc_csdadc_ch_pin_t <structcy__stc__csdadc__ch__pin__t.html#a779f03fc5a410ad80fce366739f2641c>`__

      .. rubric:: - m -
         :name: m--

      -  mVolts :
         `cy_stc_csdadc_result_t <structcy__stc__csdadc__result__t.html#a990e2904dba2fa3e32618d95f35223ee>`__

      .. rubric:: - n -
         :name: n--

      -  numChannels :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a23dde4c610164db57e735247cae2a614>`__

      .. rubric:: - o -
         :name: o--

      -  operClkDivider :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#ad52ccca632d93990d9d823558f620984>`__

      .. rubric:: - p -
         :name: p--

      -  periClk :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a741a2776e5dd698eaa9816cce8192f87>`__
      -  periDivInd :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#aaa975ceb0fc31d67a1c4190a06ecd374>`__
      -  periDivTyp :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a8eb894da889de622b62002b79658a52f>`__
      -  pin :
         `cy_stc_csdadc_ch_pin_t <structcy__stc__csdadc__ch__pin__t.html#abd34cbae8a1162d22e4619f031b5c80f>`__
      -  ptrEOCCallback :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a9eabf78f640fbc46f50676cb1678d2f8>`__
      -  ptrPinList :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#af0839917f36aa586434fd2c07a042043>`__

      .. rubric:: - r -
         :name: r--

      -  range :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a65ce5b914a728dd7f11623342392a18c>`__
      -  resolution :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a5419abcb7049711f0a174ec7dcfee78b>`__

      .. rubric:: - s -
         :name: s--

      -  snsClkDivider :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a157f4e14e95c451c10254bc2b5199284>`__
      -  status :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a640ef5a8ad40a654cedf8e9ab3304c0a>`__

      .. rubric:: - t -
         :name: t--

      -  tFull :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#ad736c4241f7fb1e4dbda9d069607587b>`__
      -  tRecover :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#abe88be29090b4cb9706d59ef884bba56>`__
      -  tVdda2Vref :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a5eeca82a30d56501d957e2c9814bd5ff>`__
      -  tVssa2Vref :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a0ad124d61469eb01715dcf0da463a47b>`__

      .. rubric:: - v -
         :name: v--

      -  vBusBMv :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a75d456b25b6f157cf69064c9f8bf4423>`__
      -  vdda :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a70dcda504cca76fccc718cfe567cb643>`__
      -  vddaMv :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#abdbf95d6c9d2929fa6b8b3230df8880c>`__
      -  vMaxMv :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#abdf04370aa730b05d3065a202a4e2556>`__
      -  vref :
         `cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html#a6ad9e565c92f9e2cfda8018ce151bd19>`__
      -  vRefGain :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a71583fa394151889778b58d0b0d94f11>`__
      -  vRefMv :
         `cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html#a57dd55db68b8df92fee8a535b5de89c7>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
