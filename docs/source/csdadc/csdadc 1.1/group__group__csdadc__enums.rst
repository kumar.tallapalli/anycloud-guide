=================
Enumerated types
=================
   
.. doxygengroup:: group_csdadc_enums
   :project: csdadc1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: