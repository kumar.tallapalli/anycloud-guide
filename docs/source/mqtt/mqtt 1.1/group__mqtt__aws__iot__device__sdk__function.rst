===================
AWS IoT Device SDK
===================



.. doxygengroup:: mqtt_aws_iot_device_sdk_function
   :project: mqtt 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: