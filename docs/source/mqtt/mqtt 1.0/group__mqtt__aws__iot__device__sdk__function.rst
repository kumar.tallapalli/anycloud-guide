===================
AWS IoT Device SDK
===================



.. doxygengroup:: mqtt_aws_iot_device_sdk_function
   :project: mqtt 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: