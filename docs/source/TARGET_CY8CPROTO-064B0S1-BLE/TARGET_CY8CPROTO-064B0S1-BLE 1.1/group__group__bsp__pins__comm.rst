===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CY8CPROTO-064B0S1-BLE 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: