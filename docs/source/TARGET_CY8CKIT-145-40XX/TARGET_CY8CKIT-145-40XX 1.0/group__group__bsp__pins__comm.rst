===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CY8CKIT-145-40XX 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: