<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__result" kind="group">
    <compoundname>group_result</compoundname>
    <title>Result Type</title>
      <sectiondef kind="user-defined">
      <header>Fields</header>
      <description><para>Utility macros for constructing result values and extracting individual fields from existing results. </para></description>
      <memberdef id="group__group__result_1ga9ae9b152a38632a9e0f8620845c6cea9" kind="define" prot="public" static="no">
        <name>CY_RSLT_GET_TYPE</name>
        <param><defname>x</defname></param>
        <initializer>(((x) &gt;&gt; CY_RSLT_TYPE_POSITION) &amp; CY_RSLT_TYPE_MASK)</initializer>
        <briefdescription>
<para>Get the value of the result type field. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>x</parametername>
</parameternamelist>
<parameterdescription>
<para>the <ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref> value from which to extract the result type </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="106" column="9" file="output/libs/core-lib/include/cy_result.h" line="106" />
      </memberdef>
      <memberdef id="group__group__result_1gab959fc67da8489c03d3ab631442393b4" kind="define" prot="public" static="no">
        <name>CY_RSLT_GET_MODULE</name>
        <param><defname>x</defname></param>
        <initializer>(((x) &gt;&gt; CY_RSLT_MODULE_POSITION) &amp; CY_RSLT_MODULE_MASK)</initializer>
        <briefdescription>
<para>Get the value of the module identifier field. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>x</parametername>
</parameternamelist>
<parameterdescription>
<para>the <ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref> value from which to extract the module id </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="111" column="9" file="output/libs/core-lib/include/cy_result.h" line="111" />
      </memberdef>
      <memberdef id="group__group__result_1ga5a7fc037210b7ff81c65160e11823443" kind="define" prot="public" static="no">
        <name>CY_RSLT_GET_CODE</name>
        <param><defname>x</defname></param>
        <initializer>(((x) &gt;&gt; CY_RSLT_CODE_POSITION) &amp; CY_RSLT_CODE_MASK)</initializer>
        <briefdescription>
<para>Get the value of the result code field. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>x</parametername>
</parameternamelist>
<parameterdescription>
<para>the <ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref> value from which to extract the result code </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="116" column="9" file="output/libs/core-lib/include/cy_result.h" line="116" />
      </memberdef>
      <memberdef id="group__group__result_1ga711a53e03635b4f3e21c41aaf9459251" kind="define" prot="public" static="no">
        <name>CY_RSLT_CREATE</name>
        <param><defname>type</defname></param>
        <param><defname>module</defname></param>
        <param><defname>code</defname></param>
        <initializer>((((module) &amp; CY_RSLT_MODULE_MASK) &lt;&lt; CY_RSLT_MODULE_POSITION) | \
    (((code) &amp; CY_RSLT_CODE_MASK) &lt;&lt; CY_RSLT_CODE_POSITION) | \
    (((type) &amp; CY_RSLT_TYPE_MASK) &lt;&lt; CY_RSLT_TYPE_POSITION))</initializer>
        <briefdescription>
<para>Create a new <ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref> value that encodes the specified type, module, and result code. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>type</parametername>
</parameternamelist>
<parameterdescription>
<para>one of <ref kindref="member" refid="group__group__result_1ga611032ac7ceaccf05943b73868e13438">CY_RSLT_TYPE_INFO</ref>, <ref kindref="member" refid="group__group__result_1ga5aa8f2db241158e42052d8940045d2a0">CY_RSLT_TYPE_WARNING</ref>, <ref kindref="member" refid="group__group__result_1ga8d656d344524464af71296f2140a7b37">CY_RSLT_TYPE_ERROR</ref>, <ref kindref="member" refid="group__group__result_1ga2bc4a48af22980d72766f825eda62658">CY_RSLT_TYPE_FATAL</ref> </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>module</parametername>
</parameternamelist>
<parameterdescription>
<para>Identifies the module where this result originated; see <ref kindref="member" refid="group__group__result_1anchor_modules">Modules</ref>. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>code</parametername>
</parameternamelist>
<parameterdescription>
<para>a module-defined identifier to identify the specific situation that this result describes. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="126" column="9" file="output/libs/core-lib/include/cy_result.h" line="126" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="user-defined">
      <header>Result Types</header>
      <description><para>Defines codes to identify the type of result. </para></description>
      <memberdef id="group__group__result_1ga611032ac7ceaccf05943b73868e13438" kind="define" prot="public" static="no">
        <name>CY_RSLT_TYPE_INFO</name>
        <initializer>(0U)</initializer>
        <briefdescription>
<para>The result code is informational-only. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="140" column="9" file="output/libs/core-lib/include/cy_result.h" line="140" />
      </memberdef>
      <memberdef id="group__group__result_1ga5aa8f2db241158e42052d8940045d2a0" kind="define" prot="public" static="no">
        <name>CY_RSLT_TYPE_WARNING</name>
        <initializer>(1U)</initializer>
        <briefdescription>
<para>The result code is warning of a problem but will proceed. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="142" column="9" file="output/libs/core-lib/include/cy_result.h" line="142" />
      </memberdef>
      <memberdef id="group__group__result_1ga8d656d344524464af71296f2140a7b37" kind="define" prot="public" static="no">
        <name>CY_RSLT_TYPE_ERROR</name>
        <initializer>(2U)</initializer>
        <briefdescription>
<para>The result code is an error. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="144" column="9" file="output/libs/core-lib/include/cy_result.h" line="144" />
      </memberdef>
      <memberdef id="group__group__result_1ga2bc4a48af22980d72766f825eda62658" kind="define" prot="public" static="no">
        <name>CY_RSLT_TYPE_FATAL</name>
        <initializer>(3U)</initializer>
        <briefdescription>
<para>The result code is a fatal error. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="146" column="9" file="output/libs/core-lib/include/cy_result.h" line="146" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="user-defined">
      <header>Modules</header>
      <description><para><anchor id="group__group__result_1anchor_modules" />Defines codes to identify the module from which an error originated. For some large libraries, a range of module codes is defined here; see the library documentation for values corresponding to individual modules. Valid range is 0x0000-0x4000. </para></description>
      <memberdef id="group__group__result_1gaf229ad57884c3c8e4c5ecc7f217ce8aa" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_DRIVERS_PDL_BASE</name>
        <initializer>(0x0000U)</initializer>
        <briefdescription>
<para>Base module identifier for peripheral driver library drivers (0x0000 - 0x007F) </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="161" column="9" file="output/libs/core-lib/include/cy_result.h" line="161" />
      </memberdef>
      <memberdef id="group__group__result_1ga328dd4831cad3621a05e3fad151f9cd5" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_DRIVERS_WHD_BASE</name>
        <initializer>(0x0080U)</initializer>
        <briefdescription>
<para>Base module identifier for wireless host driver library modules (0x0080 - 0x00FF) </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="163" column="9" file="output/libs/core-lib/include/cy_result.h" line="163" />
      </memberdef>
      <memberdef id="group__group__result_1gaba8505defe4e04a5927b6e0590e256f0" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_ABSTRACTION_HAL_BASE</name>
        <initializer>(0x0100U)</initializer>
        <briefdescription>
<para>Deprecated. </para>        </briefdescription>
        <detaileddescription>
<para>Use <ref kindref="member" refid="group__group__result_1gada155710418c5225c8ccaa8e75e9a577">CY_RSLT_MODULE_ABSTRACTION_HAL</ref> </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="166" column="9" file="output/libs/core-lib/include/cy_result.h" line="166" />
      </memberdef>
      <memberdef id="group__group__result_1gada155710418c5225c8ccaa8e75e9a577" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_ABSTRACTION_HAL</name>
        <initializer>(0x0100U)</initializer>
        <briefdescription>
<para>Module identifier for the Hardware Abstraction Layer. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="168" column="9" file="output/libs/core-lib/include/cy_result.h" line="168" />
      </memberdef>
      <memberdef id="group__group__result_1ga4f6360a8bf5a570d6927d50123e95c1d" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_ABSTRACTION_BSP</name>
        <initializer>(0x0180U)</initializer>
        <briefdescription>
<para>Module identifier for board support package. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="170" column="9" file="output/libs/core-lib/include/cy_result.h" line="170" />
      </memberdef>
      <memberdef id="group__group__result_1ga9cde2c478cdd2b7f626bc45c2a4a0543" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_ABSTRACTION_FS</name>
        <initializer>(0x0181U)</initializer>
        <briefdescription>
<para>Module identifier for file system abstraction. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="172" column="9" file="output/libs/core-lib/include/cy_result.h" line="172" />
      </memberdef>
      <memberdef id="group__group__result_1gab757b71680be515dd43368895fa73b7d" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_ABSTRACTION_RESOURCE</name>
        <initializer>(0x0182U)</initializer>
        <briefdescription>
<para>Module identifier for resource abstraction. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="174" column="9" file="output/libs/core-lib/include/cy_result.h" line="174" />
      </memberdef>
      <memberdef id="group__group__result_1ga5a59f041b014217d0d699bc3b0ae3cca" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_ABSTRACTION_OS</name>
        <initializer>(0x0183U)</initializer>
        <briefdescription>
<para>Module identifier for rtos abstraction. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="176" column="9" file="output/libs/core-lib/include/cy_result.h" line="176" />
      </memberdef>
      <memberdef id="group__group__result_1gad1c76e11af277ca324da1c8516d7d31a" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_ABSTRACTION_ENV</name>
        <initializer>(0x0184U)</initializer>
        <briefdescription>
<para>Base identifier for environment abstraction modules (0x0184 - 0x01FF) </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="178" column="9" file="output/libs/core-lib/include/cy_result.h" line="178" />
      </memberdef>
      <memberdef id="group__group__result_1ga809314d87ef1e8f175564e88ccef70a9" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_LIB_BASE</name>
        <initializer>(0x01A0U)</initializer>
        <briefdescription>
<para>Base module identifier for Board Libraries (0x01A0 - 0x01BF) </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="181" column="9" file="output/libs/core-lib/include/cy_result.h" line="181" />
      </memberdef>
      <memberdef id="group__group__result_1ga2424e0be0c1e672c829fb3798720465f" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_LIB_RETARGET_IO</name>
        <initializer>(0x1A0U)</initializer>
        <briefdescription>
<para>Module identifier for the Retarget IO Board Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="183" column="9" file="output/libs/core-lib/include/cy_result.h" line="183" />
      </memberdef>
      <memberdef id="group__group__result_1gaea80fb321cb0b3e298a3a2e90e445d3d" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_LIB_RGB_LED</name>
        <initializer>(0x01A1U)</initializer>
        <briefdescription>
<para>Module identifier for the RGB LED Board Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="185" column="9" file="output/libs/core-lib/include/cy_result.h" line="185" />
      </memberdef>
      <memberdef id="group__group__result_1ga0ce9bf3fc06bf768cd32558b44aaaf0f" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_LIB_SERIAL_FLASH</name>
        <initializer>(0x01A2U)</initializer>
        <briefdescription>
<para>Module identifier for the Serial Flash Board Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="187" column="9" file="output/libs/core-lib/include/cy_result.h" line="187" />
      </memberdef>
      <memberdef id="group__group__result_1ga927ff40c1f2cbe51a3c5c3f52c533e57" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_LIB_WHD_INTEGRATION</name>
        <initializer>(0x01A3U)</initializer>
        <briefdescription>
<para>Module identifier for the WiFi Host Driver + Board Support Integration Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="189" column="9" file="output/libs/core-lib/include/cy_result.h" line="189" />
      </memberdef>
      <memberdef id="group__group__result_1ga76bcb30b36e7fea0c8165c55e561de73" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_SHIELD_BASE</name>
        <initializer>(0x01B8U)</initializer>
        <briefdescription>
<para>Base module identifier for Shield Board Libraries (0x01B8 - 0x01BF) </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="192" column="9" file="output/libs/core-lib/include/cy_result.h" line="192" />
      </memberdef>
      <memberdef id="group__group__result_1gaa5b8e84947e9bf1a7fb5d8003ef276bf" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_SHIELD_028_EPD</name>
        <initializer>(0x01B8U)</initializer>
        <briefdescription>
<para>Module identifier for Shield Board CY8CKIT-028-EPD. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="194" column="9" file="output/libs/core-lib/include/cy_result.h" line="194" />
      </memberdef>
      <memberdef id="group__group__result_1ga40e8ea560bec96d98a8397308ce773cd" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_SHIELD_028_TFT</name>
        <initializer>(0x01B9U)</initializer>
        <briefdescription>
<para>Module identifier for Shield Board CY8CKIT-028-TFT. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="196" column="9" file="output/libs/core-lib/include/cy_result.h" line="196" />
      </memberdef>
      <memberdef id="group__group__result_1gad3e437a0d81d24329914409b890d05e9" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_SHIELD_032</name>
        <initializer>(0x01BAU)</initializer>
        <briefdescription>
<para>Module identifier for Shield Board CY8CKIT-032. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="198" column="9" file="output/libs/core-lib/include/cy_result.h" line="198" />
      </memberdef>
      <memberdef id="group__group__result_1ga57f83cdac42fe9fc2afbe82609a00d74" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_HARDWARE_BASE</name>
        <initializer>(0x01C0U)</initializer>
        <briefdescription>
<para>Base module identifier for Board Hardware Libraries (0x01C0 - 0x01FF) </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="201" column="9" file="output/libs/core-lib/include/cy_result.h" line="201" />
      </memberdef>
      <memberdef id="group__group__result_1ga19d6abe548162ac238afbc69ace1a2d4" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_HARDWARE_BMI160</name>
        <initializer>(0x01C0U)</initializer>
        <briefdescription>
<para>Module identifier for the BMI160 Motion Sensor Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="203" column="9" file="output/libs/core-lib/include/cy_result.h" line="203" />
      </memberdef>
      <memberdef id="group__group__result_1ga36f615a1ecf7bba46416720e0892c115" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_HARDWARE_E2271CS021</name>
        <initializer>(0x01C1U)</initializer>
        <briefdescription>
<para>Module identifier for the E2271CS021 E-Ink Controller Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="205" column="9" file="output/libs/core-lib/include/cy_result.h" line="205" />
      </memberdef>
      <memberdef id="group__group__result_1ga052784c9e3c64b33e5d056aa1db57c59" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_HARDWARE_THERMISTOR</name>
        <initializer>(0x01C2U)</initializer>
        <briefdescription>
<para>Module identifier for the NTC GPIO Thermistor Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="207" column="9" file="output/libs/core-lib/include/cy_result.h" line="207" />
      </memberdef>
      <memberdef id="group__group__result_1ga9c77295c2fb9f887a857b8b63810f18d" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_HARDWARE_SSD1306</name>
        <initializer>(0x01C3U)</initializer>
        <briefdescription>
<para>Module identifier for the SSD1306 OLED Controller Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="209" column="9" file="output/libs/core-lib/include/cy_result.h" line="209" />
      </memberdef>
      <memberdef id="group__group__result_1ga646b094bb857dcf1bdf21448224f8b7a" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_HARDWARE_ST7789V</name>
        <initializer>(0x01C4U)</initializer>
        <briefdescription>
<para>Module identifier for the ST7789V TFT Controller Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="211" column="9" file="output/libs/core-lib/include/cy_result.h" line="211" />
      </memberdef>
      <memberdef id="group__group__result_1ga929b1c06f7aa2a85ea6e3c317c3d5cf6" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_HARDWARE_LIGHT_SENSOR</name>
        <initializer>(0x01C5U)</initializer>
        <briefdescription>
<para>Module identifier for the Light Sensor Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="213" column="9" file="output/libs/core-lib/include/cy_result.h" line="213" />
      </memberdef>
      <memberdef id="group__group__result_1ga3cb51b341c4dac2984d76e0ab00056bb" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_BOARD_HARDWARE_AK4954A</name>
        <initializer>(0x01C6U)</initializer>
        <briefdescription>
<para>Module identifier for the AK4954A Audio Codec Library. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="215" column="9" file="output/libs/core-lib/include/cy_result.h" line="215" />
      </memberdef>
      <memberdef id="group__group__result_1ga2db34d05e5194f919d6cc45957dc5047" kind="define" prot="public" static="no">
        <name>CY_RSLT_MODULE_MIDDLEWARE_BASE</name>
        <initializer>(0x0200U)</initializer>
        <briefdescription>
<para>Base module identifier for Middleware Libraries (0x0200 - 0x02FF) </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="218" column="9" file="output/libs/core-lib/include/cy_result.h" line="218" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="typedef">
      <memberdef id="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1" kind="typedef" prot="public" static="no">
        <type>uint32_t</type>
        <definition>typedef uint32_t cy_rslt_t</definition>
        <argsstring />
        <name>cy_rslt_t</name>
        <briefdescription>
<para>Provides the result of an operation as a structured bitfield. </para>        </briefdescription>
        <detaileddescription>
<para>See the <ref kindref="member" refid="group__group__result_1anchor_general_description">General Description</ref> for more details on structure and usage. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="65" column="1" file="output/libs/core-lib/include/cy_result.h" line="65" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="define">
      <memberdef id="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e" kind="define" prot="public" static="no">
        <name>CY_RSLT_SUCCESS</name>
        <initializer>((<ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref>)0x00000000U)</initializer>
        <briefdescription>
<para><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref> return value indicating success </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/core-lib/include/cy_result.h" bodystart="68" column="9" file="output/libs/core-lib/include/cy_result.h" line="68" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>Defines a type and related utilities for function result handling. </para>    </briefdescription>
    <detaileddescription>
<para><anchor id="group__group__result_1anchor_general_description" />The <ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref> type is a structured bitfield which encodes information about result type, the originating module, and a code for the specific error (or warning etc). In order to extract these individual fields from a <ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref> value, the utility macros <ref kindref="member" refid="group__group__result_1ga9ae9b152a38632a9e0f8620845c6cea9">CY_RSLT_GET_TYPE</ref>, <ref kindref="member" refid="group__group__result_1gab959fc67da8489c03d3ab631442393b4">CY_RSLT_GET_MODULE</ref>, and <ref kindref="member" refid="group__group__result_1ga5a7fc037210b7ff81c65160e11823443">CY_RSLT_GET_CODE</ref> are provided. For example: <programlisting><codeline><highlight class="normal">cy_rslt_t<sp />result<sp />=<sp />cy_hal_do_operation(arg);</highlight></codeline>
<codeline><highlight class="normal">//<sp />Will<sp />be<sp />CY_RSLT_TYPE_INFO,<sp />CY_RSLT_TYPE_WARNING,<sp />CY_RSLT_TYPE_ERROR,<sp />or<sp />CY_RSLT_TYPE_FATAL</highlight></codeline>
<codeline><highlight class="normal">uint8_t<sp />type<sp />=<sp />CY_RSLT_GET_TYPE(result)</highlight></codeline>
<codeline><highlight class="normal">//<sp />See<sp />the<sp />"Modules"<sp />section<sp />for<sp />possible<sp />values</highlight></codeline>
<codeline><highlight class="normal">uint16_t<sp />module_id<sp />=<sp />CY_RSLT_GET_MODULE(result);</highlight></codeline>
<codeline><highlight class="normal">//<sp />Specific<sp />error<sp />codes<sp />are<sp />defined<sp />by<sp />each<sp />module</highlight></codeline>
<codeline><highlight class="normal">uint16_t<sp />error_code<sp />=<sp />CY_RSLT_GET_CODE(result);</highlight></codeline>
</programlisting> </para>    </detaileddescription>
  </compounddef>
</doxygen>