===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_PSVP-Player28 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: