==============================
cy_stc_eeprom_config_t Struct
==============================

.. doxygenstruct:: cy_stc_eeprom_config_t
   :project: emeeprom1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: