=================
Enumerated Types
=================

.. doxygengroup:: group_em_eeprom_enums
   :project: emeeprom1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: