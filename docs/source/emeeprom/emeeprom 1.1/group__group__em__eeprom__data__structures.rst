================
Data Structures
================

.. doxygengroup:: group_em_eeprom_data_structures
   :project: emeeprom1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::
   :hidden:

   structcy__stc__eeprom__config__t.rst
   structcy__stc__eeprom__context__t.rst
   
