===============================
cy_stc_eeprom_context_t Struct
===============================
.. doxygenstruct:: cy_stc_eeprom_context_t
   :project: emeeprom1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: