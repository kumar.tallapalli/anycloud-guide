==============================
cy_stc_eeprom_config_t Struct
==============================

.. doxygenstruct:: cy_stc_eeprom_config_t
   :project: emeeprom1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: