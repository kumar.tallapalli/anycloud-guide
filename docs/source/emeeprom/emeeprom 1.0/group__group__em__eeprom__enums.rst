=================
Enumerated Types
=================

.. doxygengroup:: group_em_eeprom_enums
   :project: emeeprom1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: