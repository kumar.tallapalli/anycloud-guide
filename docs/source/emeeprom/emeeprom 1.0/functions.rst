=====================================================
Cypress Em_EEPROM Middleware Library 2.0: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress Em_EEPROM Middleware   |
      |                                   |    Library 2.0                    |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      -  blockingWrite :
         `cy_stc_eeprom_config_t <structcy__stc__eeprom__config__t.html#a8f6acdb1ccef76931219fdf19032188d>`__
         ,
         `cy_stc_eeprom_context_t <structcy__stc__eeprom__context__t.html#a662b2e5e86183706701aa79280dca36d>`__
      -  byteInRow :
         `cy_stc_eeprom_context_t <structcy__stc__eeprom__context__t.html#a07b2a035a1c31b8f4ea6c084e92e0bde>`__
      -  eepromSize :
         `cy_stc_eeprom_config_t <structcy__stc__eeprom__config__t.html#ad41b3c5ce4619d484a648a5a4bcc0bad>`__
         ,
         `cy_stc_eeprom_context_t <structcy__stc__eeprom__context__t.html#a1136c1032e20149af06f0763849211d9>`__
      -  numberOfRows :
         `cy_stc_eeprom_context_t <structcy__stc__eeprom__context__t.html#a44b3f562448cbc33536f520c7eeb6976>`__
      -  ptrLastWrittenRow :
         `cy_stc_eeprom_context_t <structcy__stc__eeprom__context__t.html#a29be33dbb9a3ea5e901c1b912b34f443>`__
      -  redundantCopy :
         `cy_stc_eeprom_config_t <structcy__stc__eeprom__config__t.html#ad6d96546ec661d1600f92bbeb9f3fb5d>`__
         ,
         `cy_stc_eeprom_context_t <structcy__stc__eeprom__context__t.html#a9930dd5410628e79f68927cd30bca6d3>`__
      -  simpleMode :
         `cy_stc_eeprom_config_t <structcy__stc__eeprom__config__t.html#a69bd4219df4e0b5563332c8b271cfa73>`__
         ,
         `cy_stc_eeprom_context_t <structcy__stc__eeprom__context__t.html#a00649073998b271aa173eab5345878e4>`__
      -  userFlashStartAddr :
         `cy_stc_eeprom_config_t <structcy__stc__eeprom__config__t.html#a686ebc626fadcdce3ffc40406f707f49>`__
         ,
         `cy_stc_eeprom_context_t <structcy__stc__eeprom__context__t.html#aa3890fbd2cd8b72c6027b3ec6b569aa6>`__
      -  wearLevelingFactor :
         `cy_stc_eeprom_config_t <structcy__stc__eeprom__config__t.html#a19b32e2183e33f98f02e15e2bce4f207>`__
         ,
         `cy_stc_eeprom_context_t <structcy__stc__eeprom__context__t.html#a0c81c6ec6896b078744a0ae06e9925d0>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
