=========
LED Pins
=========

.. doxygengroup:: group_bsp_pins_led
   :project: TARGET_CY8CKIT-062-BLE 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   