===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CY8CPROTO-062S3-4343W 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: