=============
Pin Mappings
=============

.. doxygengroup:: group_bsp_pins
   :project: TARGET_CY8CPROTO-062S3-4343W 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
--------------   

.. toctree::

   group__group__bsp__pins__led.rst
   group__group__bsp__pins__btn.rst
   group__group__bsp__pins__comm.rst

 
 
