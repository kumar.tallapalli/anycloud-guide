===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CY8CKIT-064S0S2-4343W 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: