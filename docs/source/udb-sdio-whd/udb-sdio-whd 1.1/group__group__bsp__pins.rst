=============
Pin Mappings
=============
.. doxygengroup:: group_bsp_pins
   :project: udb-sdio-whd 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: