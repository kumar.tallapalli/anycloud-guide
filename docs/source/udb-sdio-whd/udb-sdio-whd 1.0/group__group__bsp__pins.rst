=============
Pin Mappings
=============
.. doxygengroup:: group_bsp_pins
   :project: udb-sdio-whd 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: