=======
Macros
=======

.. doxygengroup:: group_bsp_macros
   :project: udb-sdio-whd 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: