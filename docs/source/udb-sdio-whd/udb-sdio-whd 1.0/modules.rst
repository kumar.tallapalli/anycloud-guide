==============
API Reference
==============

.. raw:: html

   <hr>

The following provides a list of API documentation

.. toctree::
   :hidden:

   group__group__bsp__pin__state.rst
   group__group__bsp__pins.rst
   group__group__bsp__macros.rst
   group__group__bsp__functions.rst
   group__group__udb__sdio.rst

+----------------------------------------------------+----------------------------------+
| \ `Pin State                                       |                                  |
| <group__group__bsp__pin__state.html>`_             |                                  |
+----------------------------------------------------+----------------------------------+
| \ `Pin Mappings                                    |                                  |
| <group__group__bsp__pins.html>`_                   |                                  |
+----------------------------------------------------+----------------------------------+
| \ `Macros                                          |                                  |
| <gr oup__group__bsp__macros.html>`_                |                                  |
+----------------------------------------------------+----------------------------------+
| \ `Functions                                       |                                  |
| <group__group__bsp__functions.html>`_              |                                  |
+----------------------------------------------------+----------------------------------+
| \ `UDB_SDIO                                        | SDIO - Secure Digital Input      |
| <group__group__udb__sdio.html>`_                   | Output is a standard for         |
|                                                    | communicating with various       |
|                                                    | external devices such as Wifi    |
|                                                    | and bluetooth devices            |
+----------------------------------------------------+----------------------------------+
| \ `UDB_SDIO Macros                                 |                                  |
| <group__g roup__udb__sdio__macros.html>`_          |                                  |
+----------------------------------------------------+----------------------------------+
| \ `UDB_SDIO Functions                              |                                  |
| <group__group__udb__sdio__functions.html>`_        |                                  |
+----------------------------------------------------+----------------------------------+
| \ `UDB_SDIO Data Structures                        |                                  |
| <group__group__udb__sdio__data__structures.html>`_ |                                  |
|                                                    |                                  |
+----------------------------------------------------+----------------------------------+
 

