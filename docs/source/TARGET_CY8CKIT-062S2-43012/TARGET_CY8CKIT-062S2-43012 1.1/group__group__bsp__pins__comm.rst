===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CY8CKIT-062S2-43012 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: