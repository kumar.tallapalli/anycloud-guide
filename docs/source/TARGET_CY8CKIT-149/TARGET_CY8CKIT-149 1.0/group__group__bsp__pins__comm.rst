===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CY8CKIT-149 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: