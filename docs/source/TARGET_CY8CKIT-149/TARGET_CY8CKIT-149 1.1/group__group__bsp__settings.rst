=============
BSP Settings
=============

.. doxygengroup:: group_bsp_settings
   :project: TARGET_CY8CKIT-149 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: