=============
BSP Settings
=============

.. doxygengroup:: group_bsp_settings
   :project: TARGET_PSVP-PSOC4AS4 1.1
   :members:
   :protected-members:
   :private-members:
   :undoc-members: