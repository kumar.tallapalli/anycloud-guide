=======
Macros
=======
.. doxygengroup:: group_bsp_macros
   :project: TARGET_PSVP-PSOC4AS4 1.0
   :members:
   :protected-members:
   :private-members:
   :undoc-members: