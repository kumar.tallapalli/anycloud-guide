

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import xml.etree.ElementTree as ET
from os import path
import json


# -- Project information -----------------------------------------------------

project = 'AnyCloud Guide'
copyright = '2020, Infineon'
author = 'Infineon'

# The full version, including alpha/beta/rc tags
release = 'rev0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['breathe','sphinx.ext.mathjax']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

breathe_projects = {'anycloud-ota 1.0' : 'anycloud-ota/anycloud-ota 1.0/xml/', 'anycloud-ota 1.1' : 'anycloud-ota/anycloud-ota 1.1/xml/', 'capsense1.0' : 'capsense/capsense 1.0/xml/','capsense1.1' : 'capsense/capsense 1.1/xml/','csdadc1.0' : 'csdadc/csdadc 1.0/xml/','csdadc1.1' : 'csdadc/csdadc 1.1/xml/', 'csdidac1.0' : 'csdidac/csdidac 1.0/xml/','csdidac1.1' : 'csdidac/csdidac 1.1/xml/', 'dfu1.0' : 'dfu/dfu 1.0/xml/','dfu1.1' : 'dfu/dfu 1.1/xml/','emeeprom1.0' : 'emeeprom/emeeprom 1.0/xml/','emeeprom1.1' : 'emeeprom/emeeprom 1.1/xml/','lpa 1.0' : 'lpa/lpa 1.0/xml/','lpa 1.1' : 'lpa/lpa 1.1/xml/','mqtt 1.0' : 'mqtt/mqtt 1.0/xml/','mqtt 1.1' : 'mqtt/mqtt 1.1/xml/','secure-sockets 1.0' : 'secure-sockets/secure-sockets 1.0/xml/','secure-sockets 1.1' : 'secure-sockets/secure-sockets 1.1/xml/', 'sensor-atmo-bme680 1.0' : 'sensor-atmo-bme680/sensor-atmo-bme680 1.0/xml/', 'sensor-atmo-bme680 1.1' : 'sensor-atmo-bme680/sensor-atmo-bme680 1.1/xml/', 'sensor-motion-bmi1601.0' : 'sensor-motion-bmi160/sensor-motion-bmi160 1.0/xml/',
'sensor-motion-bmi1601.1' : 'sensor-motion-bmi160/sensor-motion-bmi160 1.1/xml/','TARGET_CY8CPROTO-064S1-SB 1.0' : 'TARGET_CY8CPROTO-064S1-SB/TARGET_CY8CPROTO-064S1-SB 1.0/xml/', 'TARGET_CY8CPROTO-064S1-SB 1.1' : 'TARGET_CY8CPROTO-064S1-SB/TARGET_CY8CPROTO-064S1-SB 1.1/xml/',  'TARGET_CYBLE-416045-EVAL 1.0' : 'TARGET_CYBLE-416045-EVAL/TARGET_CYBLE-416045-EVAL 1.0/xml/', 'TARGET_CYBLE-416045-EVAL 1.1' : 'TARGET_CYBLE-416045-EVAL/TARGET_CYBLE-416045-EVAL 1.1/xml/',
 'TARGET_CYW9P62S1-43012EVB-01 1.0' : 'TARGET_CYW9P62S1-43012EVB-01/TARGET_CYW9P62S1-43012EVB-01 1.0/xml/','TARGET_CYW9P62S1-43012EVB-01 1.1' : 'TARGET_CYW9P62S1-43012EVB-01/TARGET_CYW9P62S1-43012EVB-01 1.1/xml/','TARGET_CYW9P62S1-43438EVB-01 1.0' : 'TARGET_CYW9P62S1-43438EVB-01/TARGET_CYW9P62S1-43438EVB-01 1.0/xml/' , 'TARGET_CYW9P62S1-43438EVB-01 1.1' : 'TARGET_CYW9P62S1-43438EVB-01/TARGET_CYW9P62S1-43438EVB-01 1.1/xml/' , 
 'TARGET_PSOC4-GENERIC 1.0' : 'TARGET_PSOC4-GENERIC/TARGET_PSOC4-GENERIC 1.0/xml/','TARGET_PSOC4-GENERIC 1.1' : 'TARGET_PSOC4-GENERIC/TARGET_PSOC4-GENERIC 1.1/xml/',
 'TARGET_PSOC6-GENERIC 1.0' : 'TARGET_PSOC6-GENERIC/TARGET_PSOC6-GENERIC 1.0/xml/','TARGET_PSOC6-GENERIC 1.1' : 'TARGET_PSOC6-GENERIC/TARGET_PSOC6-GENERIC 1.1/xml/', 'TARGET_PSVP-M33X1-F256K 1.0' : 'TARGET_PSVP-M33X1-F256K/TARGET_PSVP-M33X1-F256K 1.0/xml/' , 'TARGET_PSVP-M33X1-F256K 1.1' : 'TARGET_PSVP-M33X1-F256K/TARGET_PSVP-M33X1-F256K 1.1/xml/' ,
 'TARGET_PSVP-Player28 1.0' : 'TARGET_PSVP-Player28/TARGET_PSVP-Player28 1.0/xml/' ,'TARGET_PSVP-Player28 1.1' : 'TARGET_PSVP-Player28/TARGET_PSVP-Player28 1.1/xml/' ,
 'TARGET_PSVP-PSOC4AS4 1.0' : 'TARGET_PSVP-PSOC4AS4/TARGET_PSVP-PSOC4AS4 1.0/xml/','TARGET_PSVP-PSOC4AS4 1.1' : 'TARGET_PSVP-PSOC4AS4/TARGET_PSVP-PSOC4AS4 1.1/xml/','TARGET_TEST_CYSDIO_PORT_9 1.0' : 'TARGET_TEST_CYSDIO_PORT_9/TARGET_TEST_CYSDIO_PORT_9 1.0/xml/' ,'TARGET_TEST_CYSDIO_PORT_9 1.1' : 'TARGET_TEST_CYSDIO_PORT_9/TARGET_TEST_CYSDIO_PORT_9 1.1/xml/' ,'thermistor 1.0' : 'thermistor/thermistor 1.0/xml/', 'thermistor 1.1' : 'thermistor/thermistor 1.1/xml/',
 'udb-sdio-whd 1.0' : 'udb-sdio-whd/udb-sdio-whd 1.0/xml/', 'udb-sdio-whd 1.1' : 'udb-sdio-whd/udb-sdio-whd 1.1/xml/', 'usbdev1.0' : 'usbdev/usbdev 1.0/xml/','usbdev1.1' : 'usbdev/usbdev 1.1/xml/',  'whd-bsp-integration 1.0' : 'whd-bsp-integration/whd-bsp-integration 1.0/xml/', 'whd-bsp-integration 1.1' : 'whd-bsp-integration/whd-bsp-integration 1.1/xml/', 
 'wifi-connection-manager 1.0' : 'wifi-connection-manager/wifi-connection-manager 1.0/xml/','wifi-connection-manager 1.1' : 'wifi-connection-manager/wifi-connection-manager 1.1/xml/','serial-flash1.0' : 'serial-flash/serial-flash 1.0/xml/', 'serial-flash1.1' : 'serial-flash/serial-flash 1.1/xml/','TARGET_CY8C4548AZI-S485 1.0' : 'TARGET_CY8C4548AZI-S485/TARGET_CY8C4548AZI-S485 1.0/xml/','TARGET_CY8C4548AZI-S485 1.1' : 'TARGET_CY8C4548AZI-S485/TARGET_CY8C4548AZI-S485 1.1/xml/',
 'TARGET_CY8CKIT-041-40XX 1.0' : 'TARGET_CY8CKIT-041-40XX/TARGET_CY8CKIT-041-40XX 1.0/xml/','TARGET_CY8CKIT-041-40XX 1.1' : 'TARGET_CY8CKIT-041-40XX/TARGET_CY8CKIT-041-40XX 1.1/xml/', 'TARGET_CY8CKIT-041-41XX 1.0' : 'TARGET_CY8CKIT-041-41XX/TARGET_CY8CKIT-041-41XX 1.0/xml/','TARGET_CY8CKIT-041-41XX 1.1' : 'TARGET_CY8CKIT-041-41XX/TARGET_CY8CKIT-041-41XX 1.1/xml/','TARGET_CY8CKIT-062-BLE 1.0' : 'TARGET_CY8CKIT-062-BLE/TARGET_CY8CKIT-062-BLE 1.0/xml/','TARGET_CY8CKIT-062-BLE 1.1' : 'TARGET_CY8CKIT-062-BLE/TARGET_CY8CKIT-062-BLE 1.1/xml/',
 'TARGET_CY8CKIT-062S2-43012 1.0' : 'TARGET_CY8CKIT-062S2-43012/TARGET_CY8CKIT-062S2-43012 1.0/xml/','TARGET_CY8CKIT-062S2-43012 1.1' : 'TARGET_CY8CKIT-062S2-43012/TARGET_CY8CKIT-062S2-43012 1.1/xml/', 'TARGET_CY8CKIT-062S4 1.0' : 'TARGET_CY8CKIT-062S4/TARGET_CY8CKIT-062S4 1.0/xml/', 'TARGET_CY8CKIT-062S4 1.1' : 'TARGET_CY8CKIT-062S4/TARGET_CY8CKIT-062S4 1.1/xml/',
 'TARGET_CY8CKIT-062-WIFI-BT 1.0' : 'TARGET_CY8CKIT-062-WIFI-BT/TARGET_CY8CKIT-062-WIFI-BT 1.0/xml/','TARGET_CY8CKIT-062-WIFI-BT 1.1' : 'TARGET_CY8CKIT-062-WIFI-BT/TARGET_CY8CKIT-062-WIFI-BT 1.1/xml/', 'TARGET_CY8CKIT-064B0S2-4343W 1.0' : 'TARGET_CY8CKIT-064B0S2-4343W/TARGET_CY8CKIT-064B0S2-4343W 1.0/xml/',  'TARGET_CY8CKIT-064B0S2-4343W 1.1' : 'TARGET_CY8CKIT-064B0S2-4343W/TARGET_CY8CKIT-064B0S2-4343W 1.1/xml/', 
 'TARGET_CY8CKIT-064S0S2-4343W 1.0' : 'TARGET_CY8CKIT-064S0S2-4343W/TARGET_CY8CKIT-064S0S2-4343W 1.0/xml/','TARGET_CY8CKIT-064S0S2-4343W 1.1' : 'TARGET_CY8CKIT-064S0S2-4343W/TARGET_CY8CKIT-064S0S2-4343W 1.1/xml/','TARGET_CY8CKIT-145-40XX 1.0' : 'TARGET_CY8CKIT-145-40XX/TARGET_CY8CKIT-145-40XX 1.0/xml/','TARGET_CY8CKIT-145-40XX 1.1' : 'TARGET_CY8CKIT-145-40XX/TARGET_CY8CKIT-145-40XX 1.1/xml/', 'TARGET_CY8CKIT-149 1.0' : 'TARGET_CY8CKIT-149/TARGET_CY8CKIT-149 1.0/xml/','TARGET_CY8CKIT-149 1.1' : 'TARGET_CY8CKIT-149/TARGET_CY8CKIT-149 1.1/xml/',
 'TARGET_CY8CPROTO-062-4343W 1.0' : 'TARGET_CY8CPROTO-062-4343W/TARGET_CY8CPROTO-062-4343W 1.0/xml/','TARGET_CY8CPROTO-062-4343W 1.1' : 'TARGET_CY8CPROTO-062-4343W/TARGET_CY8CPROTO-062-4343W 1.1/xml/',  'TARGET_CY8CPROTO-062S3-4343W 1.0' : 'TARGET_CY8CPROTO-062S3-4343W/TARGET_CY8CPROTO-062S3-4343W 1.0/xml/', 'TARGET_CY8CPROTO-062S3-4343W 1.1' : 'TARGET_CY8CPROTO-062S3-4343W/TARGET_CY8CPROTO-062S3-4343W 1.1/xml/', 
 'TARGET_CY8CPROTO-063-BLE 1.0' : 'TARGET_CY8CPROTO-063-BLE/TARGET_CY8CPROTO-063-BLE 1.0/xml/','TARGET_CY8CPROTO-063-BLE 1.1' : 'TARGET_CY8CPROTO-063-BLE/TARGET_CY8CPROTO-063-BLE 1.1/xml/',
 'TARGET_CY8CPROTO-064B0S1-BLE 1.0' : 'TARGET_CY8CPROTO-064B0S1-BLE/TARGET_CY8CPROTO-064B0S1-BLE 1.0/xml/','TARGET_CY8CPROTO-064B0S1-BLE 1.1' : 'TARGET_CY8CPROTO-064B0S1-BLE/TARGET_CY8CPROTO-064B0S1-BLE 1.1/xml/','TARGET_CY8CPROTO-064B0S3 1.0' : 'TARGET_CY8CPROTO-064B0S3/TARGET_CY8CPROTO-064B0S3 1.0/xml/', 'TARGET_CY8CPROTO-064B0S3 1.1' : 'TARGET_CY8CPROTO-064B0S3/TARGET_CY8CPROTO-064B0S3 1.1/xml/', 'core-lib' : 'core-lib/xml/', 
 'pdl1.0' : 'pdl/pdl 1.0/xml/','CY8CKIT-028-EPD 1.0' : 'CY8CKIT-028-EPD/CY8CKIT-028-EPD 1.0/xml/','CY8CKIT-028-EPD 1.1' : 'CY8CKIT-028-EPD/CY8CKIT-028-EPD 1.1/xml/','display-eink-e2271cs021' : 'CY8CKIT-028-EPD/CY8CKIT-028-EPD 1.0/display-eink-e2271cs021/xml/','display-eink-e2271cs021 1.1' : 'CY8CKIT-028-EPD/CY8CKIT-028-EPD 1.1/display-eink-e2271cs021/xml/','rgb-led1.0' : 'rgb-led/rgb-led 1.0/xml/','rgb-led1.1' : 'rgb-led/rgb-led 1.1/xml/',
 'CY8CKIT-028-TFT 1.0' : 'CY8CKIT-028-TFT/CY8CKIT-028-TFT 1.0/xml/', 'CY8CKIT-028-TFT 1.1' : 'CY8CKIT-028-TFT/CY8CKIT-028-TFT 1.1/xml/', 'CY8CKIT-032 1.0' : 'CY8CKIT-032/CY8CKIT-032 1.0/xml/','CY8CKIT-032 1.1' : 'CY8CKIT-032/CY8CKIT-032 1.1/xml/','clib-support1.0' : 'clib-support/clib-support 1.0/xml/' ,'clib-support1.1' : 'clib-support/clib-support 1.1/xml/','emwin1.0' : 'emwin/emwin 1.0/xml/','emwin1.1' : 'emwin/emwin 1.1/xml/',
 'display-tft-st7789v' : 'CY8CKIT-028-TFT/CY8CKIT-028-TFT 1.0/display-tft-st7789v/xml/','display-tft-st7789v 1.1' : 'CY8CKIT-028-TFT/CY8CKIT-028-TFT 1.1/display-tft-st7789v/xml/', 'sensor-light' : 'CY8CKIT-028-TFT/CY8CKIT-028-TFT 1.0/sensor-light/xml/', 'sensor-light 1.1' : 'CY8CKIT-028-TFT/CY8CKIT-028-TFT 1.1/sensor-light/xml/', 
 'audio-codec-ak4954a' : 'CY8CKIT-028-TFT/CY8CKIT-028-TFT 1.0/audio-codec-ak4954a/xml/','audio-codec-ak4954a 1.1' : 'CY8CKIT-028-TFT/CY8CKIT-028-TFT 1.1/audio-codec-ak4954a/xml/',
 'wifi-mw-core 1.0' : 'wifi-mw-core/wifi-mw-core 1.0/xml/','wifi-mw-core 1.1' : 'wifi-mw-core/wifi-mw-core 1.1/xml/' }

html_theme_options = {
    'display_version': False,
    'prev_next_buttons_location': 'bottom',
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 6,
    'includehidden': True,
    'titles_only': False
}

breathe_default_project = 'capsense'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_theme_path = ['_themes']

html_logo = '_static/image/IFX_LOGO_RGB.svg'

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = '_static/image/IFX_ICON.ico'

def setup(app):
#    app.add_css_file('css/custom.css')
    app.add_css_file('css/theme_overrides.css')
    app.add_js_file('js/custom.js')
    app.add_js_file('js/searchtools.js')
    app.add_js_file('js/hotjar.js')
    app.add_js_file('js/sajari.js')



htmlNamesForXmlString='{"row":"tr","entry":"td", "para":"p", "listitem":"li", "orderedlist":"ol", "itemizedlist":"ul", "ulink":"a", "ref":"a", "linebreak":"br", "bold":"b","emphasis":"i","image":"img"}'
htmlNamesForXml = json.loads(htmlNamesForXmlString)
totalDirectiveList=['seealso', 'attention', 'caution', 'danger', 'error', 'hint', 'important', 'note', 'tip', 'warning', 'admonition', 'pre']
for (k, filePath) in breathe_projects.items():
  files = os.listdir(filePath)
  for nameOfFile in files:
    if nameOfFile.endswith(".xml"):
#      print(filePath)
      fileName=filePath+nameOfFile
      tempfilename=filePath+'temp'+nameOfFile
#      print(fileName)
#      print(path.exists(fileName))
      ulinkArray=[]
      titleArray=[]
      if path.exists(fileName):
        with open(fileName, encoding='UTF-8') as f:
          tree = ET.parse(f)
          root = tree.getroot()
          
#          print(ET.tostring(root, encoding='utf8').decode('utf8'))
          for elem in root.getiterator():
            if ((elem.tag == 'compounddef' or elem.tag == 'compound') and elem.attrib['kind']!=None and elem.attrib['kind'] == 'page'):
              elem.attrib['kind'] = 'group'
            try:
                                
              if elem.tag == 'image':
                if elem.attrib['type']!= None and elem.attrib['type'] == 'latex':
                  elem.attrib.pop('type')
                  elem.attrib.pop('name')
                  elem.tag = elem.tag.replace(elem.tag,'removeimage')
                  
              if 'memberdef' == elem.tag and elem.attrib['kind'] == 'enum':
                nameElem = elem.find('name')
                briefDescElem = elem.find('briefdescription')
                if(briefDescElem!=None):
                  nameText = str(nameElem.text).strip()
                  paraElem = briefDescElem.find("para")
                  paraElemString = ET.tostring(paraElem, encoding='utf8').decode('utf8')
                  paraElemString = paraElemString.replace("<?xml version='1.0' encoding='utf8'?>","")
                  paraElemString = paraElemString.replace("<para>", "").replace("</para>", "").strip()
                  
                  if not paraElemString.startswith("<bold>"+nameText):
                    paraElemString = "<para><bold>"+nameText+": </bold><linebreak/>"+paraElemString+"</para>"
                  else:
                    paraElemString = "<para>"+paraElemString+"</para>"
                
                  briefDescElem.remove(paraElem)
                  briefDescElem.append(ET.fromstring(paraElemString))
              
              
#              if 'memberdef' == elem.tag and elem.attrib['kind'] == 'define':
#                nameElem = elem.find('name')
#                initElem = elem.find('initializer')
#                briefDescElem = elem.find('briefdescription')
#                if(briefDescElem!=None):
#                  nameText = str(nameElem.text).strip()
#                  paraElem = briefDescElem.find("para")
#                  paraElemString = ET.tostring(paraElem, encoding='utf8').decode('utf8')
#                  paraElemString = paraElemString.replace("<?xml version='1.0' encoding='utf8'?>","")
#                  paraElemString = paraElemString.replace("<para>", "").replace("</para>", "").strip()
                  
#                  if not paraElemString.startswith("<bold>"+nameText):
#                    initElemString =  ET.tostring(initElem, encoding='utf8').decode('utf8')
#                    initElemString = initElemString.replace("<?xml version='1.0' encoding='utf8'?>","")
#                    initElemString = initElemString.replace("<initializer>", "").replace("</initializer>", "").strip()
#                    paraElemString = "<para><bold>"+nameText+" "+initElemString+"</bold><linebreak/>"+paraElemString+"</para>"
#                  else:
#                    paraElemString = "<para>"+paraElemString+"</para>"
                
#                  print(paraElemString);
#                  briefDescElem.remove(paraElem)
#                  briefDescElem.append(ET.fromstring(paraElemString))

              if 'ulink' == elem.tag: 
                if elem.attrib['url']!= None:
#                  print(elem.attrib['url'])  
                  linkurl = elem.attrib["url"];
                  linkurlval = ''
                  if not (linkurl.startswith("http") or linkurl.startswith("www")):
                    if linkurl.endswith(".pdf") or linkurl.endswith(".txt") or linkurl.endswith(".py") or linkurl.endswith(".h"): 
                      linkurlvalarray = linkurl.split('/')
                      linkurlval = linkurlvalarray[len(linkurlvalarray)-1]
                      linkPath = filePath.replace("xml/", "")
                      linkPath = "../../_static/file/"+linkPath+linkurlval
                      elem.attrib["url"] = linkPath
                  
                  ulinkArray.append('<ulink url="'+elem.attrib["url"]+'">')

              if ((elem.tag).startswith("sect") and not (elem.tag).startswith('section')): 
                if elem.attrib['id']!= None:
                  elem.attrib.pop('id')
                
                elemchild = elem.find("title")
                if (elem.tag=='sect1'):
                  elemchild = elem.find("title")
                  text=elemchild.text
                  elemchild.text=""
                  heading = ET.SubElement(elemchild,'heading')
                  heading.text = text
                  heading.set('level','1')
                  elemchild.tag = elemchild.tag.replace('title', 'para')
                  
                else:
                  text=elemchild.text
                  elemchild.text=""
                  heading = ET.SubElement(elemchild,'bold')
                  heading.text = text
                  elemchild.tag = elemchild.tag.replace('title', 'para')

                elem.tag = elem.tag.replace(elem.tag,'removesec')

            except AttributeError:
              pass
              
#          print("before write")
          tree.write(tempfilename, encoding="utf-8")

        if path.exists(tempfilename):
          roottext=''    
          with open(tempfilename, encoding='UTF-8') as file:
            tree = ET.parse(file)
            root = tree.getroot()
            
            xmlstr = ET.tostring(root, encoding='utf8').decode('utf8')
            roottext = xmlstr.replace('<removesec>','').replace('</removesec>','')
            roottext = roottext.replace('<removeimage>','').replace('</removeimage>','').replace('<removeimage />','').replace('<removeimage/>','')
            for ulink in ulinkArray: 
              roottext = roottext.replace(ulink+"<bold>",ulink)

            roottext = roottext.replace('</bold></ulink>','</ulink>').replace('</bold> </ulink>','</ulink>').replace('</bold>\n</ulink>','</ulink>')
            roottext = roottext.replace('<computeroutput>','').replace('</computeroutput>','')
            roottext = roottext.replace('<ndash/>','-')
#            roottext = roottext.replace('<para><heading level="1">','<title>', 1).replace('</heading></para>','</title>', 1)
#            roottext = roottext.replace('<parahruler>','<para>').replace('</parahruler>','</para><hruler/>')

#          print(roottext)
        
          tree = ET.ElementTree(ET.fromstring(roottext))
          root = tree.getroot()
          
          
          for headingElem in root.iter('heading'):
            headingVal = int(headingElem.attrib['level'])
            if headingVal>2:
              headingElem.tag = headingElem.tag.replace(headingElem.tag,'bold') 
              headingElem.attrib.pop('level')
          
          for simSctElem in root.iter('simplesect'):
            if len(simSctElem.items()) > 0 and simSctElem.attrib['kind']!=None and simSctElem.attrib['kind'] in totalDirectiveList:
              admDirVal = simSctElem.attrib['kind']
              simSctElem.attrib.pop('kind')
              for elemchild in simSctElem.iter():
                if elemchild.tag  in htmlNamesForXml:
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,htmlNamesForXml[elemchild.tag]) 
#                  print(elemchild.items());    
                  for (attrname1, attval1) in elemchild.items():
                    if attrname1=='url':
                      elemchild.set('href',attval1)
                         
                    if attrname1=='refid': 
                      if path.exists(filePath+attval1+".xml"):
                        elemchild.set('href',attval1+'.html#')
                      else:
                        attval1.split('_')
                        x = attval1.split('_')
                        arr=[] 
                        count=0 
                        for val in x: 
                          if count<(len(x)-1):
                            arr.append(val) 
                          count=count+1 
                           
                        str1='_'.join(arr)
                        if path.exists(filePath+str1+".xml"):
                          elemchild.set('href',str1+'.html#'+attval1)
                        else:
                          elemchild.set('href','#'+attval1)
                        
                    elemchild.attrib.pop(attrname1)
                
                elif elemchild.tag=='programlisting':
                  elemchild.set('class','highlight-default notranslate')
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,'div') 
                  
                elif elemchild.tag=='codeline':
                  elemchild.set('class','highlight')
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,'div')

                elif elemchild.tag=='highlight':
                  elemchild.attrib.pop('class')
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,'pre')
                  
                elif elemchild.tag=='sp':
                  elemchild.tag = elemchild.tag.replace(elemchild.tag,'span')
                    
              simplesectString =  ET.tostring(simSctElem, encoding='utf8').decode('utf8')
              simplesectString = simplesectString.replace("<?xml version='1.0' encoding='utf8'?>","")
              
              if admDirVal=='pre':
                simplesectString = simplesectString.replace('<simplesect>','<div class="admonition-precondition admonition"><p class="admonition-title"><span>Precondition</span></p><p>').replace('</simplesect>','</p></div>')
              else:
                simplesectString = simplesectString.replace('<simplesect>','<div class="admonition '+admDirVal+'"><p class="admonition-title">'+admDirVal+'</p><p>').replace('</simplesect>','</p></div>')
                
              simplesectString = simplesectString.replace('\n','')
              simplesectString = 'embed:rst \n.. raw:: html\n\n  '+simplesectString
           
              simSctElem.clear()
              simSctElem.text=simplesectString
              simSctElem.tag = simSctElem.tag.replace('simplesect','verbatim')
              
            elif len(simSctElem.items()) > 0 and simSctElem.attrib['kind']!=None:
              simTitVal = simSctElem.attrib['kind']
              if simTitVal=='par':
                for simParaElem in simSctElem.iter('para'):
                  simParaElemStr =  ET.tostring(simParaElem, encoding='utf8').decode('utf8')
                  simParaElemStr = simParaElemStr.replace("<?xml version='1.0' encoding='utf8'?>","") 
                  isParachanged=False
                  if '<heading level="1">' in simParaElemStr :
                    simParaElemStr = simParaElemStr.replace('<para><heading level="1">','').replace('</heading></para>','')
                    isParachanged=True
                  if '<para><bold>' in simParaElemStr :
                    simParaElemStr = simParaElemStr.replace('<para><bold>','').replace('</bold></para>','')
                    isParachanged=True
                    
                  if isParachanged :
                    simParaElem.tag = simParaElem.tag.replace('para','title') 
                    simParaElem.clear()
                    simParaElem.text=simParaElemStr
           
          for tableElem in root.iter('table'):
#            print(tableElem.items())
            for (attrname, attval) in tableElem.items():
#              print(attrname)
              tableElem.attrib.pop(attrname)
            for rowElem in tableElem.iter():
              if rowElem.tag  in htmlNamesForXml:
                rowElem.tag = rowElem.tag.replace(rowElem.tag,htmlNamesForXml[rowElem.tag]) 
                
                for (attrname1, attval1) in rowElem.items():
                  if attrname1=='thead' and attval1.lower()=='yes':
                    rowElem.set('class','head')
                        
                  if attrname1=='url':
                    rowElem.set('href',attval1)

                  if attrname1=='name' and rowElem.tag=='img':
                    imagePath = filePath.replace("xml/", "")
                    imagePath = "../../_static/image/"+imagePath+attval1
                    rowElem.set('src',imagePath)
                    rowElem.set('alt',attval1)
                    rowElem.set('height','100%')
                    rowElem.text='helloimage'
                  
                  if attrname1=='refid': 
                    if path.exists(filePath+attval1+".xml"):
                        rowElem.set('href',attval1+'.html#')
                    else:
                      attval1.split('_')
                      x = attval1.split('_')
                      arr=[] 
                      count=0 
                      for val in x: 
                        if count<(len(x)-1):
                          arr.append(val) 
                        count=count+1 
                           
                      str1='_'.join(arr)
                      if path.exists(filePath+str1+".xml"):
                        rowElem.set('href',str1+'.html#'+attval1)
                      else:
                        rowElem.set('href','#'+attval1)
                      
                  if attrname1!='rowspan' and attrname1!='colspan': 
                    rowElem.attrib.pop(attrname1)
              
              elif rowElem.tag=='programlisting':
                rowElem.tag = rowElem.tag.replace(rowElem.tag,'div')
                rowElem.set('class','highlight-default notranslate')
                  
              elif rowElem.tag=='codeline':
                rowElem.tag = rowElem.tag.replace(rowElem.tag,'div')
                rowElem.set('class','highlight')

              elif rowElem.tag=='highlight':
                rowElem.attrib.pop('class')
                rowElem.tag = rowElem.tag.replace(rowElem.tag,'pre')
                  
              elif rowElem.tag=='sp':
                rowElem.tag = rowElem.tag.replace(rowElem.tag,'span')
              
            tablestr =  ET.tostring(tableElem, encoding='utf8').decode('utf8')
            tablestr = tablestr.replace("<?xml version='1.0' encoding='utf8'?>","") 
            tablestr = tablestr.replace('<table>','<table class="docutils align-default">')
            tablestr = tablestr.replace('\n','')
            tablestr = tablestr.replace('<td><p><img','<td width="25%"><img').replace('helloimage</img></p></td>', '</td>').replace('helloimage</img>   </p></td>', '</td>')
#            tablestr = tablestr.replace('<','\&lt;').replace('>','\&gt;')
            tablestr="embed:rst \n.. raw:: html\n\n  "+tablestr
        
            tableElem.tag = tableElem.tag.replace('table','verbatim') 
            tableElem.clear()
            tableElem.text=tablestr

          tree.write(fileName, encoding="utf-8")
          os.remove(tempfilename)
        
#      print("END Loop")