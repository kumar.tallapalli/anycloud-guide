==============
API Reference
==============

.. raw:: html

   <hr>

The following provides a list of API documentation

.. toctree::
   :hidden:

   group__group__board__libs__shield.rst
   group__group__board__libs__pins.rst
   group__group__board__libs__microphone.rst
   group__group__board__libs__codec.rst
   group__group__board__libs__light.rst
   group__group__board__libs__motion.rst
   group__group__board__libs__display.rst


+----------------------------------+----------------------------------+
|  \ `Shield <group__gro           | Basic set of APIs for            |
| up__board__libs__shield.html>`__ | interacting with the             |
|                                  | CY8CKIT-028-TFT shield board     |
+----------------------------------+----------------------------------+
|  \ `Pins <group__g               | Pin mapping of the GPIOs used by |
| roup__board__libs__pins.html>`__ | shield peripherals               |
+----------------------------------+----------------------------------+
|  \ `Microphone <group__group__   | The microphone uses the HAL      |
| board__libs__microphone.html>`__ | PDM_PCM driver for its           |
|                                  | initialization                   |
+----------------------------------+----------------------------------+
|  \ `Audio                        | The audio codec is handled by    |
| Codec <group__gr                 | the audio-codec-ak4954a library, |
| oup__board__libs__codec.html>`__ | details are available at         |
|                                  | https://github.com/cypresssem    |
|                                  | iconductorco/audio-codec-ak4954a |
+----------------------------------+----------------------------------+
|  \ `Light                        | The light sensor is handled by   |
| Sensor <group__gr                | the sensor-light library,        |
| oup__board__libs__light.html>`__ | details are available at         |
|                                  | https://github.com/cyp           |
|                                  | resssemiconductorco/sensor-light |
+----------------------------------+----------------------------------+
|  \ `Motion                       | The motion sensor is handled by  |
| Sensor <group__gro               | the sensor-motion-bmi160         |
| up__board__libs__motion.html>`__ | library, details are available   |
|                                  | at                               |
|                                  | https://github.com/cypresssemi   |
|                                  | conductorco/sensor-motion-bmi160 |
+----------------------------------+----------------------------------+
|  \ `TFT                          | The display is handled by the    |
| Display <group__grou             | display-tft-st7789v library,     |
| p__board__libs__display.html>`__ | details are available at         |
|                                  | https://github.com/cypresssem    |
|                                  | iconductorco/display-tft-st7789v |
+----------------------------------+----------------------------------+

