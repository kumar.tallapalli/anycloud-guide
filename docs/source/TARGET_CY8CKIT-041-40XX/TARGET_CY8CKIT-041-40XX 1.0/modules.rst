==================
BSP API Reference
==================

.. toctree::
   :hidden:

   group__group__bsp__settings.rst
   group__group__bsp__pin__state.rst
   group__group__bsp__pins.rst
   group__group__bsp__macros.rst
   group__group__bsp__functions.rst


| `BSP Settings <group__group__bsp__settings.html>`__  Peripheral Default HAL Settings:

   
+---------+-------------+----------------+--------+
|Resource |Parameter    |Value           |Remarks |
+=========+=============+================+========+
|         |Flow control |No flow control |        |
|UART     +-------------+----------------+--------+
|         |Data format  |8N1             |        |
|         +-------------+----------------+--------+
|         |Baud rate    |115200          |        |
+---------+-------------+----------------+--------+


| `Pin States <group__group__bsp__pin__state.html>`__
| `Pin Mappings <group__group__bsp__pins.html>`__
|    `LED Pins <group__group__bsp__pins__led.html>`__
|    `Button Pins <group__group__bsp__pins__btn.html>`__
|    `Communication Pins <group__group__bsp__pins__comm.html>`__
|    `Arduino Header Pins <group__group__bsp__pins__arduino.html>`__
|    `J2 Header Pins <group__group__bsp__pins__j2.html>`__
| `Macros <group__group__bsp__macros.html>`__
| `Functions <group__group__bsp__functions.html>`__

