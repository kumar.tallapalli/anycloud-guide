===========
Pin States
===========

.. doxygendefine:: CYBSP_LED_STATE_ON
   :project: TARGET_CY8CPROTO-064S1-SB 1.0
   
.. doxygendefine:: CYBSP_LED_STATE_OFF
   :project: TARGET_CY8CPROTO-064S1-SB 1.0
   
.. doxygendefine:: CYBSP_BTN_PRESSED
   :project: TARGET_CY8CPROTO-064S1-SB 1.0
   
.. doxygendefine:: CYBSP_BTN_OFF
   :project: TARGET_CY8CPROTO-064S1-SB 1.0
   


