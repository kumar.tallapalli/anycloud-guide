=======
Macros
=======

.. doxygendefine:: CYBSP_RSLT_ERR_SYSCLK_PM_CALLBACK
   :project: TARGET_CY8CPROTO-064S1-SB 1.0
  

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

 




