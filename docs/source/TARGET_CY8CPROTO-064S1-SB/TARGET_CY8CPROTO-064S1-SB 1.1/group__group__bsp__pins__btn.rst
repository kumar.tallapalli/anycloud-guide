============
Button Pins
============


.. doxygendefine:: CYBSP_SW2
   :project: TARGET_CY8CPROTO-064S1-SB 1.1
   
.. doxygendefine:: CYBSP_USER_BTN1
   :project: TARGET_CY8CPROTO-064S1-SB 1.1
   
.. doxygendefine:: CYBSP_USER_BTN
   :project: TARGET_CY8CPROTO-064S1-SB 1.1
   







