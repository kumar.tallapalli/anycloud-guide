=========
LED Pins
=========

.. doxygendefine:: CYBSP_LED3
   :project: TARGET_CY8CPROTO-064S1-SB 1.1
   
.. doxygendefine:: CYBSP_LED4 
   :project: TARGET_CY8CPROTO-064S1-SB 1.1
   
.. doxygendefine:: CYBSP_USER_LED1
   :project: TARGET_CY8CPROTO-064S1-SB 1.1
   
.. doxygendefine:: CYBSP_USER_LED2
   :project: TARGET_CY8CPROTO-064S1-SB 1.1
 
.. doxygendefine:: CYBSP_USER_LED 
   :project: TARGET_CY8CPROTO-064S1-SB 1.1
   





